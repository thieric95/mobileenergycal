package com.nixbymedia.energycalculator;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.extra.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static com.nixbymedia.energycalculator.extra.Utils.getFolderFile;

public class BaseActivity extends AppCompatActivity {

    public File photo;
    private Uri mImageUri;
    private Activity activity;
    private String sub_path;
    private String file;
    ProgressDialog dialog;
    public Double A4Horizantal = 8.27, A4Vertical = 11.69;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
    }

    public String getDate() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd / MM / yyyy");
            return format.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    // Here we are passing file name , sub_path means sub folder for each tab
    public void openCamera(String file, String sub_path, Activity activity) {
        try {
            this.file = file;
            this.sub_path = sub_path;
            this.activity = activity;
            askForMediaPermissionsCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void askForMediaPermissionsCamera() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasMediaPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
                if (hasMediaPermission != PackageManager.PERMISSION_GRANTED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        com.nixbymedia.energycalculator.extra.Utils.showMessageOKCancel(this, getString(R.string.access_read_external_storage_message), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Extra.PermissionCodes.REQUEST_PERMISSION_GALLERY_CAMERA);
                            }
                        });
                        return;
                    }
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Extra.PermissionCodes.REQUEST_PERMISSION_GALLERY_CAMERA);
                    return;
                }
            }
            askForCameraPermissions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void askForCameraPermissions() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasCameraPermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA);
                if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.CAMERA)) {
                        Utils.showMessageOKCancel(this, getString(R.string.access_camera_message), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Extra.PermissionCodes.REQUEST_PERMISSION_CAMERA);
                            }
                        });
                        return;
                    }
                    ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.CAMERA}, 10);
                    return;
                } else
                    startCamera();

            } else
                startCamera();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressDialog(final String message) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage(message);
                    dialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openFile(Context context, String url) {
        try {
            // Create URI
            File sharedFile = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS), "RocobookBusiness/" + url.substring(url.lastIndexOf("/"), url.length()));
            Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", sharedFile);

            //get MIMEtype from Uri
            ContentResolver cR = context.getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = mime.getExtensionFromMimeType(cR.getType(uri));
            // Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (type.equalsIgnoreCase("PDF")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (type.equalsIgnoreCase("CSV")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (type.equalsIgnoreCase("doc")) {
                // Excel file
                intent.setDataAndType(uri, "application/msword");
            } else if (type.equalsIgnoreCase("docx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(intent, "Open File"));

        } catch (Exception e) {
            e.printStackTrace();
            //   CommonFunctions.toastShort(getContext(), "No component found to open file");
        }

    }

    //Here we pass the file name and directoryPath where we are going to save the pdf.
    public void saveToPdf(final String sub_path) {
        new Thread() {
            public void run() {
                try {
                    showProgressDialog("Saving To pdf");
                    File pdfFile = new File(getFolderFile(BaseActivity.this, sub_path) + "/" + file + ".pdf");
                    PdfDocument document = new PdfDocument();

                    final Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath());

                    PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(bitmap.getWidth(), bitmap.getHeight(), 1).create();

                    PdfDocument.Page page = document.startPage(pageInfo);
                    page.getCanvas().drawColor(Color.LTGRAY);
                    page.getCanvas().drawBitmap(bitmap, 0, 0, null);
                    document.finishPage(page);
                    document.writeTo(new FileOutputStream(pdfFile));
                    document.close();
                    file = null;
                    hideProgressDialog();
                    if (pdfFile.exists() && pdfFile.length() > 0) {
                        photo.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();


    }

    public void showError(String msg, View view) {
        try {
            if (msg != null && !msg.isEmpty()) {
                Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getComment(String key) {
        try {
            if(CommentModel.getInstance().checkforkeyexist(key)){
                return CommentModel.getInstance().getComment().get(key);
            }else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void saveComment(String comment, String key) {
        try {
            if(!CommentModel.getInstance().getComment().containsKey(comment)){
                CommentModel.getInstance().getComment().put(key,comment);
            }else if(CommentModel.getInstance().getComment().containsKey(comment)){
                CommentModel.getInstance().getComment().put(key,comment);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    for showing only alert messages.
    public void showAlertMessages(String text) {
        try {
            AlertDialog dialog;
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog = mBuilder.create();
            if (text != null && !text.isEmpty()) {
                dialog.setMessage(text);
                dialog.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openComment(final String key) {
        try {
            LayoutInflater li = LayoutInflater.from(this);
            View dialogView = li.inflate(R.layout.comment_dialog, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);
            // set title
            alertDialogBuilder.setTitle("Assessors Comment");
            // set custom_dialog.xml to alertdialog builder
            alertDialogBuilder.setView(dialogView);

            Button btncomment = dialogView.findViewById(R.id.btnComment);
            final EditText text = dialogView.findViewById(R.id.text_comment);

            final AlertDialog alertDialog = alertDialogBuilder.create();
            if(CommentModel.getInstance().checkforkeyexist(key)){
                text.setText(CommentModel.getInstance().getComment().get(key));
            }
            btncomment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (text.getText().toString().trim().equals("")) {
                            text.setError("Please Enter Some Comment!");
                        } else {
                            saveComment(text.getText().toString(), key);
                            Toast.makeText(BaseActivity.this, "Commented : " + text.getText().toString(), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                // place where to store reference to camera taken picture deliveryNote.getDeliveryNoteNo()
                photo = new File(getFolderFile(this, sub_path), file + ".jpg");
            } catch (Exception e) {
                Utils.showToast(this, "Please check SD card! Can not take image shot!");
            }
            if (photo != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mImageUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", photo);

                } else {
                    mImageUri = Uri.fromFile(photo);

                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                //start camera intent
                activity.startActivityForResult(intent, Extra.ActivityRequestCode.CAMERA_REQUEST_CODE);

            } else
                Utils.showToast(this, "Please check SD card! Can not take image shot!");
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getMessage() != null)
                Utils.showToast(this, e.getMessage());
            else
                Utils.showToast(this, "Something went wrong , please try after some time");
        }
        activity = null;
        //  file = null;
        sub_path = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Extra.PermissionCodes.REQUEST_PERMISSION_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                } else {
                    Utils.showToast(this, getString(R.string.access_camera_denied));
                }
                break;
            case Extra.PermissionCodes.REQUEST_PERMISSION_GALLERY_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    askForCameraPermissions();
                } else {
                    // Permission Denied
                    Utils.showToast(this, getString(R.string.access_read_external_storage_denied));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public byte[] convertImageToByte(Uri uri){
        byte[] data = null;
        try {
            ContentResolver cr = getBaseContext().getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Extra.ActivityRequestCode.CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            FileModel.getFileModel().fileArrayByte.put(this.file, this.convertImageToByte(this.mImageUri));
        }
    }


}
