package com.nixbymedia.energycalculator.lighting;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;


import com.nixbymedia.energycalculator.Activity_Home;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.water_user.Water_Use;
import com.nixbymedia.energycalculator.extra.Debugger;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.Heating_System;
import com.nixbymedia.energycalculator.property.PropertyModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class DayLight_Lightening extends Fragment implements TextWatcher {


    protected Spinner spMeter;
    protected LinearLayout header;
    ImageButton back, comment, forward;
    ImageButton img1, img2, img3, img4, img5, img6;
    EditText led, fluorescent, halogen, indoor_wattage, outdoor_wattage, sensors;
    RadioButton rb;
    TextView cmnt;
    FragmentManager fragmentManager;
    String rb_Text = "NO";
    View view;
    private Activity ctx;

    public DayLight_Lightening() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_day_light, container, false);

        back = view.findViewById(R.id.day_light_back);
        forward = view.findViewById(R.id.day_light_forward);
        comment = view.findViewById(R.id.day_light_comment);
        spMeter = view.findViewById(R.id.sp_meter);
        img1 = view.findViewById(R.id.img_led);
        img2 = view.findViewById(R.id.img_fluorescent);
        img3 = view.findViewById(R.id.img_halogen);
        img4 = view.findViewById(R.id.img_total_wattage);
        img5 = view.findViewById(R.id.img_outdoor_lights_wattage);
        img6 = view.findViewById(R.id.img_wattage_sensor);
        led = view.findViewById(R.id.text_LED_count);
        led.addTextChangedListener(this);

        fluorescent = view.findViewById(R.id.text_Fluorescent_count);
        fluorescent.addTextChangedListener(this);

        halogen = view.findViewById(R.id.text_Halogen_count);
        halogen.addTextChangedListener(this);

        indoor_wattage = view.findViewById(R.id.text_indoor_light_wattage);
        outdoor_wattage = view.findViewById(R.id.text_outdoor_light_wattage);
        sensors = view.findViewById(R.id.text_wattage_sensor);

        cmnt = view.findViewById(R.id.comment_daylight_text);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).openComment(Extra.Tag.Daylight);
            }
        });
        rb = view.findViewById(R.id.inspect_radio_daylight);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.LED_Lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.LED_Lights, Extra.SubPath.Lighting, getActivity());
                }

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Fluorescent_Lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Fluorescent_Lights, Extra.SubPath.Lighting, getActivity());
                }

            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Halogen_lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Halogen_lights, Extra.SubPath.Lighting, getActivity());
                }

            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Indoor_Light + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Indoor_Light, Extra.SubPath.Lighting, getActivity());
                }

            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Outdoor_Light + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Outdoor_Light, Extra.SubPath.Lighting, getActivity());
                }

            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wattage_Sensor + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wattage_Sensor, Extra.SubPath.Lighting, getActivity());
                }

            }
        });

        rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_Text = "YES";
                } else {
                    rb_Text = "NO";
                }
            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StepProcessAct) getActivity()).replaceFragment(new Heating_System(), Extra.Tag.HeatingSystem);
            }
        });

//
//        DayLightBackTask dayLightBackTask = new DayLightBackTask(getContext(), byteimg1, byteimg2, byteimg3,
//                byteimg4, byteimg5, byteimg6);
//        dayLightBackTask.execute("remove");

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    calculatePoints();
                    saveinspector();
                    setLuxMeter();
                    ((StepProcessAct) getActivity()).replaceFragment(new Water_Use(), Extra.Tag.WaterUse);
                }
            }
        });

        setSpinner();
        setTextWatcherForIndoorWattage();
        return view;
    }


    public void saveinspector() {
        try {
            LightingModel.getInstance().setInspectorlightview(rb_Text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isValidate() {
        try {
            if (led.getText().toString().isEmpty() && fluorescent.getText().toString().isEmpty()
                    && halogen.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showAlertMessages(getString(R.string.error_validate_msg));
                return false;
            } else if (led.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Number of LED Lights", view);
                return false;
            } else if (fluorescent.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Number of Fluorescent Lights", view);
                return false;
            } else if (halogen.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Number of Halogen Lights", view);
                return false;
            } else if (outdoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Total Outdoor Light Wattage", view);
                return false;
            } else if (sensors.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Wattage with sensorsfitted", view);
                return false;
            } else if (!rb.isChecked()) {
                ((StepProcessAct) getActivity()).showError("Please check the checklist", view);
                return false;
            } else
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setOutDoorPoints() {
        try {
            int total_points = 0;
            if (!outdoor_wattage.getText().toString().isEmpty()) {
                total_points = total_points + Integer.parseInt(outdoor_wattage.getText().toString());
                LightingModel.getInstance().setTotal_outdoor_wattage(Integer.parseInt(outdoor_wattage.getText().toString()));
            }

            if (!sensors.getText().toString().isEmpty()) {
                total_points = total_points + Integer.parseInt(sensors.getText().toString());
                LightingModel.getInstance().setSensor_wattage(Integer.parseInt(sensors.getText().toString()));
            }

            LightingModel.getInstance().setTotal_outdoor_light(LightingModel.getInstance().getTotal_outdoor_wattage() - (LightingModel.getInstance().getSensor_wattage() / 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLuxMeter() {
        try {
            LightingModel.getInstance().setLux_meter(spMeter.getSelectedItem().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculatePoints() {
        try {
            setIndoorPoints();
            setOutDoorPoints();
            Debugger.debugE("LightingModel", LightingModel.getInstance().toString());

            float point = (LightingModel.getInstance().getTotal_indoor_light()
                    + LightingModel.getInstance().getTotal_outdoor_light())
                    / Float.parseFloat(PropertyModel.getInstance().getDwellingSize());
            if (point <= 1.5) {
                LightingModel.getInstance().setTotal_points(4f);
            } else if (point <= 3) {
                LightingModel.getInstance().setTotal_points(3f);
            } else if (point <= 5)
                LightingModel.getInstance().setTotal_points(2f);
            else if (point <= 7)
                LightingModel.getInstance().setTotal_points(1f);
            else if (point > 7) {
                LightingModel.getInstance().setTotal_points(0f);
            }

            if (spMeter.getSelectedItem().toString().equalsIgnoreCase("Yes")) {
                LightingModel.getInstance().setTotal_points(
                        LightingModel.getInstance().getTotal_points() + 1
                );
            }
            Debugger.debugE("LightingModel", LightingModel.getInstance().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setIndoorPoints() {
        try {
            int total_points = 0;
            if (!led.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(led.getText().toString())) * Extra.Watts.LED);
                LightingModel.getInstance().setLed_light(led.getText().toString());
            }
            if (!fluorescent.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(fluorescent.getText().toString())) * Extra.Watts.CFL);
                LightingModel.getInstance().setCfl_light(fluorescent.getText().toString());
            }

            if (!halogen.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(halogen.getText().toString())) * Extra.Watts.Halogen);
                LightingModel.getInstance().setHalogen_light(halogen.getText().toString());
            }

            if (!indoor_wattage.getText().toString().isEmpty()) {
                total_points = total_points + (Integer.parseInt(indoor_wattage.getText().toString()));
                LightingModel.getInstance().setTotal_indoor_wattage(Float.parseFloat(indoor_wattage.getText().toString()));
            }

            LightingModel.getInstance().setTotal_indoor_light((total_points));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTextWatcherForIndoorWattage() {
        try {
            indoor_wattage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (!s.toString().isEmpty()) {
                            clearLights(false);
                        } else
                            clearLights(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearLights(boolean isActive) {
        try {
            led.setEnabled(isActive);
            fluorescent.setEnabled(isActive);
            halogen.setEnabled(isActive);
            img1.setEnabled(isActive);
            img2.setEnabled(isActive);
            img3.setEnabled(isActive);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinner() {
        try {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.lux_meter, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spMeter.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // this textwatcher we have used for led_lights , fluorescent_lights ,incandescent_lights
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            if (!s.toString().isEmpty()) {
                indoor_wattage.setEnabled(false);
                img4.setEnabled(false);
            } else {
                indoor_wattage.setEnabled(true);
                img4.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
