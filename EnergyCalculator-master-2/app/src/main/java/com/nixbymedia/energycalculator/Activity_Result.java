package com.nixbymedia.energycalculator;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
//import android.support.constraint.Guideline;
//import android.support.v7.widget.AppCompatTextView;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.constraint.Guideline;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.nearby.connection.Payload;
import com.nixbymedia.energycalculator.BackgroundOperations.DayLightBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.HeatingBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.PropertyBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.ThermalBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.WaterUseBackTask;
import com.nixbymedia.energycalculator.certificate.CertificateActivity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.extra.Utils;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.heatingsystem.WaterHeating;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.report.ReportActivity;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;
import com.nixbymedia.energycalculator.water_user.Water_Use;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import android.util.Base64;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.nixbymedia.energycalculator.extra.Utils.getFolderFile;

public class Activity_Result extends BaseActivity implements View.OnClickListener {

    protected View energyRatingBox;
    protected View waterRatingBox;
    protected View borderEnergy;
    protected View borderWater;
    protected TextView tvA;
    protected TextView energyA;
    protected AppCompatTextView energyB;
    protected AppCompatTextView energyC;
    protected AppCompatTextView energyD;
    protected AppCompatTextView energyE;
    protected AppCompatTextView energyF;
    protected TextView tvWaterA;
    protected TextView tvWaterB;
    protected TextView tvWaterC;
    protected TextView tvWaterD;
    protected TextView tvWaterE;
    protected TextView tvWaterF;
    protected TextView waterA;
    protected TextView waterB;
    protected TextView waterC;
    protected TextView waterD;
    protected TextView waterE;
    protected TextView waterF;
    protected TextView tvB;
    protected TextView tvC;
    protected TextView tvD;
    protected TextView tvE;
    protected TextView tvF;
    protected Button btnCertificate;
    protected Button btnReport;
    protected Button btn_returnhome_result;
    protected View header;
    protected ImageView icIcon1;
    protected Guideline centerGuideline;
    protected Guideline centerVerticalGuideline;
    protected TextView tvEnergyRating;
    protected TextView tvWaterRating;
    double heating_point, water_point, daylight_point;
    private int userid;
    private String pro_id;
    SessionManagement session;
    DatabaseHelperClass dbhelper;
    private String option;
    HeatModel heatmodel = new HeatModel();
    WaterHeating waterheating = new WaterHeating();
    WaterUseModel watermodel = new WaterUseModel();
    PropertyModel propermodel = new PropertyModel();
    ThermalEnvelope thermalmodel = new ThermalEnvelope();
    LightingModel lightingmodel = new LightingModel();
    boolean finishedfetching = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_layout);
        this.dbhelper = new DatabaseHelperClass(this);
        this.getuserID();
        initView();
        Intent intent = getIntent();
        if (intent != null) {
            this.option = intent.getStringExtra("option");
            System.out.println(this.option);
            if (this.option.equals("view")) {
                this.pro_id = intent.getStringExtra("propertyID");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewing();


                    }
                });

            } else if (this.option.equals("update")) {
                if (!intent.getStringExtra("propertycert").equals(null) || !intent.getStringExtra("propertycert").equals("")) {
                    this.pro_id = intent.getStringExtra("propertycert");
                    System.out.println(WaterUseModel.getInstance().getTotal_points());
                    setWaterGrade(WaterUseModel.getInstance().getTotal_points());
                    System.out.println(ThermalEnvelope.getInstance().getPoints());
                    System.out.println(HeatModel.getInstance().getPoints());
                    System.out.println(LightingModel.getInstance().getTotal_points());


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            float total = ThermalEnvelope.getInstance().getPoints() + HeatModel.getInstance().getPoints() + LightingModel.getInstance().getTotal_points() + getWaterPoints();
                            setEnergyGrade(total);
                        }
                    });

                    EditingSection.Iseditinggroup.isheatingedited = false;
                    EditingSection.Iseditinggroup.iswateruseedited = false;
                    EditingSection.Iseditinggroup.isdaylightingedited = false;
                    EditingSection.Iseditinggroup.ispropertyedited = false;
                    EditingSection.Iseditinggroup.isthermaledited = false;
                    if (FileModel.getFileModel().fileArrayByte.size() > 0) {

                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    for (Map.Entry<String, byte[]> entry : FileModel.getFileModel().fileArrayByte.entrySet()) {
                                        createImg("ReportImage", entry.getKey(), entry.getValue());
                                        sleep(1500);
                                    }

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }
                        }.start();

                    }
                }
            } else if (this.option.equals("add")) {
                setWaterGrade(WaterUseModel.getInstance().getTotal_points());

                float total = ThermalEnvelope.getInstance().getPoints() + HeatModel.getInstance().getPoints() + LightingModel.getInstance().getTotal_points() + getWaterPoints();
                setEnergyGrade(total);
                if (FileModel.getFileModel().fileArrayByte.size() > 0) {

                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                for (Map.Entry<String, byte[]> entry : FileModel.getFileModel().fileArrayByte.entrySet()) {
                                    createImg("ReportImage", entry.getKey(), entry.getValue());
                                    sleep(1500);
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }.start();


                }
            }

        }

    }

    private float getWaterPoints() {
        if (WaterUseModel.getInstance().getShower_point() + WaterUseModel.getInstance().getKitchen_tap_point() < 12) {
            return WaterHeating.getInstance().getPoints() + 1;
        } else if (WaterUseModel.getInstance().getShower_point() + WaterUseModel.getInstance().getKitchen_tap_point() < 12) {
            return WaterHeating.getInstance().getPoints() + 0.5f;
        } else
            return WaterHeating.getInstance().getPoints();
    }

    private void SetGrade(int id_text) {
        TextView textView = findViewById(id_text);
        textView.setVisibility(View.VISIBLE);
    }

    private void setWaterGrade(double points) {
        if (points >= 12) {
            SetGrade(R.id.water_a);
        } else if (points >= 10) {
            SetGrade(R.id.water_b);
        } else if (points >= 7) {
            SetGrade(R.id.water_c);
        } else if (points >= 5) {
            SetGrade(R.id.water_d);
        } else if (points >= 3) {
            SetGrade(R.id.water_e);
        } else {
            SetGrade(R.id.water_f);
        }

    }

    private void setEnergyGrade(float points) {
        try {
            if (points > 25) {
                SetGrade(R.id.energy_a);
            } else if (points > 20) {
                SetGrade(R.id.energy_b);
            } else if (points > 15) {
                SetGrade(R.id.energy_c);
            } else if (points > 11) {
                SetGrade(R.id.energy_d);
            } else if (points > 8) {
                SetGrade(R.id.energy_e);
            } else if (points < 8) {
                SetGrade(R.id.energy_f);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        energyRatingBox = findViewById(R.id.energy_rating_box);
        waterRatingBox = (View) findViewById(R.id.water_rating_box);
        borderEnergy = (View) findViewById(R.id.border_energy);
        borderWater = (View) findViewById(R.id.border_water);
        tvA = (TextView) findViewById(R.id.tv_a);
        energyA = (TextView) findViewById(R.id.energy_a);
        energyB = (AppCompatTextView) findViewById(R.id.energy_b);
        energyC = (AppCompatTextView) findViewById(R.id.energy_c);
        energyD = (AppCompatTextView) findViewById(R.id.energy_d);
        energyE = (AppCompatTextView) findViewById(R.id.energy_e);
        energyF = (AppCompatTextView) findViewById(R.id.energy_f);
        tvWaterA = (TextView) findViewById(R.id.tv_water_a);
        tvWaterB = (TextView) findViewById(R.id.tv_water_b);
        tvWaterC = (TextView) findViewById(R.id.tv_water_c);
        tvWaterD = (TextView) findViewById(R.id.tv_water_d);
        tvWaterE = (TextView) findViewById(R.id.tv_water_e);
        tvWaterF = (TextView) findViewById(R.id.tv_water_f);
        waterA = (TextView) findViewById(R.id.water_a);
        waterB = (TextView) findViewById(R.id.water_b);
        waterC = (TextView) findViewById(R.id.water_c);
        waterD = (TextView) findViewById(R.id.water_d);
        waterE = (TextView) findViewById(R.id.water_e);
        waterF = (TextView) findViewById(R.id.water_f);
        tvB = (TextView) findViewById(R.id.tv_b);
        tvC = (TextView) findViewById(R.id.tv_c);
        tvD = (TextView) findViewById(R.id.tv_d);
        tvE = (TextView) findViewById(R.id.tv_e);
        tvF = (TextView) findViewById(R.id.tv_f);
        btnCertificate = (Button) findViewById(R.id.btn_certificate);
        btnCertificate.setOnClickListener(Activity_Result.this);
        btnReport = (Button) findViewById(R.id.btn_report);
        btnReport.setOnClickListener(Activity_Result.this);
//        this.btnReturnHome = findViewById(R.id.btnReturnHome);
//        this.btnReturnHome.setOnClickListener(Activity_Result.this);
        header = (View) findViewById(R.id.header);
        icIcon1 = (ImageView) findViewById(R.id.ic_icon_1);
        centerGuideline = (Guideline) findViewById(R.id.center_guideline);
        centerVerticalGuideline = (Guideline) findViewById(R.id.center_vertical_guideline);
        tvEnergyRating = (TextView) findViewById(R.id.tv_energy_Rating);
        tvWaterRating = (TextView) findViewById(R.id.tv_water_rating);
        btn_returnhome_result = (Button) findViewById(R.id.btn_returnhome_result);
        btn_returnhome_result.setOnClickListener(Activity_Result.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_certificate:
                startActivity(new Intent(this, CertificateActivity.class));
                break;
            case R.id.btn_report:
                startActivity(new Intent(this, ReportActivity.class));
                break;
            case R.id.btn_returnhome_result:
                startActivity(new Intent(this, Activity_Home.class));
                FileModel.getFileModel().clear_filearraybyte();
                FileModel.getFileModel().clear_fileMap();
                CommentModel.getInstance().clear_comment();
                this.clearimage();
                setWaterGrade(0);
                setEnergyGrade(0);
                this.clearvalue();
                break;
            default:
                break;
        }
    }


    private void viewing() {


            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something here
                    read_property();
                }
            }, 900);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something here
                    read_thermal_envelope();
                }
            }, 900);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something here
                    read_heatingsystem();

                }
            }, 900);

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something here
                    read_daylightlightening();
                }
            }, 900);


            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something here
                    read_wateruse();
                }
            }, 800);




        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something here
                System.out.println(WaterUseModel.getInstance().getTotal_points());
                setWaterGrade(WaterUseModel.getInstance().getTotal_points());
                float total = ThermalEnvelope.getInstance().getPoints() + HeatModel.getInstance().getPoints() + LightingModel.getInstance().getTotal_points() + getWaterPoints();
                System.out.println(total);
                setEnergyGrade(total);
            }
        }, 3000);
    }


    private void read_property() {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROPERTYSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", this.pro_id);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Activity_Result.this.runOnUiThread(
                        () -> Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Get JSON", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                Activity_Result.this.runOnUiThread(
                                        (Runnable) () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {

                            propermodel.setCertificate_number(jsonObject.getString("prop_id"));
                            propermodel.setDwellingType(jsonObject.getString("dwell_type"));
                            propermodel.setDwellingSize(jsonObject.getString("dwell_size"));
                            propermodel.setNoOfBedRooms(jsonObject.getString("room_no"));
                            propermodel.setClimate_location(jsonObject.getString("climate_location"));
                            propermodel.setLegal_description(jsonObject.getString("legal_desc"));
                            propermodel.setTitle_certificate(jsonObject.getString("cert_title"));
                            propermodel.setAddress(jsonObject.getString("address"));
                            propermodel.setComment(jsonObject.getString("asstcomment"));

                            PropertyModel.getInstance().setCertificate_number(propermodel.getCertificate_number());
                            PropertyModel.getInstance().setDwellingType(propermodel.getDwellingType());
                            PropertyModel.getInstance().setDwellingSize(propermodel.getDwellingSize());
                            PropertyModel.getInstance().setNoOfBedRooms(propermodel.getNoOfBedRooms());
                            PropertyModel.getInstance().setClimate_location(propermodel.getClimate_location());
                            PropertyModel.getInstance().setLegal_description(propermodel.getLegal_description());
                            PropertyModel.getInstance().setTitle_certificate(propermodel.getTitle_certificate());
                            PropertyModel.getInstance().setAddress(propermodel.getAddress());
                            PropertyModel.getInstance().setComment(propermodel.getComment());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Activity_Result.this.runOnUiThread(
                            () -> Toast.makeText(getApplicationContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    private void read_wateruse() {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLWATERUSESELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", this.pro_id);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Activity_Result.this.runOnUiThread(
                        () -> Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Response", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                Activity_Result.this.runOnUiThread(
                                        () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );
                            }
                        } else {
                            watermodel.setId(jsonObject.getInt("wateruseid"));
                            watermodel.setKitchen_tap(jsonObject.get("kitchen_tap").toString());
                            watermodel.setKitchen_tap_point(BigDecimal.valueOf(jsonObject.getDouble("kitchen_tap_points")).floatValue());
                            if (jsonObject.get("kitchen_tap_img") == null) {
                                watermodel.setImage_1(null);
                            } else {
                                watermodel.setImage_1(Base64.decode(jsonObject.getString("kitchen_tap_img"), Base64.DEFAULT));
                            }
                            watermodel.setBasin_tap(jsonObject.get("basin_tap").toString());
                            watermodel.setBasin_tap_point(BigDecimal.valueOf(jsonObject.getDouble("basin_tap_points")).floatValue());
                            if (jsonObject.get("basin_tap_img") == null) {
                                watermodel.setImage_2(null);
                            } else {
                                watermodel.setImage_2(Base64.decode(jsonObject.getString("basin_tap_img"), Base64.DEFAULT));
                            }
                            watermodel.setShower(jsonObject.get("shower").toString());
                            watermodel.setShower_point(BigDecimal.valueOf(jsonObject.getDouble("shower_points")).floatValue());
                            if (jsonObject.get("shower_img") == null) {
                                watermodel.setImage_3(null);
                            } else {
                                watermodel.setImage_3(Base64.decode(jsonObject.getString("shower_img"), Base64.DEFAULT));
                            }
                            watermodel.setMeasuredInspect(jsonObject.getString("rates_inspect"));
                            watermodel.setWc_point(jsonObject.getString("worst_wc"));
                            watermodel.setWorst_Wc_point(BigDecimal.valueOf(jsonObject.getDouble("worst_wc_points")).floatValue());
                            watermodel.setDocInspect(jsonObject.getString("doc_by_inspect"));
                            watermodel.setTotal_points(BigDecimal.valueOf(jsonObject.getDouble("total_points")).floatValue());
                            watermodel.setComment(jsonObject.getString("asstcomment"));


                            WaterUseModel.getInstance().setId(watermodel.getId());
                            WaterUseModel.getInstance().setKitchen_tap(watermodel.getKitchen_tap());
                            WaterUseModel.getInstance().setKitchen_tap_point(watermodel.getKitchen_tap_point());
                            WaterUseModel.getInstance().setImage_1(watermodel.getImage_1());
                            WaterUseModel.getInstance().setBasin_tap(watermodel.getBasin_tap());
                            WaterUseModel.getInstance().setBasin_tap_point(watermodel.getBasin_tap_point());
                            WaterUseModel.getInstance().setImage_2(watermodel.getImage_2());
                            WaterUseModel.getInstance().setShower(watermodel.getShower());
                            WaterUseModel.getInstance().setShower_point(watermodel.getShower_point());
                            WaterUseModel.getInstance().setImage_3(watermodel.getImage_3());
                            WaterUseModel.getInstance().setMeasuredInspect(watermodel.getMeasuredInspect());
                            WaterUseModel.getInstance().setWc_point(watermodel.getWc_point());
                            WaterUseModel.getInstance().setWorst_Wc_point(watermodel.getWorst_Wc_point());
                            WaterUseModel.getInstance().setDocInspect(watermodel.getDocInspect());
                            WaterUseModel.getInstance().setTotal_points(watermodel.getTotal_points());
                            WaterUseModel.getInstance().setComment(watermodel.getComment());

                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.kitchen_tap, watermodel.getImage_1());
                            createImg("ReportImage", Extra.FileConstants.kitchen_tap, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.kitchen_tap));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.basin_tap, watermodel.getImage_2());
                            createImg("ReportImage", Extra.FileConstants.basin_tap, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.basin_tap));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.worst_shower, watermodel.getImage_3());
                            createImg("ReportImage", Extra.FileConstants.worst_shower, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.worst_shower));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Activity_Result.this.runOnUiThread(
                            () -> Toast.makeText(getApplicationContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    private void read_heatingsystem() {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLHEATINGSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", this.pro_id);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Activity_Result.this.runOnUiThread(
                        (Runnable) () -> Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Response", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                Activity_Result.this.runOnUiThread(
                                        () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {
                            heatmodel.setId(jsonObject.getInt("heatid"));
                            heatmodel.setHeat_pump_to_air(jsonObject.get("air_perc").toString());
                            heatmodel.setHeat_pump_to_air_kw(jsonObject.get("air_kw").toString());
                            if (jsonObject.get("air_image") == null) {
                                heatmodel.setImage1(null);
                            } else {
                                heatmodel.setImage1(Base64.decode(jsonObject.getString("air_image"), Base64.DEFAULT));
                            }
                            heatmodel.setHeat_pump_hydrolic(jsonObject.get("hyd_perc").toString());
                            heatmodel.setHeat_pump_hydrolic_kw(jsonObject.get("hyd_kw").toString());

                            if (jsonObject.get("hyd_image") == null) {
                                heatmodel.setImage2(null);
                            } else {
                                heatmodel.setImage2(Base64.decode(jsonObject.getString("hyd_image"), Base64.DEFAULT));
                            }
                            heatmodel.setNatural_gas(jsonObject.get("lpg_gas_perc").toString());
                            heatmodel.setNatural_gas_kw(jsonObject.get("lpg_gas_kw").toString());

                            if (jsonObject.get("lpg_gas_image") == null) {
                                heatmodel.setImage3(null);
                            } else {
                                heatmodel.setImage3(Base64.decode(jsonObject.getString("lpg_gas_image"), Base64.DEFAULT));
                            }
                            heatmodel.setWood_burner(jsonObject.get("wood_perc").toString());
                            heatmodel.setWood_burner_kw(jsonObject.get("wood_kw").toString());

                            if (jsonObject.get("wood_image") == null) {
                                heatmodel.setImage4(null);
                            } else {
                                heatmodel.setImage4(Base64.decode(jsonObject.getString("wood_image"), Base64.DEFAULT));
                            }
                            heatmodel.setEletric_heater(jsonObject.get("ele_h_perc").toString());
                            heatmodel.setEletric_heater_kw(jsonObject.get("ele_h_kw").toString());

                            if (jsonObject.get("ele_h_image") == null) {
                                heatmodel.setImage5(null);
                            } else {
                                heatmodel.setImage5(Base64.decode(jsonObject.getString("ele_h_image").trim(), Base64.DEFAULT));
                            }
                            heatmodel.setLpg_gas(jsonObject.get("natural_gas_perc").toString());
                            heatmodel.setLpg_gas_kw(jsonObject.get("natural_gas_kw").toString());

                            if (jsonObject.get("natural_gas_image") == null) {
                                heatmodel.setImage6(null);
                            } else {
                                heatmodel.setImage6(Base64.decode(jsonObject.getString("natural_gas_image"), Base64.DEFAULT));
                            }
                            waterheating.setPoints(BigDecimal.valueOf(jsonObject.getDouble("back_boosted_points")).floatValue());
                            waterheating.setWater_heating(jsonObject.getString("back_boosted"));
                            if (jsonObject.get("back_boosted_image") == null) {
                                heatmodel.setImage7(null);
                            } else {
                                heatmodel.setImage7(Base64.decode(jsonObject.getString("back_boosted_image"), Base64.DEFAULT));
                            }
                            heatmodel.setPoints(BigDecimal.valueOf(jsonObject.getDouble("heat_points")).floatValue());
                            heatmodel.setComment(jsonObject.getString("asstcomment"));


                            HeatModel.getInstance().setId(heatmodel.getId());
                            HeatModel.getInstance().setHeat_pump_to_air(heatmodel.getHeat_pump_to_air());
                            HeatModel.getInstance().setHeat_pump_to_air_kw(heatmodel.getHeat_pump_to_air_kw());

                            HeatModel.getInstance().setImage1(heatmodel.getImage1());

                            HeatModel.getInstance().setHeat_pump_hydrolic(heatmodel.getHeat_pump_hydrolic());
                            HeatModel.getInstance().setHeat_pump_hydrolic_kw(heatmodel.getHeat_pump_hydrolic_kw());

                            HeatModel.getInstance().setImage2(heatmodel.getImage2());
                            HeatModel.getInstance().setNatural_gas(heatmodel.getNatural_gas());
                            HeatModel.getInstance().setNatural_gas_kw(heatmodel.getNatural_gas_kw());
                            HeatModel.getInstance().setImage3(heatmodel.getImage3());
                            HeatModel.getInstance().setWood_burner(heatmodel.getWood_burner());
                            HeatModel.getInstance().setWood_burner_kw(heatmodel.getWood_burner_kw());

                            HeatModel.getInstance().setImage4(heatmodel.getImage4());
                            HeatModel.getInstance().setEletric_heater(heatmodel.getEletric_heater());
                            HeatModel.getInstance().setEletric_heater_kw(heatmodel.getEletric_heater_kw());
                            HeatModel.getInstance().setImage5(heatmodel.getImage5());

                            HeatModel.getInstance().setLpg_gas(heatmodel.getLpg_gas());
                            HeatModel.getInstance().setLpg_gas_kw(heatmodel.getLpg_gas_kw());
                            HeatModel.getInstance().setImage6(heatmodel.getImage6());
                            WaterHeating.getInstance().setPoints(waterheating.getPoints());
                            WaterHeating.getInstance().setWater_heating(waterheating.getWater_heating());
                            HeatModel.getInstance().setImage7(heatmodel.getImage7());
                            HeatModel.getInstance().setPoints(heatmodel.getPoints());
                            HeatModel.getInstance().setComment(heatmodel.getComment());

                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.heatair, heatmodel.getImage1());
                            createImg("ReportImage", Extra.FileConstants.heatair, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heatair));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.heathydronic, heatmodel.getImage2());
                            createImg("ReportImage", Extra.FileConstants.heathydronic, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heathydronic));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.gas, heatmodel.getImage3());
                            createImg("ReportImage", Extra.FileConstants.gas, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.gas));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.naturalgas, heatmodel.getImage4());
                            createImg("ReportImage", Extra.FileConstants.naturalgas, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.naturalgas));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Wood, heatmodel.getImage5());
                            createImg("ReportImage", Extra.FileConstants.Wood, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wood));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.ElectricHeater, heatmodel.getImage6());
                            createImg("ReportImage", Extra.FileConstants.ElectricHeater, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.ElectricHeater));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Solar, heatmodel.getImage7());
                            createImg("ReportImage", Extra.FileConstants.Solar, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Solar));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Activity_Result.this.runOnUiThread(
                            (Runnable) () -> Toast.makeText(getApplicationContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    private void read_daylightlightening() {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLLIGHTINGSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", this.pro_id);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Activity_Result.this.runOnUiThread(
                        () -> Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Response", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                Activity_Result.this.runOnUiThread(
                                        () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );
                            }
                        } else {
                            lightingmodel.setId(jsonObject.getInt("daylightid"));
                            lightingmodel.setLux_meter(jsonObject.getString("lux_meter"));
                            lightingmodel.setLed_light(Integer.toString(jsonObject.getInt("no_led")));

                            if (jsonObject.get("no_led_img") == null) {
                                lightingmodel.setImage_1(null);
                            } else {
                                lightingmodel.setImage_1(Base64.decode(jsonObject.getString("no_led_img"), Base64.DEFAULT));
                            }
                            lightingmodel.setCfl_light(jsonObject.getString("no_fluo"));

                            if (jsonObject.get("no_fluo_img") == null) {
                                lightingmodel.setImage_2(null);
                            } else {
                                lightingmodel.setImage_2(Base64.decode(jsonObject.getString("no_fluo_img"), Base64.DEFAULT));
                            }

                            lightingmodel.setHalogen_light(Integer.toString(jsonObject.getInt("no_halog")));

                            if (jsonObject.get("no_halog_img") == null) {
                                lightingmodel.setImage_3(null);
                            } else {
                                lightingmodel.setImage_3(Base64.decode(jsonObject.getString("no_halog_img"), Base64.DEFAULT));
                            }

                            lightingmodel.setTotal_indoor_wattage(BigDecimal.valueOf(jsonObject.getDouble("total_in_watt")).floatValue());

                            if (jsonObject.get("total_in_watt_img") == null) {
                                lightingmodel.setImage_4(null);
                            } else {
                                lightingmodel.setImage_4(Base64.decode(jsonObject.getString("total_in_watt_img"), Base64.DEFAULT));
                            }
                            lightingmodel.setTotal_outdoor_wattage(jsonObject.getInt("total_out_watt"));

                            if (jsonObject.get("total_out_watt_img") == null) {
                                lightingmodel.setImage_5(null);
                            } else {
                                lightingmodel.setImage_5(Base64.decode(jsonObject.getString("total_out_watt_img"), Base64.DEFAULT));
                            }
                            lightingmodel.setSensor_wattage(jsonObject.getInt("watt_sensor"));

                            if (jsonObject.get("watt_sensor_img") == null) {
                                lightingmodel.setImage_6(null);
                            } else {
                                lightingmodel.setImage_6(Base64.decode(jsonObject.getString("watt_sensor_img"), Base64.DEFAULT));
                            }
                            lightingmodel.setTotal_indoor_light(BigDecimal.valueOf(jsonObject.getDouble("total_in_light")).floatValue());
                            lightingmodel.setTotal_outdoor_light(BigDecimal.valueOf(jsonObject.getDouble("total_out_light")).floatValue());
                            lightingmodel.setInspectorlightview(jsonObject.getString("light_inspect"));
                            lightingmodel.setTotal_points(jsonObject.getInt("light_points"));
                            lightingmodel.setComment(jsonObject.getString("asstcomment"));

                            LightingModel.getInstance().setId(lightingmodel.getId());
                            LightingModel.getInstance().setLux_meter(lightingmodel.getLux_meter());
                            LightingModel.getInstance().setLed_light(lightingmodel.getLed_light());
                            LightingModel.getInstance().setImage_1(lightingmodel.getImage_1());
                            LightingModel.getInstance().setCfl_light(lightingmodel.getCfl_light());
                            LightingModel.getInstance().setImage_2(lightingmodel.getImage_2());
                            LightingModel.getInstance().setHalogen_light(lightingmodel.getHalogen_light());
                            LightingModel.getInstance().setImage_3(lightingmodel.getImage_3());
                            LightingModel.getInstance().setTotal_indoor_wattage(lightingmodel.getTotal_indoor_wattage());
                            LightingModel.getInstance().setImage_4(lightingmodel.getImage_4());
                            LightingModel.getInstance().setTotal_outdoor_wattage(lightingmodel.getTotal_outdoor_wattage());
                            LightingModel.getInstance().setImage_5(lightingmodel.getImage_5());
                            LightingModel.getInstance().setSensor_wattage(lightingmodel.getSensor_wattage());
                            LightingModel.getInstance().setImage_6(lightingmodel.getImage_6());
                            LightingModel.getInstance().setTotal_indoor_light(lightingmodel.getTotal_indoor_light());
                            LightingModel.getInstance().setTotal_outdoor_light(lightingmodel.getTotal_outdoor_light());
                            LightingModel.getInstance().setInspectorlightview(lightingmodel.getInspectorlightview());
                            LightingModel.getInstance().setTotal_points(lightingmodel.getTotal_points());
                            LightingModel.getInstance().setComment(lightingmodel.getComment());

                            if (lightingmodel.getImage_1() != null) {
                                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.LED_Lights, lightingmodel.getImage_1());
                                createImg("ReportImage", Extra.FileConstants.LED_Lights, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.LED_Lights));
                            }
                            if (lightingmodel.getImage_2() != null) {
                                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Fluorescent_Lights, lightingmodel.getImage_2());
                                createImg("ReportImage", Extra.FileConstants.Fluorescent_Lights, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Fluorescent_Lights));
                            }
                            if (lightingmodel.getImage_3() != null) {
                                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Halogen_lights, lightingmodel.getImage_3());
                                createImg("ReportImage", Extra.FileConstants.Halogen_lights, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Halogen_lights));
                            }
                            if (lightingmodel.getImage_4() != null) {
                                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Indoor_Light, lightingmodel.getImage_4());
                                createImg("ReportImage", Extra.FileConstants.Indoor_Light, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Indoor_Light));
                            }
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Outdoor_Light, lightingmodel.getImage_5());
                            createImg("ReportImage", Extra.FileConstants.Outdoor_Light, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Outdoor_Light));
                            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Wattage_Sensor, lightingmodel.getImage_6());
                            createImg("ReportImage", Extra.FileConstants.Wattage_Sensor, FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wattage_Sensor));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Activity_Result.this.runOnUiThread(
                            () -> Toast.makeText(getApplicationContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    private void read_thermal_envelope() {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLTHERMALSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", this.pro_id);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Activity_Result.this.runOnUiThread(
                        () -> Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Response", jsonData);
                    try {
                        Log.e("Get JSON", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                Activity_Result.this.runOnUiThread(
                                        (Runnable) () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {
                            thermalmodel.setId(jsonObject.getInt("thermalid"));
                            thermalmodel.setIndex(jsonObject.getString("thermalindex"));
                            thermalmodel.setPoints(jsonObject.getInt("thermalpoint"));
                            thermalmodel.setComment(jsonObject.getString("asstcomment"));


                            ThermalEnvelope.getInstance().setId(thermalmodel.getId());
                            ThermalEnvelope.getInstance().setIndex(thermalmodel.getIndex());
                            ThermalEnvelope.getInstance().setPoints(thermalmodel.getPoints());
                            ThermalEnvelope.getInstance().setComment(thermalmodel.getComment());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Activity_Result.this.runOnUiThread(
                            () -> Toast.makeText(getApplicationContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    private void getuserID() {
        this.session = new SessionManagement(getApplicationContext());
        if (this.session.checkforlogin()) {
            finish();
        }
        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
    }

    private void clearimage() {
        lightingmodel.setImage_1(null);
        lightingmodel.setImage_2(null);
        lightingmodel.setImage_3(null);
        lightingmodel.setImage_4(null);
        lightingmodel.setImage_5(null);
        lightingmodel.setImage_6(null);
        watermodel.setImage_1(null);
        watermodel.setImage_2(null);
        watermodel.setImage_3(null);
        heatmodel.setImage1(null);
        heatmodel.setImage2(null);
        heatmodel.setImage3(null);
        heatmodel.setImage4(null);
        heatmodel.setImage5(null);
        heatmodel.setImage6(null);
        heatmodel.setImage7(null);

        HeatModel.getInstance().setImage1(null);
        HeatModel.getInstance().setImage2(null);
        HeatModel.getInstance().setImage3(null);
        HeatModel.getInstance().setImage4(null);
        HeatModel.getInstance().setImage5(null);
        HeatModel.getInstance().setImage6(null);
        HeatModel.getInstance().setImage7(null);


        LightingModel.getInstance().setImage_1(null);
        LightingModel.getInstance().setImage_2(null);
        LightingModel.getInstance().setImage_3(null);
        LightingModel.getInstance().setImage_4(null);
        LightingModel.getInstance().setImage_5(null);
        LightingModel.getInstance().setImage_6(null);

        WaterUseModel.getInstance().setImage_1(null);
        WaterUseModel.getInstance().setImage_2(null);
        WaterUseModel.getInstance().setImage_3(null);
    }

    public void createImg(final String path, final String filename, byte[] bytes) {
        new Thread() {
            @Override
            public void run() {
                try {
                    File image_file_path = getFolderFile(Activity_Result.this, path);

                    //final File file = new File(getFolderFile(Activity_Result.this, path), filename + System.currentTimeMillis() + ".jpg");
                    //final File file = File.createTempFile(filename + System.currentTimeMillis(), ".jpg", getFolderFile(Activity_Result.this.getApplicationContext(), path));
                    FileOutputStream fos = null;
                    if (bytes != null) {
                        try {
                            if (!getFolderFile(Activity_Result.this, path).exists()) {
                                image_file_path.mkdirs();
                            }
                            File imagefile = File.createTempFile(filename + System.currentTimeMillis(), ".jpg", getFolderFile(Activity_Result.this, path));

                            fos = new FileOutputStream(imagefile);
                            fos.write(bytes);
                            fos.flush();

                            FileModel.getFileModel().fileMap.put(filename, imagefile.getAbsolutePath());
                            //System.out.println(filename + ": " + FileModel.getFileModel().fileMap.get(filename));


                            System.out.println("File Written Successfully");


                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (fos != null) {
                                    fos.close();
                                }
                            } catch (IOException ioe) {
                                System.out.println("Error in closing the Stream");
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();


    }

    private void clearvalue() {
        PropertyModel.getInstance().setCertificate_number("");
        PropertyModel.getInstance().setDwellingType("");
        PropertyModel.getInstance().setDwellingSize("");
        PropertyModel.getInstance().setNoOfBedRooms("");
        PropertyModel.getInstance().setClimate_location("");
        PropertyModel.getInstance().setLegal_description("");
        PropertyModel.getInstance().setTitle_certificate("");
        PropertyModel.getInstance().setAddress("");
        PropertyModel.getInstance().setComment("");

        WaterUseModel.getInstance().setId(0);
        WaterUseModel.getInstance().setKitchen_tap("");
        WaterUseModel.getInstance().setKitchen_tap_point(0);
        WaterUseModel.getInstance().setBasin_tap("");
        WaterUseModel.getInstance().setBasin_tap_point(0);
        WaterUseModel.getInstance().setShower("");
        WaterUseModel.getInstance().setShower_point(0);
        WaterUseModel.getInstance().setMeasuredInspect("");
        WaterUseModel.getInstance().setWc_point("");
        WaterUseModel.getInstance().setWorst_Wc_point(0);
        WaterUseModel.getInstance().setDocInspect("");
        WaterUseModel.getInstance().setTotal_points(0);
        WaterUseModel.getInstance().setComment("");

        HeatModel.getInstance().setId(0);
        HeatModel.getInstance().setHeat_pump_to_air("");
        HeatModel.getInstance().setHeat_pump_to_air_kw("");
        HeatModel.getInstance().setHeat_pump_hydrolic("");
        HeatModel.getInstance().setHeat_pump_hydrolic_kw("");
        HeatModel.getInstance().setNatural_gas("");
        HeatModel.getInstance().setNatural_gas_kw("");
        HeatModel.getInstance().setWood_burner("");
        HeatModel.getInstance().setWood_burner_kw("");
        HeatModel.getInstance().setEletric_heater("");
        HeatModel.getInstance().setEletric_heater_kw("");
        HeatModel.getInstance().setLpg_gas("");
        HeatModel.getInstance().setLpg_gas_kw("");
        WaterHeating.getInstance().setPoints(0);
        WaterHeating.getInstance().setWater_heating("");
        HeatModel.getInstance().setPoints(0);
        HeatModel.getInstance().setComment("");

        LightingModel.getInstance().setId(0);
        LightingModel.getInstance().setLux_meter("");
        LightingModel.getInstance().setLed_light("");
        LightingModel.getInstance().setCfl_light("");
        LightingModel.getInstance().setHalogen_light("");
        LightingModel.getInstance().setTotal_indoor_wattage(0);
        LightingModel.getInstance().setTotal_outdoor_wattage(0);
        LightingModel.getInstance().setSensor_wattage(0);
        LightingModel.getInstance().setTotal_indoor_light(0);
        LightingModel.getInstance().setTotal_outdoor_light(0);
        LightingModel.getInstance().setInspectorlightview("");
        LightingModel.getInstance().setTotal_points(0);
        LightingModel.getInstance().setComment("");

        ThermalEnvelope.getInstance().setId(0);
        ThermalEnvelope.getInstance().setIndex("");
        ThermalEnvelope.getInstance().setPoints(0);
        ThermalEnvelope.getInstance().setComment("");


    }

}
