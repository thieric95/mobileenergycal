package com.nixbymedia.energycalculator.property;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.Places;
//import com.google.android.gms.location.places.ui.PlacePicker;
import com.nixbymedia.energycalculator.Activity_Home;
import com.nixbymedia.energycalculator.Activity_Result;
import com.nixbymedia.energycalculator.BackgroundOperations.PropertyBackTask;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static android.app.Activity.RESULT_OK;

public class PropertyDetail extends Fragment implements View.OnClickListener{

    //Context context;
    private Spinner spinner, dwel_type, climate_location;
    private EditText dwel_size, bed_rooms, legal_desc,
            certificate, address_loc;// city, state, post_code;
    private ImageButton comment, forward, btn_home;
    private TextView cmnt_text;
    private FragmentManager fragmentManager;
    private View view;
    private GoogleApiClient mGoogleApiClient;
    private Activity ctx;
    public PropertyDetail() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_property_detail, container, false);
//        mGoogleApiClient = new GoogleApiClient
//                .Builder(getActivity())
//                .addApi(Places.GEO_DATA_API)
//                .addApi(Places.PLACE_DETECTION_API)
//                .enableAutoManage(getActivity(), 0,this)
//                .build();
        dwel_type = view.findViewById(R.id.dwelling_type);
        climate_location = view.findViewById(R.id.climate_location);
        dwel_size = view.findViewById(R.id.text_dwelling_size);
        bed_rooms = view.findViewById(R.id.text_bed_rooms);
        legal_desc = view.findViewById(R.id.text_legal_description);
        certificate = view.findViewById(R.id.text_certificate);
        address_loc = view.findViewById(R.id.address);
       // city = view.findViewById(R.id.city_town);
      //  state = view.findViewById(R.id.state);
      //  post_code = view.findViewById(R.id.postal_code);
        // back = view.findViewById(R.id.prop_back);
        comment = view.findViewById(R.id.prop_comment);
        forward = view.findViewById(R.id.prop_forward);
        cmnt_text = view.findViewById(R.id.comment_text_view);
        btn_home = view.findViewById(R.id.btn_home);
        //PropertyBackTask propertyBackTask = new PropertyBackTask(getContext());
        //propertyBackTask.execute("add");
        // back.setEnabled(false);

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*CommentDialog commentDialog = new CommentDialog(getContext());
                commentDialog.show();*/
                ((BaseActivity) getActivity()).openComment(Extra.Tag.property);
            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
//                    mGoogleApiClient.stopAutoManage(getActivity());
//                    mGoogleApiClient.disconnect();
                    setPropertyData();

                    ((StepProcessAct) getActivity()).replaceFragment(new Thermal_Envelope(), Extra.Tag.property);
                }
            }
        });
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mGoogleApiClient.stopAutoManage(getActivity());
//                mGoogleApiClient.disconnect();
                Intent intent = new Intent((StepProcessAct) getActivity(), Activity_Home.class);
                startActivity(intent);

            }
        });
        //   spinner_values(view, R.id.climate_location);
        setClimateLocation();
//        iv_location.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openPlacePicker();
//            }
//        });

        return view;
    }


    private void setPropertyData() {
        try {
            PropertyModel.getInstance().setDwellingSize(dwel_size.getText().toString());
            PropertyModel.getInstance().setDwellingType(dwel_type.getSelectedItem().toString());
            PropertyModel.getInstance().setNoOfBedRooms(bed_rooms.getText().toString());
            PropertyModel.getInstance().setLegal_description(legal_desc.getText().toString());
            PropertyModel.getInstance().setTitle_certificate(certificate.getText().toString());
            PropertyModel.getInstance().setHouse_no(address_loc.getText().toString());
            PropertyModel.getInstance().setCertificate_number(getCertificateNumber());
            PropertyModel.getInstance().setClimate_location(climate_location.getSelectedItem().toString());
            PropertyModel.getInstance().setAddress(this.address_loc.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    private void openPlacePicker() {
//        try {
//            int PLACE_PICKER_REQUEST = 1;
//            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//            getActivity().startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            if (requestCode == 1 && resultCode == RESULT_OK) {
//                Place place = PlacePicker.getPlace(getActivity(), data);
//                String toastMsg = String.format("Place: %s", place.getName());
//                //  Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
//                if (!place.getAddress().toString().isEmpty()){
//                    address_loc.setText(place.getAddress());
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private String getCertificateNumber() {
        try {
            return "EWR " + ((StepProcessAct) getActivity()).getDate() + "/" + new Random().nextInt(1000) + 10000;
        } catch (Exception e) {
            e.printStackTrace();
            return "EWR – 2018/05/23/5247";
        }
    }

    private boolean isValidate() {
        try {
            if (dwel_size.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Dwelling size", view);
                return false;
            } else if (bed_rooms.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Number of Bedrooms", view);
                return false;
            } else if (legal_desc.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Legal Description", view);
                return false;
            } else if (certificate.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Certificate Title", view);
                return false;
            } else if (address_loc.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Landmark/House Number", view);
                return false;
            }
//
//            else if (city.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter City/Town", view);
//                return false;
//            } else if (state.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter State/Region", view);
//                return false;
//            } else if (post_code.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter ZipCode", view);
//                return false;
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private void setClimateLocation() {
        try {
            ArrayList<String> location = new ArrayList<>();
            location.add("1");
            location.add("2");
            location.add("3A");
            location.add("3B");
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    view.getContext(), android.R.layout.simple_spinner_item, location);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            climate_location.setAdapter(spinnerArrayAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void spinner_values(View v, int id) {
        spinner = v.findViewById(id);
        List age = new ArrayList<Integer>();
        for (int i = 1; i <= 100; i++) {
            age.add(Integer.toString(i));
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(
                v.getContext(), android.R.layout.simple_spinner_item, age);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        PropertyBackTask propertyBackTask = new PropertyBackTask(context);
//        propertyBackTask.execute("add");
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
