package com.nixbymedia.energycalculator;

public class SchemaHelperClass {

    //DATABASE SCHEMA FOR USER TABLE
    public class User{
        public static final String TABLE_NAME_USER = "user_table";
        public static final String USER_ID = "user_id";
        public static final String USERNAME = "username";
        public static final String EMAIL = "email";
        public static final String COMPANY = "company";
        public static final String WEBSITE = "website";
        public static final String MOBILE = "mobile";
        public static final String PASSWORD = "password";
    }


    //DATABASE SCHEMA FOR PROPERTY DETAILS TABLE
    public class Property{
        public static final String TABLE_NAME_PROP = "property_table";
        public static final String PROP_ID = "prop_id";
        public static final String DWEL_TYPE = "dwel_type";
        public static final String DWEL_SIZE = "dwel_size";
        public static final String ROOM_NO = "no_rooms";
        public static final String CLIMATE_LOCATION = "climate_location";
        public static final String LEGAL_DESCP = "legal_descp";
        public static final String CERT_TITLE = "certificate_title";
        public static final String ADDRESS = "address";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String USER_ID = "user_id";
    }

    //DATABASE SCHEMA FOR THERMAL ENVELOPE
    public class Thermal{
        public static final String TABLE_NAME_THERMAL = "thermal_envelope";
        public static final String THERMAL_ID = "thermal_id";
        public static final String POINTS = "points";
        public static final String INDEX_COL = "index_col";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String PROP_ID = "prop_id";
        public static final String USER_ID = "user_id";
    }

    //DATABASE SCHEMA FOR HEAT SYSTEM
    public class Heating{
        public static final String TABLE_NAME_HEAT = "heating_system";
        public static final String HEAT_ID = "heat_id";
        public static final String HP_AIR_PERC = "hp_air_perc";
        public static final String HP_AIR_KW = "hp_air_kw";
        public static final String HP_AIR_IMG = "hp_air_img";
        public static final String HP_HYD_PERC = "hp_hyd_perc";
        public static final String HP_HYD_KW = "hp_hyd_kw";
        public static final String HP_HYD_IMG = "hp_hyd_img";
        public static final String GAS_PERC = "gas_perc";
        public static final String GAS_KW = "gas_kw";
        public static final String GAS_IMG = "gas_img";
        public static final String WOOD_PERC = "wood_perc";
        public static final String WOOD_KW = "wood_kw";
        public static final String WOOD_IMG = "wood_img";
        public static final String ELE_H_PERC = "ele_h_perc";
        public static final String ELE_H_KW = "ele_h_kw";
        public static final String ELE_H_IMG = "ele_h_img";
        public static final String GAS_CAL_KW = "gas_cal_kw";
        public static final String GAS_CAL_EFF = "gas_cal_eff";
        public static final String GAS_CAL_IMG = "gas_cal_img";
        public static final String BACK_BOOSTED = "back_boosted";
        public static final String BACK_BOOSTED_POINTS = "back_boosted_points";
        public static final String BACK_BOOSTED_IMG = "back_boosted_img";
        public static final String HEAT_POINTS = "heat_points";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String PROP_ID = "prop_id";
        public static final String USER_ID = "user_id";
    }

    //DATABASE SCHEMA FOR DAYLIGHT LIGHTING
    public class Daylight{
        public static final String TABLE_NAME_DAYLIGHT = "daylight_lighting";
        public static final String DAYLIGHT_ID = "daylight_id";
        public static final String LUX_METER = "lux_meter";
        public static final String NO_LED = "no_led";
        public static final String NO_LED_IMG = "no_led_img";
        public static final String NO_FLUO = "no_flou";
        public static final String NO_FLUO_IMG = "no_flou_img";
        public static final String NO_HALOG = "no_halog";
        public static final String NO_HALOG_IMG = "no_halog_img";
        public static final String TOTAL_IN_WATT = "total_in_watt";
        public static final String TOTAL_IN_WATT_IMG = "total_in_watt_img";
        public static final String TOTAL_IN_LIGHT = "total_indoor_light";
        public static final String TOTAL_OUT_WATT = "total_out_watt";
        public static final String TOTAL_OUT_WATT_IMG = "total_out_watt_img";
        public static final String WATT_SENSOR = "watt_sensor";
        public static final String WATT_SENSOR_IMG = "watt_sensor_img";
        public static final String LIGHT_INSPECT = "light_inspect";
        public static final String LIGHT_POINTS = "light_points";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String PROP_ID = "prop_id";
        public static final String USER_ID = "user_id";
    }

    //DATABASE SCHEMA FOR WATER USE
    public class WaterUse {
        public static final String TABLE_NAME_WATERUSE = "water_use";
        public static final String WATER_USE_ID = "water_use_id";
        public static final String KITCHEN_TAP = "kitchen_tap";
        public static final String KITCHEN_TAP_POINTS = "kitchen_tap_points";
        public static final String KITCHEN_TAP_IMG = "kichen_tap_img";
        public static final String BASIN_TAP = "basin_tap";
        public static final String BASIN_TAP_POINTS = "basin_tap_points";
        public static final String BASIN_TAP_IMG = "basin_tap_img";
        public static final String SHOWER = "shower";
        public static final String SHOWER_POINTS = "shower_points";
        public static final String SHOWER_IMG = "shower_img";
        public static final String RATES_INSPECT = "rates_inspect";
        public static final String WORST_WC = "worst_wc";
        public static final String WORST_WC_POINTS = "worst_wc_points";
        public static final String DOC_BY_INSPECT = "doc_by_inspect";
        public static final String TOTAL_POINTS = "total_points";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String PROP_ID = "prop_id";
        public static final String USER_ID = "user_id";
    }

    //DATABASE SCHEMA FOR OTHER USE
    public class OtherUse{
        public static final String TABLE_NAME_OTHER_USE = "other_use";
        public static final String OTHER_USE_ID = "other_use_id";
        public static final String SWIM_POOL = "swim_pool";
        public static final String SPA_POOL = "spa_pool";
        public static final String ASSESSORS_CMT = "assessors_cmt";
        public static final String PROP_ID = "prop_id";
        public static final String USER_ID = "user_id";
    }

}
