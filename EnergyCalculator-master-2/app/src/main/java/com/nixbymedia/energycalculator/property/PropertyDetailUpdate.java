package com.nixbymedia.energycalculator.property;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

//import com.google.android.gms.location.places.ui.PlacePicker;
import com.nixbymedia.energycalculator.Activity_Home;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.EditingSection;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.SchemaHelperClass;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.StepProcessActUpdate;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope_Update;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;


public class PropertyDetailUpdate extends Fragment implements View.OnClickListener{

    //Context context;
    private Spinner spinner, dwel_type, climate_location;
    private EditText dwel_size, bed_rooms, legal_desc,
            certificate, address_loc;// city, state, post_code;
    private ImageButton comment, forward, btn_home;
    private ImageView iv_location;
    private TextView cmnt_text;
    private FragmentManager fragmentManager;
    private View view;
    private GoogleApiClient mGoogleApiClient;
    //private Activity ctx;
    private String CertifiNo;
    private int userid;
    PropertyModel propermodel = new PropertyModel();
    DatabaseHelperClass dbhelper;
    public PropertyDetailUpdate() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.dbhelper = new DatabaseHelperClass(getContext());
        this.CertifiNo = getArguments().getString("propertyid");
        this.userid = getArguments().getInt("userid");
        System.out.println("Cert: " +this.CertifiNo);
        System.out.println("ID: " + this.userid);
        view = inflater.inflate(R.layout.fragment_property_detail_update, container, false);
//        mGoogleApiClient = new GoogleApiClient
//                .Builder(getActivity())
//                .addApi(Places)
//                .addApi(Place.PLACE_DETECTION_API)
//                .enableAutoManage(getActivity(), 0,this)
//                .build();


//        iv_location = view.findViewById(R.id.iv_location);
        dwel_type = view.findViewById(R.id.dwelling_type);
        climate_location = view.findViewById(R.id.climate_location);
        dwel_size = view.findViewById(R.id.text_dwelling_size);
        bed_rooms = view.findViewById(R.id.text_bed_rooms);
        legal_desc = view.findViewById(R.id.text_legal_description);
        certificate = view.findViewById(R.id.text_certificate);
        address_loc = view.findViewById(R.id.address);
        // city = view.findViewById(R.id.city_town);
        //  state = view.findViewById(R.id.state);
        //  post_code = view.findViewById(R.id.postal_code);
        // back = view.findViewById(R.id.prop_back);
        comment = view.findViewById(R.id.prop_comment);
        forward = view.findViewById(R.id.prop_forward);
        cmnt_text = view.findViewById(R.id.comment_text_view);
        btn_home = view.findViewById(R.id.btn_home);
        //PropertyBackTask propertyBackTask = new PropertyBackTask(getContext());
        //propertyBackTask.execute("add");
        // back.setEnabled(false);

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*CommentDialog commentDialog = new CommentDialog(getContext());
                commentDialog.show();*/
                ((BaseActivity) getActivity()).openComment(Extra.Tag.property);
            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    setPropertyData();
//                    mGoogleApiClient.stopAutoManage(getActivity());
//                    mGoogleApiClient.disconnect();
                    EditingSection.Iseditinggroup.ispropertyedited = true;
                    Thermal_Envelope_Update thermal_update = new Thermal_Envelope_Update();
                    Bundle bundle = new Bundle();
                    bundle.putString("propertyid", CertifiNo);
                    bundle.putInt("userid", userid);
                    thermal_update.setArguments(bundle);
                    ((StepProcessActUpdate) getContext()).replaceFragment(thermal_update, Extra.Tag.property);
                }
            }
        });
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                mGoogleApiClient.stopAutoManage(getActivity());
//                mGoogleApiClient.disconnect();
                EditingSection.Iseditinggroup.isthermaledited = false;
                EditingSection.Iseditinggroup.ispropertyedited = false;
                EditingSection.Iseditinggroup.isdaylightingedited = false;
                EditingSection.Iseditinggroup.iswateruseedited = false;
                EditingSection.Iseditinggroup.isheatingedited = false;
                Intent intent = new Intent(getContext(), Activity_Home.class);
                startActivity(intent);

            }
        });
        //   spinner_values(view, R.id.climate_location);
        setClimateLocation(propermodel.getClimate_location());
//        iv_location.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openPlacePicker();
//            }
//        });
        this.getProperty(this.userid, this.CertifiNo);
        this.bindtocomponent();
        return view;
    }



    private void setPropertyData() {
        try {
            PropertyModel.getInstance().setDwellingSize(dwel_size.getText().toString());
            PropertyModel.getInstance().setDwellingType(dwel_type.getSelectedItem().toString());
            PropertyModel.getInstance().setNoOfBedRooms(bed_rooms.getText().toString());
            PropertyModel.getInstance().setLegal_description(legal_desc.getText().toString());
            PropertyModel.getInstance().setTitle_certificate(certificate.getText().toString());
            PropertyModel.getInstance().setHouse_no(address_loc.getText().toString());
            PropertyModel.getInstance().setCertificate_number(propermodel.getCertificate_number());
            PropertyModel.getInstance().setClimate_location(climate_location.getSelectedItem().toString());
            PropertyModel.getInstance().setAddress(this.address_loc.getText().toString());
            PropertyModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.property));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openPlacePicker() {
        try {
            int PLACE_PICKER_REQUEST = 1;
//            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//            getActivity().startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1 && resultCode == RESULT_OK) {
//                Place place = PlacePicker.getPlace(getActivity(), data);
//                String toastMsg = String.format("Place: %s", place.getName());
//                //  Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
//                if (!place.getAddress().toString().isEmpty()){
//                    address_loc.setText(place.getAddress());
//                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean isValidate() {
        try {
            if (dwel_size.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getContext()).showError("Please enter Dwelling size", view);
                return false;
            } else if (bed_rooms.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getContext()).showError("Please enter Number of Bedrooms", view);
                return false;
            } else if (legal_desc.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getContext()).showError("Please enter Legal Description", view);
                return false;
            } else if (certificate.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getContext()).showError("Please enter Certificate Title", view);
                return false;
            } else if (address_loc.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getContext()).showError("Please enter Landmark/House Number", view);
                return false;
            }
//
//            else if (city.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter City/Town", view);
//                return false;
//            } else if (state.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter State/Region", view);
//                return false;
//            } else if (post_code.getText().toString().isEmpty()) {
//                ((Activity_Home) getActivity()).showError("Please enter ZipCode", view);
//                return false;
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    private void setClimateLocation(String result) {
        try {
            ArrayList<String> location = new ArrayList<>();
            location.add("1");
            location.add("2");
            location.add("3A");
            location.add("3B");
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                    view.getContext(), android.R.layout.simple_spinner_item, location);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            climate_location.setAdapter(spinnerArrayAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onClick(View view) {

    }


    public void getProperty(int userid, String properID){

        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROPERTYSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", properID);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                        () -> Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Get JSON", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ((StepProcessActUpdate) getContext()).runOnUiThread(
                                        (Runnable) () -> Toast.makeText(getContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {

                            propermodel.setCertificate_number(jsonObject.getString("prop_id"));
                            propermodel.setDwellingType(jsonObject.getString("dwell_type"));
                            propermodel.setDwellingSize(jsonObject.getString("dwell_size"));
                            propermodel.setNoOfBedRooms(jsonObject.getString("room_no"));
                            propermodel.setClimate_location(jsonObject.getString("climate_location"));
                            propermodel.setLegal_description(jsonObject.getString("legal_desc"));
                            propermodel.setTitle_certificate(jsonObject.getString("cert_title"));
                            propermodel.setAddress(jsonObject.getString("address"));
                            propermodel.setComment(jsonObject.getString("asstcomment"));
                            ((StepProcessActUpdate) getContext()).runOnUiThread(
                                    (Runnable) () -> bindtocomponent()
                            );

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ((StepProcessActUpdate) getActivity()).runOnUiThread(
                            () -> Toast.makeText(getContext(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    public void bindtocomponent(){
        if(!EditingSection.Iseditinggroup.ispropertyedited){
            dwel_size.setText(propermodel.getDwellingSize());
            bed_rooms.setText(propermodel.getNoOfBedRooms());
            legal_desc.setText(propermodel.getLegal_description());
            certificate.setText(propermodel.getTitle_certificate());
            address_loc.setText(propermodel.getAddress());
            for(int i = 0; i < dwel_type.getCount(); i++){
                if(dwel_type.getItemAtPosition(i).toString().equals(propermodel.getDwellingType())){
                    dwel_type.setSelection(i);
                }
            }
            for(int i = 0; i < climate_location.getCount(); i++){
                if(climate_location.getItemAtPosition(i).toString().equals(propermodel.getClimate_location())){
                    climate_location.setSelection(i);
                }
            }
            BaseActivity.saveComment(propermodel.getComment(), Extra.Tag.property);
        }else{
            dwel_size.setText(PropertyModel.getInstance().getDwellingSize());
            bed_rooms.setText(PropertyModel.getInstance().getNoOfBedRooms());
            legal_desc.setText(PropertyModel.getInstance().getLegal_description());
            certificate.setText(PropertyModel.getInstance().getTitle_certificate());
            address_loc.setText(PropertyModel.getInstance().getAddress());
            for(int i = 0; i < dwel_type.getCount(); i++){
                if(dwel_type.getItemAtPosition(i).toString().equals(PropertyModel.getInstance().getDwellingType())){
                    dwel_type.setSelection(i);
                }
            }
            for(int i = 0; i < climate_location.getCount(); i++){
                if(climate_location.getItemAtPosition(i).toString().equals(PropertyModel.getInstance().getClimate_location())){
                    climate_location.setSelection(i);
                }
            }

            BaseActivity.saveComment(PropertyModel.getInstance().getComment(), Extra.Tag.property);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}