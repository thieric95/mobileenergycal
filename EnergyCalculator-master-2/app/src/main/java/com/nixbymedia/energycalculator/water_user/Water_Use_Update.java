package com.nixbymedia.energycalculator.water_user;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nixbymedia.energycalculator.Activity_Result;
import com.nixbymedia.energycalculator.BackgroundOperations.WaterUseBackTask;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.EditingSection;
import com.nixbymedia.energycalculator.FileModel;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.SchemaHelperClass;
import com.nixbymedia.energycalculator.Splash_Screen_Process;
import com.nixbymedia.energycalculator.Splash_Screen_Process_Update;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.StepProcessActUpdate;
import com.nixbymedia.energycalculator.extra.Debugger;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.lighting.DayLight_Lightening;
import com.nixbymedia.energycalculator.lighting.Daylight_LighteningUpdate;
import com.nixbymedia.energycalculator.property.PropertyModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class Water_Use_Update extends Fragment {

    private ImageButton image1, image2, image3, back, forward, comment;
    private EditText kitchen_tap, worst_basin_tap, worst_shower;
    private RadioButton rb1, rb2;
    private Spinner spinner1;
    private TextView cmnt;
    private WaterUseBackTask waterUseBackTask;
    private String rbst1, rbst2;
    private FragmentManager fragmentManager;
    private View view;
    private Activity ctx;
    private int userid;
    private String CertifiNo;
    WaterUseModel watermodel = new WaterUseModel();
    DatabaseHelperClass dbhelper;
    public Water_Use_Update() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.CertifiNo = getArguments().getString("propertyid");
        this.userid = getArguments().getInt("userid");
        // Inflate the layout for this fragment
        this.dbhelper = new DatabaseHelperClass(getContext());
        this.getWaterUser(this.userid, this.CertifiNo);
        view = inflater.inflate(R.layout.fragment_water__use__update, container, false);
        image1 = view.findViewById(R.id.img_kitchen_tap);
        image2 = view.findViewById(R.id.img_basin_tap);
        image3 = view.findViewById(R.id.img_worst_shower);

        cmnt = view.findViewById(R.id.water_comment_text);

        kitchen_tap = view.findViewById(R.id.text_kitchen_tap);
        worst_basin_tap = view.findViewById(R.id.text_basin_tap);
        worst_shower = view.findViewById(R.id.text_worst_shower);

        rb1 = view.findViewById(R.id.rb_flow_inspect);
        rb2 = view.findViewById(R.id.rb_doc_inspect);

        spinner1 = view.findViewById(R.id.spinner_heat_to_air_water);

        back = view.findViewById(R.id.water_back);
        forward = view.findViewById(R.id.water_forward);
        comment = view.findViewById(R.id.water_comment);
        rb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbst1 = "YES";
                } else {
                    rbst1 = "NO";
                }
            }
        });

        rb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbst2 = "YES";
                } else {
                    rbst2 = "NO";
                }
            }
        });


        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.basin_tap + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessActUpdate) getActivity()).openCamera(Extra.FileConstants.basin_tap, Extra.SubPath.Water, getActivity());
                }
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.basin_tap + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessActUpdate) getActivity()).openCamera(Extra.FileConstants.basin_tap, Extra.SubPath.Water, getActivity());
                }
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.worst_shower + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessActUpdate) getActivity()).openCamera(Extra.FileConstants.worst_shower, Extra.SubPath.Water, getActivity());
                }
            }
        });
//
//        waterUseBackTask = new WaterUseBackTask(getContext(), img1, img2, img3);
//        waterUseBackTask.execute("remove");

        fragmentManager = getActivity().getSupportFragmentManager();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Daylight_LighteningUpdate daylight_update = new Daylight_LighteningUpdate();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
                bundle.putInt("userid", userid);
                daylight_update.setArguments(bundle);
                ((StepProcessActUpdate) getActivity()).replaceFragment(daylight_update, Extra.Tag.Daylight);
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StepProcessActUpdate) getActivity()).openComment(Extra.Tag.WaterUse);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    EditingSection.Iseditinggroup.iswateruseedited = true;
                    savePoints();
                    Intent intent = new Intent(getActivity(), Splash_Screen_Process_Update.class);
                    intent.putExtra("option", "updating");
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        bindingtocomponent();
        return view;
    }

    private boolean isValidate() {
        try {
            if (kitchen_tap.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Kitchen Tap", view);
                return false;
            } else if (worst_basin_tap.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Worst Basin Tap", view);
                return false;
            } else if (worst_shower.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Worst Shower", view);
                return false;
            } else
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void savePoints() {
        try {
            saveShowerPoint();
            saveKitchenPoint();
            saveBathroomPoint();
            saveToiletPoint();
            this.saveInspector();
            float total_points = WaterUseModel.getInstance().getBasin_tap_point()
                    + WaterUseModel.getInstance().getKitchen_tap_point()
                    + WaterUseModel.getInstance().getShower_point()
                    + WaterUseModel.getInstance().getWorst_Wc();
            WaterUseModel.getInstance().setTotal_points(total_points);
            WaterUseModel.getInstance().setId(watermodel.getId());
            WaterUseModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.WaterUse));
            Debugger.debugE("water", WaterUseModel.getInstance().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveShowerPoint() {
        try {
            if (Float.parseFloat(worst_shower.getText().toString()) < 7.5) {
                WaterUseModel.getInstance().setShower_point(4f);
            } else if (Float.parseFloat(worst_shower.getText().toString()) < 9) {
                WaterUseModel.getInstance().setShower_point(3.5f);
            } else if (Float.parseFloat(worst_shower.getText().toString()) > 9) {
                WaterUseModel.getInstance().setShower_point(0f);
            }
            WaterUseModel.getInstance().setShower(worst_shower.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveKitchenPoint() {
        try {
            if (Float.parseFloat(kitchen_tap.getText().toString()) < 5) {
                WaterUseModel.getInstance().setKitchen_tap_point(1.5f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) < 6) {
                WaterUseModel.getInstance().setKitchen_tap_point(1f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) < 7.5) {
                WaterUseModel.getInstance().setKitchen_tap_point(0.5f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) > 7.5) {
                WaterUseModel.getInstance().setKitchen_tap_point(0f);
            }
            WaterUseModel.getInstance().setKitchen_tap(kitchen_tap.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveBathroomPoint() {
        try {
            if (Float.parseFloat(worst_basin_tap.getText().toString()) < 5) {
                WaterUseModel.getInstance().setBasin_tap_point(1.5f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) < 6) {
                WaterUseModel.getInstance().setBasin_tap_point(1f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) < 7.5) {
                WaterUseModel.getInstance().setBasin_tap_point(0.5f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) > 7.5) {
                WaterUseModel.getInstance().setBasin_tap_point(0f);
            }
            WaterUseModel.getInstance().setBasin_tap(worst_basin_tap.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //start from saveToiletPoint.
    private void saveToiletPoint() {
        try {
            Debugger.debugE("selectedItem", spinner1.getSelectedItem().toString());
            switch (getResources().getStringArray(R.array.water_wc)[spinner1.getSelectedItemPosition()]) {
                case "> 4.5 WELS":
                    WaterUseModel.getInstance().setWorst_Wc(3f);
                    break;
                case "4 star WELS":
                    WaterUseModel.getInstance().setWorst_Wc(2f);
                    break;
                case "3 star WELS":
                    WaterUseModel.getInstance().setWorst_Wc(1f);
                    break;
                case "single flush":
                    WaterUseModel.getInstance().setWorst_Wc(0f);
                    break;
            }
            WaterUseModel.getInstance().setWc_point(spinner1.getSelectedItem().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveInspector(){
        try{
            WaterUseModel.getInstance().setMeasuredInspect(this.rbst1);
            WaterUseModel.getInstance().setDocInspect(this.rbst2);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void getWaterUser(int userid, String properID){

        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLWATERUSESELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", properID);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                        () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Response", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                        () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                                );
                            }
                        } else {
                            watermodel.setId(jsonObject.getInt("wateruseid"));
                            watermodel.setKitchen_tap(jsonObject.get("kitchen_tap").toString());
                            watermodel.setKitchen_tap_point(BigDecimal.valueOf(jsonObject.getDouble("kitchen_tap_points")).floatValue());
                            if(jsonObject.get("kitchen_tap_img") == null){
                                watermodel.setImage_1(null);
                            }else{
                                watermodel.setImage_1(jsonObject.getString("kitchen_tap_img").getBytes());
                            }
                            watermodel.setBasin_tap(jsonObject.get("basin_tap").toString());
                            watermodel.setBasin_tap_point(BigDecimal.valueOf(jsonObject.getDouble("basin_tap_points")).floatValue());
                            if(jsonObject.get("basin_tap_img") == null){
                                watermodel.setImage_2(null);
                            }else{
                                watermodel.setImage_2(jsonObject.getString("basin_tap_img").getBytes());
                            }
                            watermodel.setShower(jsonObject.get("shower").toString());
                            watermodel.setShower_point(BigDecimal.valueOf(jsonObject.getDouble("shower_points")).floatValue());
                            if(jsonObject.get("shower_img") == null){
                                watermodel.setImage_3(null);
                            }else{
                                watermodel.setImage_3(jsonObject.getString("shower_img").getBytes());
                            }
                            watermodel.setMeasuredInspect(jsonObject.getString("rates_inspect"));
                            watermodel.setWc_point(jsonObject.getString("worst_wc"));
                            watermodel.setWorst_Wc_point(BigDecimal.valueOf(jsonObject.getDouble("worst_wc_points")).floatValue());
                            watermodel.setDocInspect(jsonObject.getString("doc_by_inspect"));
                            watermodel.setTotal_points(BigDecimal.valueOf(jsonObject.getDouble("total_points")).floatValue());
                            watermodel.setComment(jsonObject.getString("asstcomment"));
                            ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                    (Runnable) () -> bindingtocomponent()
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ctx.runOnUiThread(
                            () -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }


    public void bindingtocomponent(){
        if(!EditingSection.Iseditinggroup.iswateruseedited){
            kitchen_tap.setText(watermodel.getKitchen_tap());
            worst_basin_tap.setText(watermodel.getBasin_tap());
            worst_shower.setText(watermodel.getShower());
            this.rbst1 = watermodel.getMeasuredInspect();
            this.rbst2 = watermodel.getDocInspect();
            for(int i = 0; i < spinner1.getCount(); i++){
                if(spinner1.getItemAtPosition(i).toString().equals(watermodel.getWc_point())){
                    spinner1.setSelection(i);
                }
            }
            if(this.rbst1 != null){
                if(this.rbst1.equals("YES")){
                    this.rb1.setChecked(true);
                }
            }
            if(this.rbst2 != null){
                if(this.rbst2.equals("YES")){
                    this.rb2.setChecked(true);
                }
            }
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.kitchen_tap, watermodel.getImage_1());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.basin_tap, watermodel.getImage_2());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.worst_shower, watermodel.getImage_3());

            BaseActivity.saveComment(watermodel.getComment(), Extra.Tag.WaterUse);
            WaterUseModel.getInstance().setId(watermodel.getId());
        }else{
            kitchen_tap.setText(WaterUseModel.getInstance().getKitchen_tap());
            worst_basin_tap.setText(WaterUseModel.getInstance().getBasin_tap());
            worst_shower.setText(Float.toString(WaterUseModel.getInstance().getWorst_Wc()));
            this.rbst1 = WaterUseModel.getInstance().getMeasuredInspect();
            this.rbst2 = WaterUseModel.getInstance().getDocInspect();
            for(int i = 0; i < spinner1.getCount(); i++){
                if(spinner1.getItemAtPosition(i).toString().equals(WaterUseModel.getInstance().getWc_point())){
                    spinner1.setSelection(i);
                }
            }
            if(this.rbst1 != null){
                if(this.rbst1.equals("YES")){
                    this.rb1.setChecked(true);
                }
            }
            if(this.rbst2 != null){
                if(this.rbst2.equals("YES")){
                    this.rb2.setChecked(true);
                }
            }

            BaseActivity.saveComment(WaterUseModel.getInstance().getComment(), Extra.Tag.WaterUse);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}