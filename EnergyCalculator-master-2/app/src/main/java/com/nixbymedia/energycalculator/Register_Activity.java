package com.nixbymedia.energycalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


public class Register_Activity extends AppCompatActivity {

    Button btnLogin, btnSignUp;
    EditText mobilenumber, password, username, email, website, company;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_);

        btnSignUp = findViewById(R.id.signUp_reg);
        btnLogin = findViewById(R.id.signIn_reg);
        password = findViewById(R.id.password_reg);
        mobilenumber = findViewById(R.id.mobile_number_reg);
        username = findViewById(R.id.usernametxt);
        email = findViewById(R.id.emailtxt);
        website = findViewById(R.id.websitetxt);
        company = findViewById(R.id.companytxt);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DatabaseBackTask databaseBackTask = new DatabaseBackTask(Register_Activity.this);
                ServerCall serCall = new ServerCall(Register_Activity.this);
                if(!mobilenumber.getText().toString().equals("") && !password.getText().toString().equals("")
                        && !username.getText().toString().equals("") && !email.getText().toString().equals("")){

                    serCall.register(username.getText().toString(), email.getText().toString(), company.getText().toString(),
                            website.getText().toString(), mobilenumber.getText().toString(), password.getText().toString());
                }else{
                    Toast.makeText(getApplicationContext(), "Please Complete filling you name, email, mobile and password", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register_Activity.this, Login_Activity.class));
                overridePendingTransition(R.anim.animation1,R.anim.animation2);
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register_Activity.this, Login_Activity.class));
        overridePendingTransition(R.anim.animation1,R.anim.animation2);
        finish();
    }
}
