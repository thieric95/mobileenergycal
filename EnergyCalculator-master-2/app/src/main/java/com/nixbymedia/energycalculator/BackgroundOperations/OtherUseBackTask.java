package com.nixbymedia.energycalculator.BackgroundOperations;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.Toast;

import com.nixbymedia.energycalculator.DatabaseHelperClass;

public class OtherUseBackTask extends AsyncTask<String, String, String> {

    Activity ctx;
    private int user_id;
    private String pro_id;

    public OtherUseBackTask(Context context){
        this.ctx = (Activity) context;

    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    @Override
    protected String doInBackground(String... params) {
        String method = params[0];
        DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(ctx);
//        if(method.equals("add")){
//
//            databaseHelperClass.addOther(db, params[1], params[2], params[3], this.pro_id, this.user_id);
//            return "added";
//        }else if(method.equals("remove")){
//            SQLiteDatabase db = databaseHelperClass.getWritableDatabase();
//            databaseHelperClass.deleteOther(db);
//            return "removed";
//        }else if(method.equals("updated")){
//            SQLiteDatabase db = databaseHelperClass.getWritableDatabase();
//            return "updated";
//        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s != null){
            if(s.equals("added")){
                Toast.makeText(ctx, "Internal Water Use Data Saved", Toast.LENGTH_SHORT).show();
            }else if(s.equals("updated")){
                Toast.makeText(ctx, "Day Light Data Updated", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(ctx, "Error", Toast.LENGTH_SHORT).show();
        }
    }

}
