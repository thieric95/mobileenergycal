package com.nixbymedia.energycalculator.property;

public class PropertyModel {
    private String dwellingSize = "", dwellingType = "", noOfBedRooms = "", climate_location = "", legal_description = "", title_certificate = "", house_no = "", city = "", state = "", zipcode = "", address = "";
    private String certificate_number = "", comment = "";

    public PropertyModel() {
    }

    public PropertyModel(String certificate_number, String dwellingType, String dwellingSize, String noOfBedRooms, String climate_location,
                         String legal_description, String title_certificate, String address, String comment) {
        this.dwellingSize = dwellingSize;
        this.dwellingType = dwellingType;
        this.noOfBedRooms = noOfBedRooms;
        this.climate_location = climate_location;
        this.legal_description = legal_description;
        this.title_certificate = title_certificate;
        this.address = address;
        this.certificate_number = certificate_number;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getDwellingType() {
        return dwellingType;
    }

    public void setDwellingType(String dwellingType) {
        this.dwellingType = dwellingType;
    }

    public String getNoOfBedRooms() {
        return noOfBedRooms;
    }

    public void setNoOfBedRooms(String noOfBedRooms) {
        this.noOfBedRooms = noOfBedRooms;
    }

    public String getClimate_location() {
        return climate_location;
    }

    public void setClimate_location(String climate_location) {
        this.climate_location = climate_location;
    }

    public String getLegal_description() {
        return legal_description;
    }

    public void setLegal_description(String legal_description) {
        this.legal_description = legal_description;
    }

    public String getTitle_certificate() {
        return title_certificate;
    }

    public void setTitle_certificate(String title_certificate) {
        this.title_certificate = title_certificate;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public static PropertyModel getModel() {
        return model;
    }

    public static void setModel(PropertyModel model) {
        PropertyModel.model = model;
    }

    private static PropertyModel model = new PropertyModel();

    public static PropertyModel getInstance() {
        return model;
    }

    public String getDwellingSize() {
        return dwellingSize;
    }

    public void setDwellingSize(String dwellingSize) {
        this.dwellingSize = dwellingSize;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "PropertyModel{" +
                "dwellingSize='" + dwellingSize + '\'' +
                ", dwellingType='" + dwellingType + '\'' +
                ", noOfBedRooms='" + noOfBedRooms + '\'' +
                ", climate_location='" + climate_location + '\'' +
                ", legal_description='" + legal_description + '\'' +
                ", title_certificate='" + title_certificate + '\'' +
                ", house_no='" + house_no + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", address='" + address + '\'' +
                ", certificate_number='" + certificate_number + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
