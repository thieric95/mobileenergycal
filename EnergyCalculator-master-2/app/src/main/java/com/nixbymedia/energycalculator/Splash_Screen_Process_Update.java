package com.nixbymedia.energycalculator;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.nixbymedia.energycalculator.BackgroundOperations.DayLightBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.HeatingBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.PropertyBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.ThermalBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.WaterUseBackTask;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.heatingsystem.WaterHeating;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import static java.lang.Thread.sleep;

public class Splash_Screen_Process_Update extends AppCompatActivity {
    ProgressBar progressBar;
    SessionManagement session;
    private int userid;
    private String pro_id;
    DatabaseHelperClass dbhelper;
    private String option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen__process__update);
        progressBar = findViewById(R.id.progress_splash);

        FadingCircle wave = new FadingCircle();
        progressBar.setIndeterminateDrawable(wave);
        this.dbhelper = new DatabaseHelperClass(this);
        this.session = new SessionManagement(getApplicationContext());
        if (this.session.checkforlogin()) {
            finish();
        }
        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
        Intent intent = getIntent();
        if (intent != null) {
            this.option = intent.getStringExtra("option");
            if (this.option.equals("updating")) {

                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    update_property();
                }, 1500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    update_thermal_envelope();
                }, 2500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    update_heatingsystem();
                }, 3500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    update_daylightlightening();
                }, 4500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    update_wateruse();
                }, 5500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    Intent intent1 = new Intent(Splash_Screen_Process_Update.this, Activity_Result.class);
                    intent1.putExtra("option", "update");
                    intent1.putExtra("propertycert", PropertyModel.getInstance().getCertificate_number());
                    startActivity(intent1);
                }, 7400);
            }
        }
    }

    private void update_property() {

        PropertyBackTask propertytask = new PropertyBackTask(Splash_Screen_Process_Update.this);
        propertytask.setUser_id(this.userid);
        PropertyModel.getInstance().setCertificate_number(PropertyModel.getInstance().getCertificate_number());
        PropertyModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.property));
        propertytask.update(PropertyModel.getInstance().getCertificate_number(), PropertyModel.getInstance().getDwellingType(),
                PropertyModel.getInstance().getDwellingSize(), PropertyModel.getInstance().getNoOfBedRooms(), PropertyModel.getInstance().getClimate_location(),
                PropertyModel.getInstance().getLegal_description(), PropertyModel.getInstance().getTitle_certificate(), PropertyModel.getInstance().getAddress(),
                PropertyModel.getInstance().getComment());
    }

    private void update_wateruse() {
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.kitchen_tap)) {
            WaterUseModel.getInstance().setImage_1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.kitchen_tap));
        } else {
            WaterUseModel.getInstance().setImage_1(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.basin_tap)) {
            WaterUseModel.getInstance().setImage_2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.basin_tap));
        } else {
            WaterUseModel.getInstance().setImage_2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.worst_shower)) {
            WaterUseModel.getInstance().setImage_3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.worst_shower));
        } else {
            WaterUseModel.getInstance().setImage_3(this.convertImageToByte());
        }
        WaterUseBackTask waterusetask = new WaterUseBackTask(Splash_Screen_Process_Update.this, WaterUseModel.getInstance().getImage_1(),
                WaterUseModel.getInstance().getImage_2(), WaterUseModel.getInstance().getImage_3());
        waterusetask.setUser_id(this.userid);
        waterusetask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        WaterUseModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.WaterUse));

        waterusetask.update(WaterUseModel.getInstance().getKitchen_tap(),
                Float.toString(WaterUseModel.getInstance().getKitchen_tap_point()),
                WaterUseModel.getInstance().getBasin_tap(),
                Float.toString(WaterUseModel.getInstance().getBasin_tap_point()),
                WaterUseModel.getInstance().getShower(),
                Float.toString(WaterUseModel.getInstance().getShower_point()),
                WaterUseModel.getInstance().getMeasuredInspect(),
                WaterUseModel.getInstance().getWc_point(),
                Float.toString(WaterUseModel.getInstance().getWorst_Wc_point()),
                WaterUseModel.getInstance().getDocInspect(),
                Float.toString(WaterUseModel.getInstance().getTotal_points())
                , WaterUseModel.getInstance().getComment(),
                Integer.toString(WaterUseModel.getInstance().getId()));
    }

    private void update_heatingsystem() {
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.heatair)) {
            HeatModel.getInstance().setImage1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heatair));
        } else {
            HeatModel.getInstance().setImage1(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.heathydronic)) {
            HeatModel.getInstance().setImage2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heathydronic));
        } else {
            HeatModel.getInstance().setImage2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.gas)) {
            HeatModel.getInstance().setImage3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.gas));
        } else {
            HeatModel.getInstance().setImage3(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.naturalgas)) {
            HeatModel.getInstance().setImage4(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.naturalgas));
        } else {
            HeatModel.getInstance().setImage4(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Wood)) {
            HeatModel.getInstance().setImage5(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wood));
        } else {
            HeatModel.getInstance().setImage5(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.ElectricHeater)) {
            HeatModel.getInstance().setImage6(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.ElectricHeater));
        } else {
            HeatModel.getInstance().setImage6(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Solar)) {
            HeatModel.getInstance().setImage7(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Solar));
        } else {
            HeatModel.getInstance().setImage7(this.convertImageToByte());
        }
        HeatingBackTask heatingtask = new HeatingBackTask(Splash_Screen_Process_Update.this, HeatModel.getInstance().getImage1(), HeatModel.getInstance().getImage2(),
                HeatModel.getInstance().getImage3(), HeatModel.getInstance().getImage4(), HeatModel.getInstance().getImage5(),
                HeatModel.getInstance().getImage6(), HeatModel.getInstance().getImage7());
        heatingtask.setUser_id(this.userid);
        heatingtask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        HeatModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.HeatingSystem));
        heatingtask.update(HeatModel.getInstance().getHeat_pump_to_air(),
                HeatModel.getInstance().getHeat_pump_to_air_kw()
                , HeatModel.getInstance().getHeat_pump_hydrolic(),
                HeatModel.getInstance().getHeat_pump_hydrolic_kw()
                , HeatModel.getInstance().getLpg_gas(),
                HeatModel.getInstance().getLpg_gas_kw(),
                HeatModel.getInstance().getNatural_gas(),
                HeatModel.getInstance().getNatural_gas_kw(),
                HeatModel.getInstance().getWood_burner(),
                HeatModel.getInstance().getWood_burner_kw(),
                HeatModel.getInstance().getEletric_heater(),
                HeatModel.getInstance().getEletric_heater_kw(),
                WaterHeating.getInstance().getWater_heating(),
                Float.toString(WaterHeating.getInstance().getPoints()),
                Float.toString(HeatModel.getInstance().getPoints()),
                HeatModel.getInstance().getComment(),
                Integer.toString(HeatModel.getInstance().getId()));
    }

    private void update_daylightlightening() {

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.LED_Lights)) {
            LightingModel.getInstance().setImage_1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.LED_Lights));
        } else {
            LightingModel.getInstance().setImage_1(this.convertImageToByte());
        }


        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Fluorescent_Lights)) {
            LightingModel.getInstance().setImage_2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Fluorescent_Lights));
        } else {
            LightingModel.getInstance().setImage_2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Halogen_lights)) {
            LightingModel.getInstance().setImage_3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Halogen_lights));
        } else {
            LightingModel.getInstance().setImage_3(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Indoor_Light)) {
            LightingModel.getInstance().setImage_4(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Indoor_Light));
        } else {
            LightingModel.getInstance().setImage_4(null);
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Outdoor_Light)) {
            LightingModel.getInstance().setImage_5(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Outdoor_Light));
        } else {
            LightingModel.getInstance().setImage_5(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Wattage_Sensor)) {
            LightingModel.getInstance().setImage_6(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wattage_Sensor));
        } else {
            LightingModel.getInstance().setImage_6(this.convertImageToByte());
        }
        DayLightBackTask daylighttask = null;
        if (LightingModel.getInstance().getImage_4() != null) {
            daylighttask = new DayLightBackTask(Splash_Screen_Process_Update.this, LightingModel.getInstance().getImage_1(), LightingModel.getInstance().getImage_2(),
                    LightingModel.getInstance().getImage_3(), LightingModel.getInstance().getImage_4(), LightingModel.getInstance().getImage_5()
                    , LightingModel.getInstance().getImage_6());
            daylighttask.setUser_id(this.userid);
            daylighttask.setPro_id(PropertyModel.getInstance().getCertificate_number());
            LightingModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.Daylight));
            daylighttask.update(LightingModel.getInstance().getLux_meter(),
                    LightingModel.getInstance().getLed_light(),
                    LightingModel.getInstance().getCfl_light()
                    , LightingModel.getInstance().getHalogen_light(),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_light()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_wattage()),
                    Integer.toString(LightingModel.getInstance().getSensor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_light()),
                    LightingModel.getInstance().getInspectorlightview(),
                    Float.toString(LightingModel.getInstance().getTotal_points()),
                    LightingModel.getInstance().getComment(),
                    Integer.toString(LightingModel.getInstance().getId()));
        } else {
            daylighttask = new DayLightBackTask(Splash_Screen_Process_Update.this, null, null,
                    null, LightingModel.getInstance().getImage_4(), LightingModel.getInstance().getImage_5()
                    , LightingModel.getInstance().getImage_6());
            daylighttask.setUser_id(this.userid);
            daylighttask.setPro_id(PropertyModel.getInstance().getCertificate_number());
            LightingModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.Daylight));
            daylighttask.update(LightingModel.getInstance().getLux_meter(),
                    LightingModel.getInstance().getLed_light(),
                    LightingModel.getInstance().getCfl_light()
                    , LightingModel.getInstance().getHalogen_light(),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_light()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_wattage()),
                    Integer.toString(LightingModel.getInstance().getSensor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_light()),
                    LightingModel.getInstance().getInspectorlightview(),
                    Float.toString(LightingModel.getInstance().getTotal_points()),
                    LightingModel.getInstance().getComment(),
                    Integer.toString(LightingModel.getInstance().getId()));
        }


    }

    private void update_thermal_envelope() {
        ThermalBackTask thermaltask = new ThermalBackTask(Splash_Screen_Process_Update.this);
        thermaltask.setUser_id(this.userid);
        thermaltask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        ThermalEnvelope.getInstance().setComment(BaseActivity.getComment(Extra.Tag.thermal));
        thermaltask.update(ThermalEnvelope.getInstance().getIndex(), Integer.toString(ThermalEnvelope.getInstance().getPoints())
                , ThermalEnvelope.getInstance().getComment(),
                Integer.toString(ThermalEnvelope.getInstance().getId()));
    }

    public byte[] convertImageToByte() {
        byte[] data = null;
        try {
            Drawable drawable = this.getResources().getDrawable(R.drawable.unavailable_image);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}