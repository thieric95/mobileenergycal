package com.nixbymedia.energycalculator.heatingsystem;

public class WaterHeating {
    String water_heating = "";
    float points = 0f;

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    private static WaterHeating waterHeating = new WaterHeating();

    public static  WaterHeating getInstance(){
        return waterHeating;
    }

    public String getWater_heating() {
        return water_heating;
    }

    public void setWater_heating(String water_heating) {
        this.water_heating = water_heating;
    }

    @Override
    public String toString() {
        return "WaterHeating{" +
                "water_heating='" + water_heating + '\'' +
                ", points=" + points +
                '}';
    }
}
