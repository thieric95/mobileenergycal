package com.nixbymedia.energycalculator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.app.AppCompatDelegate;
//import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


public class Login_Activity extends AppCompatActivity {
    Button btnSignUp, btnLogin;
    EditText password, mobilenumber;
    SessionManagement session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.signIn);
        btnSignUp = findViewById(R.id.signUp);
        password = findViewById(R.id.password);
        mobilenumber = findViewById(R.id.mobile_number);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobile,passwords;
                mobile = mobilenumber.getText().toString();
                passwords = password.getText().toString();
                ServerCall serCall = new ServerCall(Login_Activity.this);
                serCall.login(mobile, passwords);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent
                        (Login_Activity.this, Register_Activity.class));
                overridePendingTransition(R.anim.animation4, R.anim.animation3);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mobilenumber.setText("");
        this.password.setText("");
    }
}
