package com.nixbymedia.energycalculator.heatingsystem;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.EditingSection;
import com.nixbymedia.energycalculator.FileModel;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.StepProcessActUpdate;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.lighting.Daylight_LighteningUpdate;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope_Update;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Heating_SystemUpdate extends Fragment {

    ImageButton back, comment_btn, forward;
    FragmentManager fragmentManager;
    Spinner spinner, spinner_heat_air, spinner_natural_gas, spinner_hydronic, spinner_gas,
            spinner_elec_heater, spinner_wood, spinner_solar;//spinner_gas_califont

    ImageButton image_heat_to_air, image_heat_hydronic, image_gas, img_natural_gas,
            image_ele_heater, image_wood, image_solar;
    ;// image_hot_water_cylinder,image_gas_califont;

    byte[] image_heatpump_ = null, image_heat_hydronic_ = null, image_gas_califont_ = null,
            image_gas_ = null, image_ele_heater_ = null, image_wood_ = null, image_solar_ = null, image_hot_water_cylinder_ = null;

    EditText text_heat_to_air, text_heat_hydronic, text_gas_natural_gas,
            text_lpg_gas, text_ele_heater, text_wood;
    TextView cmnt_text;

    HashMap<String, Float> water_heating;
    private Activity ctx;
    private RequestQueue reqQueue;
    private int userid;
    private String CertifiNo;
    HeatModel heatmodel = new HeatModel();
    WaterHeating waterheating = new WaterHeating();
    DatabaseHelperClass dbhelper;

    public Heating_SystemUpdate() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.CertifiNo = getArguments().getString("propertyid");
        this.userid = getArguments().getInt("userid");
        // Inflate the layout for this fragment
        this.dbhelper = new DatabaseHelperClass(getContext());
        this.reqQueue = Volley.newRequestQueue(getContext());
        water_heating = new HashMap<>();

        View view = inflater.inflate(R.layout.fragment_heating__system_update, container, false);
        this.getHeating(view, this.userid, this.CertifiNo);
        // spinner_values(view, R.id.spinner_gas_califont_efficiency);
        back = view.findViewById(R.id.heat_back);
        comment_btn = view.findViewById(R.id.heat_comment);
        forward = view.findViewById(R.id.heat_forward);

        //spinners to select data is initialized
        //    spinner_water_heating = view.findViewById(R.id.spinner_water_heating);
        spinner_natural_gas = view.findViewById(R.id.spinner_natural_gas);
        spinner_heat_air = view.findViewById(R.id.spinner_heat_to_air);
        spinner_hydronic = view.findViewById(R.id.spinner_heat_hydronic);
        spinner_gas = view.findViewById(R.id.spinner_gas);
        spinner_wood = view.findViewById(R.id.spinner_wood_pellet);
        spinner_elec_heater = view.findViewById(R.id.spinner_electric_heater);
        //spinner_gas_califont = view.findViewById(R.id.spinner_gas_califont_efficiency);
        spinner_solar = view.findViewById(R.id.spinner_back_boosted);
        setWaterHeating();
        //initialized edit text to take input from user
        text_heat_to_air = view.findViewById(R.id.text_heat_to_air);
        text_heat_hydronic = view.findViewById(R.id.text_hydronic);
        text_lpg_gas = view.findViewById(R.id.text_lpg_gas);
        text_wood = view.findViewById(R.id.text_wood_pellet);
        text_ele_heater = view.findViewById(R.id.text_electric_heater);
        text_gas_natural_gas = view.findViewById(R.id.text_natural_gas);
        //   text_hot_water_cylinder = view.findViewById(R.id.text_hot_water_cylinder);
        //  kw_hot_w_cylinder = view.findViewById(R.id.kw_hot_water_cylinder);
        //image buttons to capture images are initialized
        img_natural_gas = view.findViewById(R.id.img_natural_gas);
        image_heat_to_air = view.findViewById(R.id.img_heatpump);
        image_heat_hydronic = view.findViewById(R.id.img_heatpumpair);
        image_gas = view.findViewById(R.id.img_gas);
        image_wood = view.findViewById(R.id.img_wood);
        image_ele_heater = view.findViewById(R.id.img_ele_heater);
        //    image_gas_califont = view.findViewById(R.id.img_gas_calfont);
        // image_hot_water_cylinder = view.findViewById(R.id.img_hot_water);
        image_solar = view.findViewById(R.id.img_solar);

        cmnt_text = view.findViewById(R.id.comment_heating_text);
        this.bindingtocomponent();
//        HeatingBackTask heatingBackTask = new HeatingBackTask(getContext(), null, null, null,
//                null, null, null, null, null);
//        heatingBackTask.execute("remove");


        // these functions are overridden to capture the image from camera

        image_heat_to_air.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heatair + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.heatair, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        img_natural_gas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.gas + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.gas, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_heat_hydronic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heathydronic + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.heathydronic, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_gas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.gas + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_wood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wood + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Wood, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_ele_heater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.ElectricHeater + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.ElectricHeater, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });

        image_solar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Solar + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Solar, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();
        back.setEnabled(true);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thermal_Envelope_Update thermal_update = new Thermal_Envelope_Update();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
                bundle.putInt("userid", userid);
                thermal_update.setArguments(bundle);
                ((StepProcessActUpdate) getActivity()).replaceFragment(thermal_update, Extra.Tag.property);
            }
        });

        comment_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).openComment(Extra.Tag.HeatingSystem);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getActivity().getSupportFragmentManager();
                saveHeatingPoints();
                saveWaterHeating();
                EditingSection.Iseditinggroup.isheatingedited = true;
                Daylight_LighteningUpdate daylight_update = new Daylight_LighteningUpdate();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
                bundle.putInt("userid", userid);
                daylight_update.setArguments(bundle);
                ((StepProcessActUpdate) getActivity()).replaceFragment(daylight_update, Extra.Tag.Daylight);
            }
        });
        return view;
    }

    public void setWaterHeating() {
        try {
            water_heating.put("Solar thermal + Wetback", 6f);
            water_heating.put("Heat pump + wetback", 6f);
            water_heating.put("Geothermal", 6f);
            water_heating.put("Solar thermal", 5f);
            water_heating.put("Heat Pump", 5f);
            water_heating.put("Electric + wetback", 4f);
            water_heating.put("Electric", 3f);
            water_heating.put("Gas califont + wetback", 2f);
            water_heating.put("Gas califont", 1f);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            adapter.addAll(water_heating.keySet());
            spinner_solar.setAdapter(adapter);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveHeatingPoints() {
        try {
            float points_achieved = 0.0f;

            points_achieved = points_achieved + (((Extra.EmmisionFactor.heat_pump_emission_fact /
                    Extra.COP.heat_pump_cop) * Float.parseFloat(spinner_heat_air.getSelectedItem().toString())) / 100);


            points_achieved = points_achieved + (((Extra.EmmisionFactor.heat_pump_emission_fact /
                    Extra.COP.heat_pump_cop) * Float.parseFloat(spinner_hydronic.getSelectedItem().toString())) / 100);


            // if (!spinner_gas.getSelectedItem().toString().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.lpg_gas_emission_fact /
                    Extra.COP.lpg_gas) * Float.parseFloat(spinner_gas.getSelectedItem().toString())) / 100);
            //  }

            // if (!spinner_natural_gas.getSelectedItem().toString().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.natural_gas_emission_fact /
                    Extra.COP.natural_gas) * Float.parseFloat(spinner_natural_gas.getSelectedItem().toString())) / 100);
            // }

            points_achieved = points_achieved + (((Extra.EmmisionFactor.electric_emission_fact /
                    Extra.COP.electric) * Float.parseFloat(spinner_elec_heater.getSelectedItem().toString())) / 100);
            //  }
            // if (!HeatModel.getInstance().getWood_burner().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.wood_burner_emission_fact /
                    Extra.COP.wood_burner) * Float.parseFloat(spinner_wood.getSelectedItem().toString())) / 100);
            //  }
            // if (!HeatModel.getInstance().getOpen_fire().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.open_fire_emission_fact /
                    Extra.COP.open_fire) * Float.parseFloat(spinner_wood.getSelectedItem().toString())) / 100);
            // }

            HeatModel.getInstance().setPoints(points_achieved);

            if (points_achieved <= 10)
                HeatModel.getInstance().setPoints(6f);
            else if (points_achieved <= 20)
                HeatModel.getInstance().setPoints(5f);
            else if (points_achieved <= 30)
                HeatModel.getInstance().setPoints(4f);
            else if (points_achieved <= 40)
                HeatModel.getInstance().setPoints(3f);
            else if (points_achieved <= 50)
                HeatModel.getInstance().setPoints(2f);
            else if (points_achieved <= 60)
                HeatModel.getInstance().setPoints(1f);

            HeatModel.getInstance().setEletric_heater(spinner_elec_heater.getSelectedItem().toString());
            HeatModel.getInstance().setHeat_pump_to_air(spinner_heat_air.getSelectedItem().toString());
            HeatModel.getInstance().setHeat_pump_hydrolic(spinner_hydronic.getSelectedItem().toString());
            HeatModel.getInstance().setLpg_gas(spinner_gas.getSelectedItem().toString());
            HeatModel.getInstance().setNatural_gas(spinner_natural_gas.getSelectedItem().toString());
            HeatModel.getInstance().setWood_burner(spinner_wood.getSelectedItem().toString());
            HeatModel.getInstance().setEletric_heater_kw(text_ele_heater.getText().toString());
            HeatModel.getInstance().setHeat_pump_to_air_kw(text_heat_to_air.getText().toString());
            HeatModel.getInstance().setLpg_gas_kw(text_lpg_gas.getText().toString());
            HeatModel.getInstance().setHeat_pump_hydrolic_kw(text_heat_hydronic.getText().toString());
            HeatModel.getInstance().setNatural_gas_kw(text_gas_natural_gas.getText().toString());
            HeatModel.getInstance().setWood_burner_kw(text_wood.getText().toString());
            HeatModel.getInstance().setId(heatmodel.getId());
            HeatModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.HeatingSystem));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveWaterHeating() {
        try {
            WaterHeating.getInstance().setWater_heating(spinner_solar.getSelectedItem().toString());
            WaterHeating.getInstance().setPoints(water_heating.get(spinner_solar.getSelectedItem().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void spinner_values(View v, int id, String result) {
        System.out.println(result);
        spinner = v.findViewById(id);
        List age = new ArrayList<Integer>();
        for (int i = 0; i <= 100; i = i + 10) {
            age.add(Integer.toString(i));
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(
                v.getContext(), android.R.layout.simple_spinner_item, age);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(result)) {
                spinner.setSelection(i);
            }
        }
    }

    public void getHeating(View v, int userid, String properID) {

        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLHEATINGSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", properID);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                        (Runnable) () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                        () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {
                            heatmodel.setId(jsonObject.getInt("heatid"));
                            heatmodel.setHeat_pump_to_air(jsonObject.get("air_perc").toString());
                            heatmodel.setHeat_pump_to_air_kw(jsonObject.get("air_kw").toString());
                            if(jsonObject.get("air_image") == null){
                                heatmodel.setImage1(null);
                            }else{
                                heatmodel.setImage1(jsonObject.getString("air_image").getBytes());
                            }
                            heatmodel.setHeat_pump_hydrolic(jsonObject.get("hyd_perc").toString());
                            heatmodel.setHeat_pump_hydrolic_kw(jsonObject.get("hyd_kw").toString());

                            if(jsonObject.get("hyd_image") == null){
                                heatmodel.setImage2(null);
                            }else{
                                heatmodel.setImage2(jsonObject.getString("hyd_image").getBytes());
                            }
                            heatmodel.setNatural_gas(jsonObject.get("lpg_gas_perc").toString());
                            heatmodel.setNatural_gas_kw(jsonObject.get("lpg_gas_kw").toString());

                            if(jsonObject.get("lpg_gas_image") == null){
                                heatmodel.setImage3(null);
                            }else{
                                heatmodel.setImage3(jsonObject.getString("lpg_gas_image").getBytes());
                            }
                            heatmodel.setWood_burner(jsonObject.get("wood_perc").toString());
                            heatmodel.setWood_burner_kw(jsonObject.get("wood_kw").toString());

                            if(jsonObject.get("wood_image") == null){
                                heatmodel.setImage4(null);
                            }else{
                                heatmodel.setImage4(jsonObject.getString("wood_image").getBytes());
                            }
                            heatmodel.setEletric_heater(jsonObject.get("ele_h_perc").toString());
                            heatmodel.setEletric_heater_kw(jsonObject.get("ele_h_kw").toString());

                            if(jsonObject.get("ele_h_image") == null){
                                heatmodel.setImage5(null);
                            }else{
                                heatmodel.setImage5(jsonObject.getString("ele_h_image").getBytes());
                            }
                            heatmodel.setLpg_gas(jsonObject.get("natural_gas_perc").toString());
                            heatmodel.setLpg_gas_kw(jsonObject.get("natural_gas_kw").toString());

                            if(jsonObject.get("natural_gas_image") == null){
                                heatmodel.setImage6(null);
                            }else{
                                heatmodel.setImage6(jsonObject.getString("natural_gas_image").getBytes());
                            }
                            waterheating.setPoints(BigDecimal.valueOf(jsonObject.getDouble("back_boosted_points")).floatValue());
                            waterheating.setWater_heating(jsonObject.getString("back_boosted"));
                            if(jsonObject.get("back_boosted_image") == null){
                                heatmodel.setImage7(null);
                            }else{
                                heatmodel.setImage7(jsonObject.getString("back_boosted_image").getBytes());
                            }
                            heatmodel.setPoints(BigDecimal.valueOf(jsonObject.getDouble("heat_points")).floatValue());
                            heatmodel.setComment(jsonObject.getString("asstcomment"));

                            ((StepProcessActUpdate) getActivity()).runOnUiThread((Runnable) () -> {
                                bindingtocomponent();
                                spinner_values(v, R.id.spinner_heat_to_air, heatmodel.getHeat_pump_to_air());
                                spinner_values(v, R.id.spinner_heat_hydronic, heatmodel.getHeat_pump_hydrolic());
                                spinner_values(v, R.id.spinner_gas, heatmodel.getLpg_gas());
                                spinner_values(v, R.id.spinner_wood_pellet, heatmodel.getWood_burner());
                                spinner_values(v, R.id.spinner_electric_heater, heatmodel.getEletric_heater());
                                spinner_values(v, R.id.spinner_natural_gas, heatmodel.getNatural_gas());
                            });



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ((StepProcessActUpdate) getActivity()).runOnUiThread(
                            (Runnable) () -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    public void bindingtocomponent() {

        if (!EditingSection.Iseditinggroup.isheatingedited) {
            text_heat_to_air.setText(heatmodel.getHeat_pump_to_air_kw());
            text_heat_hydronic.setText(heatmodel.getHeat_pump_hydrolic_kw());
            text_lpg_gas.setText(heatmodel.getLpg_gas_kw());
            text_gas_natural_gas.setText(heatmodel.getNatural_gas_kw());
            text_wood.setText(heatmodel.getWood_burner_kw());
            text_ele_heater.setText(heatmodel.getEletric_heater_kw());
            BaseActivity.saveComment(heatmodel.getComment(), Extra.Tag.HeatingSystem);
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.heatair, heatmodel.getImage1());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.heathydronic, heatmodel.getImage2());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.gas, heatmodel.getImage3());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.naturalgas, heatmodel.getImage4());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Wood, heatmodel.getImage5());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.ElectricHeater, heatmodel.getImage6());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Solar, heatmodel.getImage7());
            for (int i = 0; i < spinner_solar.getCount(); i++) {
                if (spinner_solar.getItemAtPosition(i).toString().equals(waterheating.getWater_heating())) {
                    spinner_solar.setSelection(i);
                }
            }
            //HeatModel.getInstance().setId(heatmodel.getId());
        } else {
            text_heat_to_air.setText(HeatModel.getInstance().getHeat_pump_to_air_kw());
            text_heat_hydronic.setText(HeatModel.getInstance().getHeat_pump_hydrolic_kw());
            text_lpg_gas.setText(HeatModel.getInstance().getLpg_gas_kw());
            text_gas_natural_gas.setText(HeatModel.getInstance().getNatural_gas_kw());
            text_wood.setText(HeatModel.getInstance().getWood_burner_kw());
            text_ele_heater.setText(HeatModel.getInstance().getEletric_heater_kw());
            for (int i = 0; i < spinner_solar.getCount(); i++) {
                if (spinner_solar.getItemAtPosition(i).toString().equals(WaterHeating.getInstance().getWater_heating())) {
                    spinner_solar.setSelection(i);
                }
            }
            BaseActivity.saveComment(HeatModel.getInstance().getComment(), Extra.Tag.HeatingSystem);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}