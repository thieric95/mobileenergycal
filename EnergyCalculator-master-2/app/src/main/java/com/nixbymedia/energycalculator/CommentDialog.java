package com.nixbymedia.energycalculator;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
//import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CommentDialog extends Dialog {

    Context ctx;
    Activity act_context;
    CommentDialog(Context context) {
        super(context);
        this.ctx = context;
        this.act_context = (Activity) context;
    }

    EditText comment;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.comment_dialog);

        comment = findViewById(R.id.text_comment);
        submit = findViewById(R.id.btnComment);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(comment.getText().toString().trim().equals(""))
                    comment.setError("Please Enter Some Comment!");
                else{
                    Toast.makeText(ctx, ""+comment.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
