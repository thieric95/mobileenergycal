package com.nixbymedia.energycalculator;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.RecyclerView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;


import com.nixbymedia.energycalculator.certificate.CertificateActivity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.report.ReportActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static java.lang.Thread.sleep;

public class ProjectList extends AppCompatActivity implements View.OnClickListener {
    SessionManagement session;
    private int userid;
    RecyclerView recyleview;
    DatabaseHelperClass dbhelper;
    ImageButton projectlistbtnhome, btn_refresh;
    //private Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);
        this.recyleview = findViewById(R.id.recyleview);
        this.projectlistbtnhome = findViewById(R.id.projectlistbtnhome);
        this.btn_refresh = findViewById(R.id.btn_refresh);
        this.dbhelper = new DatabaseHelperClass(this);

//        projectlistbtnhome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent
//                        (ProjectList.this, Activity_Home.class));
//                finish();
//            }
//        });
        this.projectlistbtnhome.setOnClickListener(ProjectList.this);
        this.btn_refresh.setOnClickListener(ProjectList.this);

        this.getuserID();

        this.fetchAllprojectbyuserid();

    }

    public void fetchAllprojectbyuserid() {


        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROJECTLIST;
        ArrayList<PropertyModel> propertyList = new ArrayList<>();
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS).retryOnConnectionFailure(true)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(this.userid));
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                runOnUiThread(
                        () -> {
                            Toast.makeText(getApplicationContext(), "Error.", Toast.LENGTH_SHORT).show();
                        }
                );
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String body = response.body().string();
                    Log.e("Response", body);
                    try {
                        JSONObject jsonObject = new JSONObject(body);

                        if (jsonObject.has("error")) {
                            String message = jsonObject.getString("message");
                            runOnUiThread(
                                    () -> Toast.makeText(getApplicationContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            JSONArray jsonarray = jsonObject.getJSONArray("projects");
                            for (int i = 0; i < jsonarray.length(); i++) {
                                JSONObject jsonObject1 = jsonarray.getJSONObject(i);
                                propertyList.add(new PropertyModel(
                                        jsonObject1.getString("prop_id"),
                                        jsonObject1.getString("dwell_type"),
                                        jsonObject1.getString("dwell_size"),
                                        Integer.toString(jsonObject1.getInt("room_no")),
                                        jsonObject1.getString("climate_location"),
                                        jsonObject1.getString("legal_desc"),
                                        jsonObject1.getString("cert_title"),
                                        jsonObject1.getString("address"),
                                        jsonObject1.getString("asstcomment")
                                ));
                            }
                            runOnUiThread(
                                    () -> {
                                        Adapter adapter = new Adapter(ProjectList.this, propertyList);
                                        recyleview.setAdapter(adapter);
                                    }
                            );

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.getuserID();
        this.fetchAllprojectbyuserid();
    }

    private void getuserID() {
        this.session = new SessionManagement(getApplicationContext());
        if (this.session.checkforlogin()) {
            finish();
        }

        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.projectlistbtnhome:
                startActivity(new Intent
                        (ProjectList.this, Activity_Home.class));
                finish();
                break;
            case R.id.btn_refresh:
                this.fetchAllprojectbyuserid();

                break;
            default:
                break;
        }
    }
}