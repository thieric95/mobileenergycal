package com.nixbymedia.energycalculator.BackgroundOperations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.Login_Activity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.property.PropertyModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WaterUseBackTask{

    Activity ctx;
    private int user_id;
    private String pro_id;
    byte[] img1, img2, img3;
    RequestQueue requestQueue;
    public WaterUseBackTask(Context context, byte[] image1, byte[] image2, byte[] image3){
        this.ctx = (Activity) context;
        this.img1 = image1;
        this.img2 = image2;
        this.img3 = image3;

    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }


    public void insert(String... params){

        String kitchen_tap = params[0];
        String kitchen_tap_points = params[1];
        String basin_tap = params[2];
        String basin_tap_points = params[3];
        String shower = params[4];
        String shower_points = params[5];
        String rates_inspect = params[6];
        String worst_wc = params[7];
        String worst_wc_points = params[8];
        String doc_by_inspect = params[9];
        String total_points = params[10];
        String comment = params[11];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLWATERUSEINSERT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("kitchen_tap", kitchen_tap)
                .addFormDataPart("kitchen_tap_points", kitchen_tap_points)
                .addFormDataPart("kitchen_tap_img", "kitchen_tap_img.jpg", RequestBody.create(this.img1, MediaType.parse("image/*")))
                .addFormDataPart("basin_tap", basin_tap)
                .addFormDataPart("basin_tap_points", basin_tap_points)
                .addFormDataPart("basin_tap_img", "basin_tap_img.jpg",RequestBody.create(this.img2, MediaType.parse("image/*")))
                .addFormDataPart("shower", shower)
                .addFormDataPart("shower_points", shower_points)
                .addFormDataPart("shower_img", "shower_img.jpg", RequestBody.create(this.img3, MediaType.parse("image/*")))
                .addFormDataPart("rates_inspect", rates_inspect)
                .addFormDataPart("worst_wc", worst_wc)
                .addFormDataPart("worst_wc_points", worst_wc_points)
                .addFormDataPart("doc_by_inspect", doc_by_inspect)
                .addFormDataPart("total_points", total_points)
                .addFormDataPart("asstcomment", comment)
                .addFormDataPart("prop_id", this.pro_id)
                .addFormDataPart("userid", Integer.toString(this.user_id))
                .build();

        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void update(String... params){
        String kitchen_tap = params[0];
        String kitchen_tap_points = params[1];
        String basin_tap = params[2];
        String basin_tap_points = params[3];
        String shower = params[4];
        String shower_points = params[5];
        String rates_inspect = params[6];
        String worst_wc = params[7];
        String worst_wc_points = params[8];
        String doc_by_inspect = params[9];
        String total_points = params[10];
        String comment = params[11];
        String wateruseid = params[12];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLWATERUSEUPDATE;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("kitchen_tap", kitchen_tap)
                .addFormDataPart("kitchen_tap_points", kitchen_tap_points)
                .addFormDataPart("kitchen_tap_img", "kitchen_tap_img.jpg", RequestBody.create(this.img1, MediaType.parse("image/*")))
                .addFormDataPart("basin_tap", basin_tap)
                .addFormDataPart("basin_tap_points", basin_tap_points)
                .addFormDataPart("basin_tap_img", "basin_tap_img.jpg",RequestBody.create(this.img2, MediaType.parse("image/*")))
                .addFormDataPart("shower", shower)
                .addFormDataPart("shower_points", shower_points)
                .addFormDataPart("shower_img", "shower_img.jpg", RequestBody.create(this.img3, MediaType.parse("image/*")))
                .addFormDataPart("rates_inspect", rates_inspect)
                .addFormDataPart("worst_wc", worst_wc)
                .addFormDataPart("worst_wc_points", worst_wc_points)
                .addFormDataPart("doc_by_inspect", doc_by_inspect)
                .addFormDataPart("total_points", total_points)
                .addFormDataPart("asstcomment", comment)
                .addFormDataPart("prop_id", this.pro_id)
                .addFormDataPart("userid", Integer.toString(this.user_id))
                .addFormDataPart("wateruseid", wateruseid)
                .build();
        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Response", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
