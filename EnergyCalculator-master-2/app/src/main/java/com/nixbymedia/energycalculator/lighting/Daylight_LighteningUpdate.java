package com.nixbymedia.energycalculator.lighting;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.EditingSection;
import com.nixbymedia.energycalculator.FileModel;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.SchemaHelperClass;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.StepProcessActUpdate;
import com.nixbymedia.energycalculator.extra.Debugger;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.Heating_System;
import com.nixbymedia.energycalculator.heatingsystem.Heating_SystemUpdate;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.water_user.Water_Use;
import com.nixbymedia.energycalculator.water_user.Water_Use_Update;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class Daylight_LighteningUpdate extends Fragment implements TextWatcher {


    protected Spinner spMeter;
    protected LinearLayout header;
    ImageButton back, comment, forward;
    ImageButton img1, img2, img3, img4, img5, img6;
    EditText led, fluorescent, halogen, indoor_wattage, outdoor_wattage, sensors;
    RadioButton rb;
    TextView cmnt;
    FragmentManager fragmentManager;
    String rb_Text = "NO";
    View view;
    private Activity ctx;
    private int userid;
    private String CertifiNo;
    LightingModel lightingmodel = new LightingModel();
    DatabaseHelperClass dbhelper;

    public Daylight_LighteningUpdate() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.CertifiNo = getArguments().getString("propertyid");
        this.userid = getArguments().getInt("userid");
        this.dbhelper = new DatabaseHelperClass(getContext());
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_daylight__lightening_update, container, false);

        back = view.findViewById(R.id.day_light_back);
        forward = view.findViewById(R.id.day_light_forward);
        comment = view.findViewById(R.id.day_light_comment);
        spMeter = view.findViewById(R.id.sp_meter);
        img1 = view.findViewById(R.id.img_led);
        img2 = view.findViewById(R.id.img_fluorescent);
        img3 = view.findViewById(R.id.img_halogen);
        img4 = view.findViewById(R.id.img_total_wattage);
        img5 = view.findViewById(R.id.img_outdoor_lights_wattage);
        img6 = view.findViewById(R.id.img_wattage_sensor);
        led = view.findViewById(R.id.text_LED_count);
        led.addTextChangedListener(this);

        fluorescent = view.findViewById(R.id.text_Fluorescent_count);
        fluorescent.addTextChangedListener(this);

        halogen = view.findViewById(R.id.text_Halogen_count);
        halogen.addTextChangedListener(this);

        indoor_wattage = view.findViewById(R.id.text_indoor_light_wattage);
        outdoor_wattage = view.findViewById(R.id.text_outdoor_light_wattage);
        sensors = view.findViewById(R.id.text_wattage_sensor);

        cmnt = view.findViewById(R.id.comment_daylight_text);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).openComment(Extra.Tag.Daylight);
            }
        });
        rb = view.findViewById(R.id.inspect_radio_daylight);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.LED_Lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.LED_Lights, Extra.SubPath.Lighting, getActivity());
                }
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Fluorescent_Lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Fluorescent_Lights, Extra.SubPath.Lighting, getActivity());
                }
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Halogen_lights + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Halogen_lights, Extra.SubPath.Lighting, getActivity());
                }
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Indoor_Light + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Indoor_Light, Extra.SubPath.Lighting, getActivity());
                }
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Outdoor_Light + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Outdoor_Light, Extra.SubPath.Lighting, getActivity());
                }
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wattage_Sensor + "_" + System.currentTimeMillis(), Extra.SubPath.Lighting, getActivity());
                    ((StepProcessActUpdate) getContext()).openCamera(Extra.FileConstants.Wattage_Sensor, Extra.SubPath.Lighting, getActivity());
                }
            }
        });

        rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rb_Text = "YES";
                } else {
                    rb_Text = "NO";
                }
            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Heating_SystemUpdate heating_update = new Heating_SystemUpdate();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
                bundle.putInt("userid", userid);
                heating_update.setArguments(bundle);
                ((StepProcessActUpdate) getContext()).replaceFragment(heating_update, Extra.Tag.HeatingSystem);
            }
        });

//
//        DayLightBackTask dayLightBackTask = new DayLightBackTask(getContext(), byteimg1, byteimg2, byteimg3,
//                byteimg4, byteimg5, byteimg6);
//        dayLightBackTask.execute("remove");

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    setLuxMeter();
                    calculatePoints();
                    saveinspector();
                    EditingSection.Iseditinggroup.isdaylightingedited = true;
                    Water_Use_Update water_user_date = new Water_Use_Update();
                    Bundle bundle = new Bundle();
                    bundle.putString("propertyid", CertifiNo);
                    bundle.putInt("userid", userid);
                    water_user_date.setArguments(bundle);
                    ((StepProcessActUpdate) getContext()).replaceFragment(water_user_date, Extra.Tag.WaterUse);
                }
            }
        });

        //setSpinner();
        setTextWatcherForIndoorWattage();
        getDaylight(this.userid, this.CertifiNo);
        bindtocomponent();
        return view;
    }


    public void saveinspector() {
        try {
            LightingModel.getInstance().setInspectorlightview(rb_Text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isValidate() {
        try {
            if (led.getText().toString().isEmpty() && fluorescent.getText().toString().isEmpty()
                    && halogen.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showAlertMessages(getString(R.string.error_validate_msg));
                return false;
            } else if (led.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Number of LED Lights", view);
                return false;
            } else if (fluorescent.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Number of Fluorescent Lights", view);
                return false;
            } else if (halogen.getText().toString().isEmpty() && indoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Number of Halogen Lights", view);
                return false;
            } else if (outdoor_wattage.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Total Outdoor Light Wattage", view);
                return false;
            } else if (sensors.getText().toString().isEmpty()) {
                ((StepProcessActUpdate) getActivity()).showError("Please enter Wattage with sensorsfitted", view);
                return false;
            } else if (!rb.isChecked()) {
                ((StepProcessActUpdate) getActivity()).showError("Please check the checklist", view);
                return false;
            } else
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setOutDoorPoints() {
        try {
            int total_points = 0;
            if (!outdoor_wattage.getText().toString().isEmpty()) {
                total_points = total_points + Integer.parseInt(outdoor_wattage.getText().toString());
                LightingModel.getInstance().setTotal_outdoor_wattage(Integer.parseInt(outdoor_wattage.getText().toString()));
            }

            if (!sensors.getText().toString().isEmpty()) {
                total_points = total_points + Integer.parseInt(sensors.getText().toString());
                LightingModel.getInstance().setSensor_wattage(Integer.parseInt(sensors.getText().toString()));
            }

            LightingModel.getInstance().setTotal_outdoor_light(LightingModel.getInstance().getTotal_outdoor_wattage() - (LightingModel.getInstance().getSensor_wattage() / 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculatePoints() {
        try {
            setIndoorPoints();
            setOutDoorPoints();
            Debugger.debugE("LightingModel", LightingModel.getInstance().toString());

            float point = (LightingModel.getInstance().getTotal_indoor_light()
                    + LightingModel.getInstance().getTotal_outdoor_light())
                    / Float.parseFloat(PropertyModel.getInstance().getDwellingSize());
            if (point <= 1.5) {
                LightingModel.getInstance().setTotal_points(4f);
            } else if (point <= 3) {
                LightingModel.getInstance().setTotal_points(3f);
            } else if (point <= 5)
                LightingModel.getInstance().setTotal_points(2f);
            else if (point <= 7)
                LightingModel.getInstance().setTotal_points(1f);
            else if (point > 7) {
                LightingModel.getInstance().setTotal_points(0f);
            }

            if (spMeter.getSelectedItem().toString().equals("Yes")) {
                LightingModel.getInstance().setTotal_points(
                        LightingModel.getInstance().getTotal_points() + 1
                );
            }
            LightingModel.getInstance().setInspectorlightview(this.rb_Text);
            LightingModel.getInstance().setId(lightingmodel.getId());
            Debugger.debugE("LightingModel", LightingModel.getInstance().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setIndoorPoints() {
        try {
            int total_points = 0;
            if (!led.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(led.getText().toString())) * Extra.Watts.LED);
                LightingModel.getInstance().setLed_light(led.getText().toString());
            }
            if (!fluorescent.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(fluorescent.getText().toString())) * Extra.Watts.CFL);
                LightingModel.getInstance().setCfl_light(fluorescent.getText().toString());
            }

            if (!halogen.getText().toString().isEmpty()) {
                total_points = total_points + ((Integer.parseInt(halogen.getText().toString())) * Extra.Watts.Halogen);
                LightingModel.getInstance().setHalogen_light(halogen.getText().toString());
            }

            if (!indoor_wattage.getText().toString().isEmpty()) {
                total_points = total_points + (Integer.parseInt(indoor_wattage.getText().toString()));
                LightingModel.getInstance().setTotal_indoor_wattage(Float.parseFloat(indoor_wattage.getText().toString()));
            }

            LightingModel.getInstance().setTotal_indoor_light((total_points));
            LightingModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.Daylight));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTextWatcherForIndoorWattage() {
        try {
            indoor_wattage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        if (!s.toString().isEmpty()) {
                            clearLights(false);
                        } else
                            clearLights(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearLights(boolean isActive) {
        try {
            led.setEnabled(isActive);
            fluorescent.setEnabled(isActive);
            halogen.setEnabled(isActive);
            img1.setEnabled(isActive);
            img2.setEnabled(isActive);
            img3.setEnabled(isActive);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLuxMeter() {
        try {
            LightingModel.getInstance().setLux_meter(spMeter.getSelectedItem().toString());
            System.out.println(LightingModel.getInstance().getLux_meter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // this textwatcher we have used for led_lights , fluorescent_lights ,incandescent_lights
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        try {
            if (!s.toString().isEmpty()) {
                indoor_wattage.setEnabled(false);
                img4.setEnabled(false);
            } else {
                indoor_wattage.setEnabled(true);
                img4.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDaylight(int userid, String properID) {

        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLLIGHTINGSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", properID);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                        () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.e("Daylight", jsonData);
                    try {
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                        () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                                );
                            }
                        } else {
                            lightingmodel.setId(jsonObject.getInt("daylightid"));
                            lightingmodel.setLux_meter(jsonObject.getString("lux_meter"));
                            lightingmodel.setLed_light(Integer.toString(jsonObject.getInt("no_led")));

                            if(jsonObject.get("no_led_img") == null){
                                lightingmodel.setImage_1(null);
                            }else{
                                lightingmodel.setImage_1(jsonObject.getString("no_led_img").getBytes());
                            }
                            lightingmodel.setCfl_light(jsonObject.getString("no_fluo"));

                            if(jsonObject.get("no_fluo_img") == null){
                                lightingmodel.setImage_2(null);
                            }else{
                                lightingmodel.setImage_2(jsonObject.getString("no_fluo_img").getBytes());
                            }

                            lightingmodel.setHalogen_light(Integer.toString(jsonObject.getInt("no_halog")));

                            if(jsonObject.get("no_halog_img") == null){
                                lightingmodel.setImage_3(null);
                            }else{
                                lightingmodel.setImage_3(jsonObject.getString("no_halog_img").getBytes());
                            }
                            if(BigDecimal.valueOf(jsonObject.getDouble("total_in_watt")).floatValue() == 0){
                                lightingmodel.setTotal_indoor_wattage(0);
                            }else{
                                lightingmodel.setTotal_indoor_wattage(BigDecimal.valueOf(jsonObject.getDouble("total_in_watt")).floatValue());
                            }


                            if(jsonObject.get("total_in_watt_img") == null){
                                lightingmodel.setImage_4(null);
                            }else{
                                lightingmodel.setImage_4(jsonObject.getString("total_in_watt_img").getBytes());
                            }
                            lightingmodel.setTotal_outdoor_wattage(jsonObject.getInt("total_out_watt"));

                            if(jsonObject.get("total_out_watt_img") == null){
                                lightingmodel.setImage_5(null);
                            }else{
                                lightingmodel.setImage_5(jsonObject.getString("total_out_watt_img").getBytes());
                            }
                            lightingmodel.setSensor_wattage(jsonObject.getInt("watt_sensor"));

                            if(jsonObject.get("watt_sensor_img") == null){
                                lightingmodel.setImage_6(null);
                            }else{
                                lightingmodel.setImage_6(jsonObject.getString("watt_sensor_img").getBytes());
                            }
                            lightingmodel.setTotal_indoor_light(BigDecimal.valueOf(jsonObject.getDouble("total_in_light")).floatValue());
                            lightingmodel.setTotal_outdoor_light(BigDecimal.valueOf(jsonObject.getDouble("total_out_light")).floatValue());
                            lightingmodel.setInspectorlightview(jsonObject.getString("light_inspect"));
                            lightingmodel.setTotal_points(jsonObject.getInt("light_points"));
                            lightingmodel.setComment(jsonObject.getString("asstcomment"));
                            ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                    (Runnable) () -> bindtocomponent()
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ctx.runOnUiThread(
                            () -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }



    public void bindtocomponent() {
        if(!EditingSection.Iseditinggroup.isdaylightingedited){

            outdoor_wattage.setText(Integer.toString(lightingmodel.getTotal_outdoor_wattage()));

            for (int i = 0; i < spMeter.getCount(); i++) {
                if (spMeter.getItemAtPosition(i).toString().equalsIgnoreCase(lightingmodel.getLux_meter())) {
                    spMeter.setSelection(i);
                }
            }
            led.setText(lightingmodel.getLed_light());
            fluorescent.setText(lightingmodel.getCfl_light());
            halogen.setText(lightingmodel.getHalogen_light());
            if(lightingmodel.getTotal_indoor_wattage() == 0){
                indoor_wattage.setText("");
            }else{
                indoor_wattage.setText(Float.toString(lightingmodel.getTotal_indoor_wattage()));
            }

            sensors.setText(Integer.toString(lightingmodel.getSensor_wattage()));
            if(lightingmodel.getInspectorlightview() != null){
                if(lightingmodel.getInspectorlightview().equals("YES")){
                    rb.setChecked(true);
                    rb_Text = lightingmodel.getInspectorlightview();
                }
            }
            if(lightingmodel.getImage_1() != null){
                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.LED_Lights, lightingmodel.getImage_1());
            }
            if(lightingmodel.getImage_2() != null){
                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Fluorescent_Lights, lightingmodel.getImage_2());
            }
            if(lightingmodel.getImage_3() != null){
                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Halogen_lights, lightingmodel.getImage_3());
            }
            if(lightingmodel.getImage_4() != null){
                FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Indoor_Light, lightingmodel.getImage_4());
            }
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Outdoor_Light, lightingmodel.getImage_5());
            FileModel.getFileModel().fileArrayByte.put(Extra.FileConstants.Wattage_Sensor, lightingmodel.getImage_6());
            LightingModel.getInstance().setId(lightingmodel.getId());
            BaseActivity.saveComment(lightingmodel.getComment(), Extra.Tag.Daylight);
        }else{
            outdoor_wattage.setText(Integer.toString(LightingModel.getInstance().getTotal_outdoor_wattage()));
            rb_Text = LightingModel.getInstance().getInspectorlightview();
            if(LightingModel.getInstance().getInspectorlightview() != null){
                if(LightingModel.getInstance().getInspectorlightview().equals("YES")){
                    rb.setChecked(true);
                    rb_Text = LightingModel.getInstance().getInspectorlightview();
                }
            }
            for (int i = 0; i < spMeter.getCount(); i++) {
                if (spMeter.getItemAtPosition(i).toString().equalsIgnoreCase(LightingModel.getInstance().getLux_meter())) {
                    spMeter.setSelection(i);
                }
            }
            led.setText(LightingModel.getInstance().getLed_light());
            fluorescent.setText(LightingModel.getInstance().getCfl_light());
            halogen.setText(LightingModel.getInstance().getHalogen_light());
            if(lightingmodel.getTotal_indoor_wattage() == 0){
                indoor_wattage.setText("");
            }else{
                indoor_wattage.setText(Float.toString(LightingModel.getInstance().getTotal_indoor_wattage()));
            }
            sensors.setText(Integer.toString(LightingModel.getInstance().getSensor_wattage()));
            BaseActivity.saveComment(LightingModel.getInstance().getComment(), Extra.Tag.Daylight);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}