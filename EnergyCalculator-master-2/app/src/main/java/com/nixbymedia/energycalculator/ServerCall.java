package com.nixbymedia.energycalculator;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.extra.EnergyAPI;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServerCall {

    String data;
    Activity ctx;
    RequestQueue requestQueue;

    public ServerCall(Activity ctx) {
        this.ctx = ctx;
    }

    public void login(String... params) {
        String mobile_number = params[0];
        String password_entered = params[1];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String urllogin = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.USERLOGIN;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("mobile", mobile_number)
                .add("password", password_entered);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(urllogin).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                ctx.runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                Log.w("failure Response", e.getMessage());
                            }
                        }
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ctx.runOnUiThread(
                                        () -> {
                                            Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show();
                                        }
                                );

                            }
                        } else {
                            SessionManagement session = new SessionManagement(ctx);
                            session.createLoginSession(jsonObject.getInt("userid"), jsonObject.getString("mobile"),
                                    jsonObject.getString("password"), jsonObject.getString("username"));

                            ctx.runOnUiThread(
                                    () -> {
                                        Toast.makeText(ctx, "Login Success!", Toast.LENGTH_SHORT).show();
                                        ctx.startActivity(new Intent(ctx, Activity_Home.class));
                                    }
                            );


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    ctx.runOnUiThread(() -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show());

                }
            }
        });
    }

    public void register(String... params) {

        String username = params[0];
        String email = params[1];
        String company = params[2];
        String website = params[3];
        String mobile = params[4];
        String password = params[5];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String urllogin = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.USERREGISTER;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("username", username)
                .add("email", email)
                .add("company", company)
                .add("website", website)
                .add("mobile", mobile)
                .add("password", password);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(urllogin).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ctx.runOnUiThread(
                        () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {

                    try {
                        String jsonData = response.body().string();
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                        ctx.startActivity(new Intent(ctx, Login_Activity.class));
                                    }
                            );
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    ctx.runOnUiThread(() -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show());

                }
            }
        });
    }


}
