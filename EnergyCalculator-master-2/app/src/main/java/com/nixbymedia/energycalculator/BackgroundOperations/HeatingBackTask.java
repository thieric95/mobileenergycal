package com.nixbymedia.energycalculator.BackgroundOperations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.Login_Activity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HeatingBackTask {
    Activity ctx;
    byte[] img1, img2, img3, img4, img5, img6, img7;
    private int user_id;
    private String pro_id;
    RequestQueue requestQueue;

    public HeatingBackTask(Context context, byte[] image1, byte[] image2, byte[] image3, byte[] image4,
                           byte[] image5, byte[] image6, byte[] image7) {
        this.ctx = (Activity) context;
        this.img1 = image1;
        this.img2 = image2;
        this.img3 = image3;
        this.img4 = image4;
        this.img5 = image5;
        this.img6 = image6;
        this.img7 = image7;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }


    public void insert(String... params) {
        String air_perc = params[0];
        String air_kw = params[1];
        String hyd_perc = params[2];
        String hyd_kw = params[3];
        String lpg_gas_perc = params[4];
        String lpg_gas_kw = params[5];
        String natural_gas_perc = params[6];
        String natural_gas_kw = params[7];
        String wood_perc = params[8];
        String wood_kw = params[9];
        String ele_h_perc = params[10];
        String ele_h_kw = params[11];
        String back_boosted = params[12];
        String back_boosted_points = params[13];
        String heat_points = params[14];
        String comment = params[15];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLHEATINGINSERT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();

        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("air_perc", air_perc)
                .addFormDataPart("air_kw", air_kw)
                .addFormDataPart("air_image", "air_image.jpg", RequestBody.create(this.img1, MediaType.parse("image/*")))
                .addFormDataPart("hyd_perc", hyd_perc)
                .addFormDataPart("hyd_kw", hyd_kw)
                .addFormDataPart("hyd_image", "hyd_image.jpg", RequestBody.create(this.img2, MediaType.parse("image/*")))
                .addFormDataPart("lpg_gas_perc", lpg_gas_perc)
                .addFormDataPart("lpg_gas_kw", lpg_gas_kw)
                .addFormDataPart("lpg_gas_image", "lpg_gas_image.jpg", RequestBody.create(this.img3, MediaType.parse("image/*")))
                .addFormDataPart("natural_gas_perc", natural_gas_perc)
                .addFormDataPart("natural_gas_kw", natural_gas_kw)
                .addFormDataPart("natural_gas_image", "natural_gas_image.jpg", RequestBody.create(this.img4, MediaType.parse("image/*")))
                .addFormDataPart("wood_perc", wood_perc)
                .addFormDataPart("wood_kw", wood_kw)
                .addFormDataPart("wood_image", "wood_image.jpg", RequestBody.create(this.img5, MediaType.parse("image/*")))
                .addFormDataPart("ele_h_perc", ele_h_perc)
                .addFormDataPart("ele_h_kw", ele_h_kw)
                .addFormDataPart("ele_h_image", "ele_h_image.jpg", RequestBody.create(this.img6, MediaType.parse("image/*")))
                .addFormDataPart("back_boosted", back_boosted)
                .addFormDataPart("back_boosted_points", back_boosted_points)
                .addFormDataPart("back_boosted_image", "back_boosted_image.jpg", RequestBody.create(this.img7, MediaType.parse("image/*")))
                .addFormDataPart("heat_points", heat_points)
                .addFormDataPart("asstcomment", comment)
                .addFormDataPart("prop_id", this.pro_id)
                .addFormDataPart("userid", Integer.toString(this.user_id))
                .build();

        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Respose", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            Log.e("Error", message);
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");

                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void update(String... params) {
        String air_perc = params[0];
        String air_kw = params[1];
        String hyd_perc = params[2];
        String hyd_kw = params[3];
        String lpg_gas_perc = params[4];
        String lpg_gas_kw = params[5];
        String natural_gas_perc = params[6];
        String natural_gas_kw = params[7];
        String wood_perc = params[8];
        String wood_kw = params[9];
        String ele_h_perc = params[10];
        String ele_h_kw = params[11];
        String back_boosted = params[12];
        String back_boosted_points = params[13];
        String heat_points = params[14];
        String comment = params[15];
        String heatid = params[15];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLHEATINGUPDATE;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("air_perc", air_perc)
                .addFormDataPart("air_kw", air_kw)
                .addFormDataPart("air_image", "air_image.jpg", RequestBody.create(this.img1, MediaType.parse("image/*")))
                .addFormDataPart("hyd_perc", hyd_perc)
                .addFormDataPart("hyd_kw", hyd_kw)
                .addFormDataPart("hyd_image", "hyd_image.jpg", RequestBody.create(this.img2, MediaType.parse("image/*")))
                .addFormDataPart("lpg_gas_perc", lpg_gas_perc)
                .addFormDataPart("lpg_gas_kw", lpg_gas_kw)
                .addFormDataPart("lpg_gas_image", "lpg_gas_image.jpg", RequestBody.create(this.img3, MediaType.parse("image/*")))
                .addFormDataPart("natural_gas_perc", natural_gas_perc)
                .addFormDataPart("natural_gas_kw", natural_gas_kw)
                .addFormDataPart("natural_gas_image", "natural_gas_image.jpg", RequestBody.create(this.img4, MediaType.parse("image/*")))
                .addFormDataPart("wood_perc", wood_perc)
                .addFormDataPart("wood_kw", wood_kw)
                .addFormDataPart("wood_image", "wood_image.jpg", RequestBody.create(this.img5, MediaType.parse("image/*")))
                .addFormDataPart("ele_h_perc", ele_h_perc)
                .addFormDataPart("ele_h_kw", ele_h_kw)
                .addFormDataPart("ele_h_image", "ele_h_image.jpg", RequestBody.create(this.img6, MediaType.parse("image/*")))
                .addFormDataPart("back_boosted", back_boosted)
                .addFormDataPart("back_boosted_points", back_boosted_points)
                .addFormDataPart("back_boosted_image", "back_boosted_image.jpg", RequestBody.create(this.img7, MediaType.parse("image/*")))
                .addFormDataPart("heat_points", heat_points)
                .addFormDataPart("asstcomment", comment)
                .addFormDataPart("prop_id", this.pro_id)
                .addFormDataPart("userid", Integer.toString(this.user_id))
                .addFormDataPart("heatid", heatid)
                .build();

        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Response", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);

                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
