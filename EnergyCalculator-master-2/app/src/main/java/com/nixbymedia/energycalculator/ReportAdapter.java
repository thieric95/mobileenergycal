package com.nixbymedia.energycalculator;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.nixbymedia.energycalculator.extra.Utils.getFolderFile;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> implements ActivityCompat.OnRequestPermissionsResultCallback {

    ArrayList<String> filePath = new ArrayList<>();

    public ArrayList<String> getFilePath() {
        return filePath;
    }

    public LayoutInflater inflater;

    public ReportAdapter(Context mContext) {
        inflater = LayoutInflater.from(mContext);
    }

    public void setFilePath(ArrayList<String> filePath) {
        this.filePath = filePath;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.report_adapter_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //Uri uri = Uri.fromFile(new File(filePath.get(position)));
        File image = new File(filePath.get(position));
        Log.i("Existed", String.valueOf(image.exists()));
//        image.setReadable(true, false);
//        System.out.println(image.exists());

        Picasso.get().load(image).
                error(R.drawable.unavailable_image).fit().centerInside().rotate(90).into(holder.iv_image, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                Log.d("Successful", "Success loading image from uri:PICASSO");
            }

            @Override
            public void onError(Exception e) {
                Log.d("Unsuccessful", e.getMessage());
            }
        });


    }

    @Override
    public int getItemCount() {
        return filePath.size();
    }

    @Override
    public void onRequestPermissionsResult(int i, @NonNull String[] strings, @NonNull int[] ints) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_image;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_image = itemView.findViewById(R.id.iv_image);
        }
    }
}
