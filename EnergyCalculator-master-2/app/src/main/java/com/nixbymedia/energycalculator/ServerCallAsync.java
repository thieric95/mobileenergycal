package com.nixbymedia.energycalculator;

import android.content.Context;
import android.net.ConnectivityManager;

public class ServerCallAsync {

    private Context ctx;

    public ServerCallAsync(Context ctx) {
        this.ctx = ctx;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
