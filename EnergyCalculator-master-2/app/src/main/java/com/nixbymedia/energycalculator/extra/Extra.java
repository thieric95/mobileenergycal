package com.nixbymedia.energycalculator.extra;

public interface Extra {

    interface PermissionCodes {
        int REQUEST_PERMISSION_CAMERA = 3;
        int REQUEST_PERMISSION_GALLERY_CAMERA = 4;
    }

    interface ActivityRequestCode {
        int CAMERA_REQUEST_CODE = 555;
    }


    interface SubPath {
        String HeatingSystem = "HeatingSystem";
        String Lighting = "Lighting";
        String Water = "Water";
    }

    // please do not include any _ in value of file constants.
    interface FileConstants {
        String heatair = "heatair";
        String naturalgas = "naturalgas";
        String heathydronic = "heathydronic";
        String gas = "gas";
        String Wood = "Wood";
        String ElectricHeater = "ElectricHeater";
        String Solar = "Solar";
        String LED_Lights = "LEDLights";
        String Fluorescent_Lights = "FluorescentLights";
        String Halogen_lights = "Halogenlights";
        String Indoor_Light = "IndoorLight";
        String Outdoor_Light = "OutdoorLight";
        String Wattage_Sensor = "WattageSensor";
        String kitchen_tap = "kitchentap";
        String basin_tap = "basintap";
        String worst_shower = "worstshower";
    }

    interface COP {
        float heat_pump_cop = 3;
        float lpg_gas = 0.8f;
        float natural_gas = 0.8f;
        float electric = 1;
        float wood_burner = 0.75f;
        float open_fire = 0.3f;
    }

    interface EmmisionFactor {
        float heat_pump_emission_fact = 38;
        float lpg_gas_emission_fact = 61;
        float natural_gas_emission_fact = 53;
        float electric_emission_fact = 38;
        float wood_burner_emission_fact = 3;
        float open_fire_emission_fact = 3;
    }

    interface Watts {
        int LED = 10;
        int CFL = 20;
        int Halogen = 60;
    }

    interface Tag {
        String property = "property";
        String thermal = "thermal";
        String HeatingSystem = "HeatingSystem";
        String Daylight = "Daylight";
        String WaterUse = "WaterUse";
        String Other_Charges = "OtherCharges";
    }
}
