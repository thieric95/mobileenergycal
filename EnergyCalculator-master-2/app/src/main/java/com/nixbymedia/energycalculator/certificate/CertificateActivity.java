package com.nixbymedia.energycalculator.certificate;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
//import android.support.constraint.ConstraintLayout;
//import android.support.constraint.Guideline;
//import android.support.v4.content.FileProvider;
//import android.support.v7.widget.AppCompatTextView;
import android.provider.DocumentsContract;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatTextView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nixbymedia.energycalculator.Activity_Result;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.Splash_Screen_Process;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.heatingsystem.WaterHeating;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.nixbymedia.energycalculator.extra.Utils.getFolderFile;

public class CertificateActivity extends BaseActivity {

    private static final int PICK_PDF_FILE = 2;
    protected TextView tvTitle;
    protected TextView tvSubTitle;
    protected TextView tvTitleProperty;
    protected Guideline left;
    protected Guideline right;
    protected View borderProperty;
    protected TextView tvAddress;
    protected TextView tvLegalDescription;
    protected TextView tvTitleCertificate;
    protected LinearLayout linearProperty1;
    protected TextView tvDwellingType;
    protected TextView tvDwellingSize;
    protected TextView tvBedrooms;
    protected LinearLayout llPropertyDetails2;
    protected TextView tvDwellingPerfomance;
    protected ImageView ivHome;
    protected View header;
    protected Guideline centerGuideline;
    protected Guideline centerVerticalGuideline;
    protected TextView tvEnergyRating;
    protected TextView tvWaterRating;
    protected View energyRatingBox;
    protected View waterRatingBox;
    protected View borderEnergy;
    protected View borderWater;
    protected TextView tvA;
    protected TextView energyA;
    protected AppCompatTextView energyB;
    protected AppCompatTextView energyC;
    protected AppCompatTextView energyD;
    protected AppCompatTextView energyE;
    protected AppCompatTextView energyF;
    protected TextView tvWaterA;
    protected TextView tvWaterB;
    protected TextView tvWaterC;
    protected TextView tvWaterD;
    protected TextView tvWaterE;
    protected TextView tvWaterF;
    protected TextView waterA;
    protected TextView waterB;
    protected TextView waterC;
    protected TextView waterD;
    protected TextView waterE;
    protected TextView waterF;
    protected TextView tvB;
    protected TextView tvC;
    protected TextView tvD;
    protected TextView tvE;
    protected TextView tvF;
    protected LinearLayout llReport;
    protected LinearLayout llEnergyUse;
    protected TextView tvTitleAssessment;
    protected TextView tvAssesscorOrganization;
    protected TextView tvOrganisationWebsite;
    protected TextView tvContactEmail;
    protected LinearLayout linearAssessment;
    protected TextView tvCertificateNumber;
    protected TextView tvCertificateDate;
    protected TextView tvAssessorName;
    protected TextView tvRegistration;
    protected LinearLayout linearAssessment1;
    protected TextView tvNote;
    protected TextView tvRemark;
    protected ConstraintLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certificate);
        initView();
        setData();
        setWaterGrade(WaterUseModel.getInstance().getTotal_points());
        float total = ThermalEnvelope.getInstance().getPoints() + HeatModel.getInstance().getPoints() + LightingModel.getInstance().getTotal_points() + getWaterPoints();
        setEnergyGrade(total);
    }

    private float getWaterPoints() {
        if (WaterUseModel.getInstance().getShower_point() + WaterUseModel.getInstance().getKitchen_tap_point() < 12) {
            return WaterHeating.getInstance().getPoints() + 1;
        } else if (WaterUseModel.getInstance().getShower_point() + WaterUseModel.getInstance().getKitchen_tap_point() < 12) {
            return WaterHeating.getInstance().getPoints() + 0.5f;
        } else
            return WaterHeating.getInstance().getPoints();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgressDialog("Please wait.....");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                saveToPdf("Certificate");
            }
        }, 6000);
    }

    public void saveToPdf(final String subpath) {
        new Thread() {
            public void run() {
                try {
                    View rootView = getWindow().getDecorView().getRootView();
                    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                    //  Display display = wm.getDefaultDisplay();
                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    CertificateActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                    final File pdfFile = new File(getFolderFile(CertificateActivity.this, subpath) + "/" + "Certificate_" + PropertyModel.getInstance().getTitle_certificate() + System.currentTimeMillis() + ".pdf");
                    if (!pdfFile.exists()) {
                        pdfFile.createNewFile();
                    }
                    final PdfDocument document = new PdfDocument();
                    int margin = 70;
                    int pageWidth = (int) (getDeviceDensity() * A4Horizantal);
                    int pageHeight = (int) (getDeviceDensity() * A4Vertical);
                    // final Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath());
                    // do a looping here.
                    Rect rect = new Rect();
                    rect.set(margin, margin, pageWidth - margin, pageHeight - margin);
                    PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder((int) (getDeviceDensity() * A4Horizantal), (int) (getDeviceDensity() * A4Vertical), 2)
                            .setContentRect(rect)
                            .create();

                    final PdfDocument.Page page = document.startPage(pageInfo);
                    final int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                    final int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight(), View.MeasureSpec.EXACTLY);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                root.measure(measureWidth, measuredHeight);
                                root.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                                root.draw(page.getCanvas());

                                // finish the page
                                document.finishPage(page);
//                    page.getCanvas().drawColor(Color.LTGRAY);
                                //page.getCanvas().drawBitmap(bitmap, 0, 0, null);
                                document.writeTo(new FileOutputStream(pdfFile));
                                document.close();
                                hideProgressDialog();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    if(pdfFile.exists()){
                        openPDF(pdfFile);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private int getDeviceDensity() {
        try {
            DisplayMetrics dm = getResources().getDisplayMetrics();
            return dm.densityDpi;
        } catch (Exception e) {
            e.printStackTrace();
            return 150;
        }
    }

    public void openPDF(final File path) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW);
                    if (Build.VERSION.SDK_INT >= 26) {
                        Uri uri = FileProvider.getUriForFile(CertificateActivity.this, CertificateActivity.this.getApplicationContext().getPackageName() + ".provider", path);
                        myIntent.setDataAndType(uri, "application/pdf");
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } else {
                        Uri uri = Uri.fromFile(path);
                        myIntent.setDataAndType(uri, "application/pdf");
                    }
                    Intent j = Intent.createChooser(myIntent, "Choose an application to open with:");
                    startActivity(j);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent1 = new Intent(CertificateActivity.this, Activity_Result.class);
            intent1.putExtra("option", "add");
            startActivity(intent1);
        }
    }

    private void setData() {
        try {
            String address = PropertyModel.getInstance().getAddress();
            tvAddress.setText(address);
            tvLegalDescription.setText(PropertyModel.getInstance().getLegal_description());
            tvTitleCertificate.setText(PropertyModel.getInstance().getTitle_certificate());
            tvDwellingType.setText(PropertyModel.getInstance().getDwellingType());
            tvDwellingSize.setText(PropertyModel.getInstance().getDwellingSize() + " m2");
            tvBedrooms.setText(PropertyModel.getInstance().getNoOfBedRooms());
            tvCertificateDate.setText(getDate());
            tvCertificateNumber.setText(PropertyModel.getInstance().getCertificate_number());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDate() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd / MM / yyyy");
            return format.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void initView() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvSubTitle = (TextView) findViewById(R.id.tv_sub_title);
        tvTitleProperty = (TextView) findViewById(R.id.tv_title_property);
        left = (Guideline) findViewById(R.id.left);
        right = (Guideline) findViewById(R.id.right);
        borderProperty = (View) findViewById(R.id.border_property);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvLegalDescription = (TextView) findViewById(R.id.tv_legal_description);
        tvTitleCertificate = (TextView) findViewById(R.id.tv_title_certificate);
        linearProperty1 = (LinearLayout) findViewById(R.id.linear_property_1);
        tvDwellingType = (TextView) findViewById(R.id.tv_dwelling_type);
        tvDwellingSize = (TextView) findViewById(R.id.tv_dwelling_size);
        tvBedrooms = (TextView) findViewById(R.id.tv_bedrooms);
        llPropertyDetails2 = (LinearLayout) findViewById(R.id.ll_property_details_2);
        tvDwellingPerfomance = (TextView) findViewById(R.id.tv_dwelling_perfomance);
        ivHome = (ImageView) findViewById(R.id.iv_home);
        header = (View) findViewById(R.id.header);
        centerGuideline = (Guideline) findViewById(R.id.center_guideline);
        centerVerticalGuideline = (Guideline) findViewById(R.id.center_vertical_guideline);
        tvEnergyRating = (TextView) findViewById(R.id.tv_energy_Rating);
        tvWaterRating = (TextView) findViewById(R.id.tv_water_rating);
        energyRatingBox = (View) findViewById(R.id.energy_rating_box);
        waterRatingBox = (View) findViewById(R.id.water_rating_box);
        borderEnergy = (View) findViewById(R.id.border_energy);
        borderWater = (View) findViewById(R.id.border_water);
        tvA = (TextView) findViewById(R.id.tv_a);
        energyA = (TextView) findViewById(R.id.energy_a);
        energyB = (AppCompatTextView) findViewById(R.id.energy_b);
        energyC = (AppCompatTextView) findViewById(R.id.energy_c);
        energyD = (AppCompatTextView) findViewById(R.id.energy_d);
        energyE = (AppCompatTextView) findViewById(R.id.energy_e);
        energyF = (AppCompatTextView) findViewById(R.id.energy_f);
        tvWaterA = (TextView) findViewById(R.id.tv_water_a);
        tvWaterB = (TextView) findViewById(R.id.tv_water_b);
        tvWaterC = (TextView) findViewById(R.id.tv_water_c);
        tvWaterD = (TextView) findViewById(R.id.tv_water_d);
        tvWaterE = (TextView) findViewById(R.id.tv_water_e);
        tvWaterF = (TextView) findViewById(R.id.tv_water_f);
        waterA = (TextView) findViewById(R.id.water_a);
        waterB = (TextView) findViewById(R.id.water_b);
        waterC = (TextView) findViewById(R.id.water_c);
        waterD = (TextView) findViewById(R.id.water_d);
        waterE = (TextView) findViewById(R.id.water_e);
        waterF = (TextView) findViewById(R.id.water_f);
        tvB = (TextView) findViewById(R.id.tv_b);
        tvC = (TextView) findViewById(R.id.tv_c);
        tvD = (TextView) findViewById(R.id.tv_d);
        tvE = (TextView) findViewById(R.id.tv_e);
        tvF = (TextView) findViewById(R.id.tv_f);
        llReport = (LinearLayout) findViewById(R.id.ll_report);
        llEnergyUse = (LinearLayout) findViewById(R.id.ll_energy_use);
        tvTitleAssessment = (TextView) findViewById(R.id.tv_title_assessment);
        tvAssesscorOrganization = (TextView) findViewById(R.id.tv_assesscor_organization);
        tvOrganisationWebsite = (TextView) findViewById(R.id.tv_organisation_website);
        tvContactEmail = (TextView) findViewById(R.id.tv_contact_email);
        linearAssessment = (LinearLayout) findViewById(R.id.linear_assessment);
        tvCertificateNumber = (TextView) findViewById(R.id.tv_certificate_number);
        tvCertificateDate = (TextView) findViewById(R.id.tv_certificate_date);
        tvAssessorName = (TextView) findViewById(R.id.tv_assessor_name);
        tvRegistration = (TextView) findViewById(R.id.tv_registration);
        linearAssessment1 = (LinearLayout) findViewById(R.id.linear_assessment_1);
        tvNote = (TextView) findViewById(R.id.tv_note);
        tvRemark = (TextView) findViewById(R.id.tv_remark);
        root = findViewById(R.id.root);
    }


    private void setWaterGrade(double points) {
        if (points >= 13)
            SetGrade(R.id.water_a);
        else if (points >= 10)
            SetGrade(R.id.water_b);
        else if (points >= 7)
            SetGrade(R.id.water_c);
        else if (points >= 5)
            SetGrade(R.id.water_d);
        else if (points >= 3)
            SetGrade(R.id.water_e);
        else
            SetGrade(R.id.water_f);
    }

    private void SetGrade(int id_text) {
        TextView textView = findViewById(id_text);
        textView.setVisibility(View.VISIBLE);
    }

    private void setEnergyGrade(float points) {
        try {
            if (points > 25)
                SetGrade(R.id.energy_a);
            else if (points > 20)
                SetGrade(R.id.energy_b);
            else if (points > 15)
                SetGrade(R.id.energy_c);
            else if (points > 11)
                SetGrade(R.id.energy_d);
            else if (points > 8)
                SetGrade(R.id.energy_e);
            else if (points < 8)
                SetGrade(R.id.energy_f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
