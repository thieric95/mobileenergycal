package com.nixbymedia.energycalculator.heatingsystem;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;


import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.lighting.DayLight_Lightening;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Heating_System extends Fragment {
    ImageButton back, comment_btn, forward;
    FragmentManager fragmentManager;
    Spinner spinner, spinner_heat_air, spinner_natural_gas, spinner_hydronic, spinner_gas,
            spinner_elec_heater, spinner_wood, spinner_solar;//spinner_gas_califont

    ImageButton image_heat_to_air, image_heat_hydronic, image_gas, img_natural_gas,
            image_ele_heater, image_wood, image_solar;
    ;// image_hot_water_cylinder,image_gas_califont;

    byte[] image_heatpump_ = null, image_heat_hydronic_ = null, image_gas_califont_ = null,
            image_gas_ = null, image_ele_heater_ = null, image_wood_ = null, image_solar_ = null, image_hot_water_cylinder_ = null;

    EditText text_heat_to_air, text_heat_hydronic, text_lpg_gas,
            text_gas_natural, text_ele_heater, text_wood;
    TextView cmnt_text;

    HashMap<String, Float> water_heating;
    private Activity ctx;

    public Heating_System() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_heating_system, container, false);

        spinner_values(view, R.id.spinner_heat_to_air);
        spinner_values(view, R.id.spinner_heat_hydronic);
        spinner_values(view, R.id.spinner_gas);
        spinner_values(view, R.id.spinner_wood_pellet);
        spinner_values(view, R.id.spinner_electric_heater);
        spinner_values(view, R.id.spinner_natural_gas);
        water_heating = new HashMap<>();

        // spinner_values(view, R.id.spinner_gas_califont_efficiency);
        back = view.findViewById(R.id.heat_back);
        comment_btn = view.findViewById(R.id.heat_comment);
        forward = view.findViewById(R.id.heat_forward);

        //spinners to select data is initialized
        //    spinner_water_heating = view.findViewById(R.id.spinner_water_heating);
        spinner_natural_gas = view.findViewById(R.id.spinner_natural_gas);
        spinner_heat_air = view.findViewById(R.id.spinner_heat_to_air);
        spinner_hydronic = view.findViewById(R.id.spinner_heat_hydronic);
        spinner_gas = view.findViewById(R.id.spinner_gas);
        spinner_wood = view.findViewById(R.id.spinner_wood_pellet);
        spinner_elec_heater = view.findViewById(R.id.spinner_electric_heater);
        //spinner_gas_califont = view.findViewById(R.id.spinner_gas_califont_efficiency);
        spinner_solar = view.findViewById(R.id.spinner_back_boosted);
        setWaterHeating();
        //initialized edit text to take input from user
        text_heat_to_air = view.findViewById(R.id.text_heat_to_air);
        text_heat_hydronic = view.findViewById(R.id.text_hydronic);
        text_gas_natural = view.findViewById(R.id.text_natural_gas);
        text_wood = view.findViewById(R.id.text_wood_pellet);
        text_ele_heater = view.findViewById(R.id.text_electric_heater);
        text_lpg_gas = view.findViewById(R.id.text_lpg_gas);
        //   text_hot_water_cylinder = view.findViewById(R.id.text_hot_water_cylinder);
        //  kw_hot_w_cylinder = view.findViewById(R.id.kw_hot_water_cylinder);
        //image buttons to capture images are initialized
        img_natural_gas = view.findViewById(R.id.img_natural_gas);
        image_heat_to_air = view.findViewById(R.id.img_heatpump);
        image_heat_hydronic = view.findViewById(R.id.img_heatpumpair);
        image_gas = view.findViewById(R.id.img_gas);
        image_wood = view.findViewById(R.id.img_wood);
        image_ele_heater = view.findViewById(R.id.img_ele_heater);
        //    image_gas_califont = view.findViewById(R.id.img_gas_calfont);
        // image_hot_water_cylinder = view.findViewById(R.id.img_hot_water);
        image_solar = view.findViewById(R.id.img_solar);

        cmnt_text = view.findViewById(R.id.comment_heating_text);

//        HeatingBackTask heatingBackTask = new HeatingBackTask(getContext(), null, null, null,
//                null, null, null, null, null);
//        heatingBackTask.execute("remove");


        // these functions are overridden to capture the image from camera

        image_heat_to_air.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heatair + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heatair, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        img_natural_gas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.naturalgas + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.naturalgas, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_heat_hydronic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heathydronic + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.heathydronic, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_gas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.gas + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.gas, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_wood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wood + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Wood, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });
        image_ele_heater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.ElectricHeater + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.ElectricHeater, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });

        image_solar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Solar + "_" + System.currentTimeMillis(), Extra.SubPath.HeatingSystem, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.Solar, Extra.SubPath.HeatingSystem, getActivity());
                }

            }
        });

        fragmentManager = getActivity().getSupportFragmentManager();
        back.setEnabled(true);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((StepProcessAct) getActivity()).replaceFragment(new Thermal_Envelope(), Extra.Tag.property);
            }
        });

        comment_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).openComment(Extra.Tag.HeatingSystem);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getActivity().getSupportFragmentManager();
                saveHeatingPoints();
                saveWaterHeating();
                ((StepProcessAct) getActivity()).replaceFragment(new DayLight_Lightening(), Extra.Tag.Daylight);
            }
        });
        return view;
    }

    public void setWaterHeating() {
        try {
            water_heating.put("Solar thermal + Wetback", 6f);
            water_heating.put("Heat pump + wetback", 6f);
            water_heating.put("Geothermal", 6f);
            water_heating.put("Solar thermal", 5f);
            water_heating.put("Heat Pump", 5f);
            water_heating.put("Electric + wetback", 4f);
            water_heating.put("Electric", 3f);
            water_heating.put("Gas califont + wetback", 2f);
            water_heating.put("Gas califont", 1f);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            adapter.addAll(water_heating.keySet());
            spinner_solar.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveHeatingPoints() {
        try {
            float points_achieved = 0.0f;

            points_achieved = points_achieved + (((Extra.EmmisionFactor.heat_pump_emission_fact /
                    Extra.COP.heat_pump_cop) * Float.parseFloat(spinner_heat_air.getSelectedItem().toString())) / 100);


            points_achieved = points_achieved + (((Extra.EmmisionFactor.heat_pump_emission_fact /
                    Extra.COP.heat_pump_cop) * Float.parseFloat(spinner_hydronic.getSelectedItem().toString())) / 100);


            // if (!spinner_gas.getSelectedItem().toString().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.lpg_gas_emission_fact /
                    Extra.COP.lpg_gas) * Float.parseFloat(spinner_gas.getSelectedItem().toString())) / 100);
            //  }

            // if (!spinner_natural_gas.getSelectedItem().toString().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.natural_gas_emission_fact /
                    Extra.COP.natural_gas) * Float.parseFloat(spinner_natural_gas.getSelectedItem().toString())) / 100);
            // }

            points_achieved = points_achieved + (((Extra.EmmisionFactor.electric_emission_fact /
                    Extra.COP.electric) * Float.parseFloat(spinner_elec_heater.getSelectedItem().toString())) / 100);
            //  }
            // if (!HeatModel.getInstance().getWood_burner().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.wood_burner_emission_fact /
                    Extra.COP.wood_burner) * Float.parseFloat(spinner_wood.getSelectedItem().toString())) / 100);
            //  }
            // if (!HeatModel.getInstance().getOpen_fire().isEmpty()) {
            points_achieved = points_achieved + (((Extra.EmmisionFactor.open_fire_emission_fact /
                    Extra.COP.open_fire) * Float.parseFloat(spinner_wood.getSelectedItem().toString())) / 100);
            // }

            HeatModel.getInstance().setPoints(points_achieved);

            if (points_achieved <= 10)
                HeatModel.getInstance().setPoints(6f);
            else if (points_achieved <= 20)
                HeatModel.getInstance().setPoints(5f);
            else if (points_achieved <= 30)
                HeatModel.getInstance().setPoints(4f);
            else if (points_achieved <= 40)
                HeatModel.getInstance().setPoints(3f);
            else if (points_achieved <= 60)
                HeatModel.getInstance().setPoints(2f);
            else if (points_achieved > 60)
                HeatModel.getInstance().setPoints(1f);

            HeatModel.getInstance().setEletric_heater(spinner_elec_heater.getSelectedItem().toString());
            HeatModel.getInstance().setHeat_pump_to_air(spinner_heat_air.getSelectedItem().toString());
            HeatModel.getInstance().setHeat_pump_hydrolic(spinner_hydronic.getSelectedItem().toString());
            HeatModel.getInstance().setLpg_gas(spinner_gas.getSelectedItem().toString());
            HeatModel.getInstance().setNatural_gas(spinner_natural_gas.getSelectedItem().toString());
            HeatModel.getInstance().setWood_burner(spinner_wood.getSelectedItem().toString());
            HeatModel.getInstance().setEletric_heater_kw(text_ele_heater.getText().toString());
            HeatModel.getInstance().setHeat_pump_to_air_kw(text_heat_to_air.getText().toString());
            HeatModel.getInstance().setLpg_gas_kw(text_wood.getText().toString());
            HeatModel.getInstance().setHeat_pump_hydrolic_kw(text_heat_hydronic.getText().toString());
            HeatModel.getInstance().setNatural_gas_kw(text_lpg_gas.getText().toString());
            HeatModel.getInstance().setWood_burner_kw(text_wood.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveWaterHeating() {
        try {
            WaterHeating.getInstance().setWater_heating(spinner_solar.getSelectedItem().toString());
            WaterHeating.getInstance().setPoints(water_heating.get(spinner_solar.getSelectedItem().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        switch (requestCode) {
//            case Extra.ActivityRequestCode.CAMERA_REQUEST_CODE:
//                ((BaseActivity) getActivity()).saveToPdf(Extra.SubPath.HeatingSystem);
//                break;
//        }
//        if (requestCode == 1 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_heatpump_ = extras.getByteArray("data");
//        } else if (requestCode == 2 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_heat_hydronic_ = extras.getByteArray("data");
//        } else if (requestCode == 3 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_gas_ = extras.getByteArray("data");
//        } else if (requestCode == 4 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_wood_ = extras.getByteArray("data");
//        } else if (requestCode == 5 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_ele_heater_ = extras.getByteArray("data");
//        } else if (requestCode == 6 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_gas_califont_ = extras.getByteArray("data");
//        } else if (requestCode == 7 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_hot_water_cylinder_ = extras.getByteArray("data");
//        } else if (requestCode == 8 && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            image_solar_ = extras.getByteArray("data");
//        }
    // }


    private void spinner_values(View v, int id) {
        spinner = v.findViewById(id);
        List age = new ArrayList<Integer>();
        for (int i = 0; i <= 100; i = i + 10) {
            age.add(Integer.toString(i));
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(
                v.getContext(), android.R.layout.simple_spinner_item, age);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
