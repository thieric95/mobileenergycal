package com.nixbymedia.energycalculator.thermal_envelope;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.nixbymedia.energycalculator.Activity_Home;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.Heating_System;


/**
 * A simple {@link Fragment} subclass.
 */
public class Thermal_Envelope extends Fragment {

    ImageButton back, forward, comment;
    private  android.support.v4.app.FragmentManager fragmentManager;
    Spinner sp_index;
    String[] index = new String[]{"< 1", "< 1.2", "< 1.4", "< 1.55", "< 1.77", "> 1.7"};
    int[] points = new int[]{10, 8, 6, 4, 2, 0};
    private Activity ctx;

    public Thermal_Envelope() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.thermal_envelope_layout, container, false);

        // cmnt = view.findViewById(R.id.thermal_comment_text);
        sp_index = view.findViewById(R.id.sp_index);
        comment = view.findViewById(R.id.thermal_comment);
        back = view.findViewById(R.id.thermal_back);
        forward = view.findViewById(R.id.thermal_forward);

        setSpinner();
        fragmentManager = getActivity().getSupportFragmentManager();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPoints(sp_index.getSelectedItemPosition());
                ((StepProcessAct) getActivity()).replaceFragment(((StepProcessAct)getActivity()).propertyDetail, Extra.Tag.property);
            }
        });

//        ThermalBackTask thermalBackTask = new ThermalBackTask(getContext());
//        thermalBackTask.execute("remove");
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity)getActivity()).openComment(Extra.Tag.thermal);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPoints(sp_index.getSelectedItemPosition());
                ((StepProcessAct) getActivity()).replaceFragment(new Heating_System(), Extra.Tag.HeatingSystem);
            }
        });


        return view;
    }

    private void setPoints(int pos) {
        try {
            if (pos < points.length) {
                ThermalEnvelope.getInstance().setPoints(points[pos]);
                ThermalEnvelope.getInstance().setIndex(sp_index.getSelectedItem().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinner() {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
            adapter.addAll(index);
            sp_index.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
