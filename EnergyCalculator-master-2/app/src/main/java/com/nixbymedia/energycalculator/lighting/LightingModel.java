package com.nixbymedia.energycalculator.lighting;

public class LightingModel {
    String led_light = "", cfl_light = "", halogen_light = "";
    float total_indoor_light =0,total_outdoor_light=0, total_indoor_wattage = 0;
    int sensor_wattage =0;
    float total_points = 0f;
    private String inspectorlightview;
    private String comment;
    private String lux_meter;
    private int id;
    private byte[] image_1, image_2, image_3, image_4, image_5, image_6;
    public int getId() {
        return id;
    }
    private int total_outdoor_wattage = 0;

    public void setId(int id) {
        this.id = id;
    }

    public String getInspectorlightview() {
        return inspectorlightview;
    }

    public void setInspectorlightview(String inspectorlightview) {
        this.inspectorlightview = inspectorlightview;
    }

    public float getTotal_indoor_wattage() {
        return total_indoor_wattage;
    }

    public void setTotal_indoor_wattage(float total_indoor_wattage) {
        this.total_indoor_wattage = total_indoor_wattage;
    }

    private static LightingModel lightingModel = new LightingModel();

    public float getTotal_indoor_light() {
        return total_indoor_light;
    }

    public void setTotal_indoor_light(float total_indoor_light) {
        this.total_indoor_light = total_indoor_light;
    }

    public float getTotal_outdoor_light() {
        return total_outdoor_light;
    }

    public void setTotal_outdoor_light(float total_outdoor_light) {
        this.total_outdoor_light = total_outdoor_light;
    }

    public int getTotal_outdoor_wattage() {
        return total_outdoor_wattage;
    }

    public void setTotal_outdoor_wattage(int total_outdoor_wattage) {
        this.total_outdoor_wattage = total_outdoor_wattage;
    }

    public static LightingModel getInstance() {
        return lightingModel;
    }

    public String getLed_light() {
        return led_light;
    }

    public void setLed_light(String led_light) {
        this.led_light = led_light;
    }

    public String getCfl_light() {
        return cfl_light;
    }

    public void setCfl_light(String cfl_light) {
        this.cfl_light = cfl_light;
    }

    public String getHalogen_light() {
        return halogen_light;
    }

    public void setHalogen_light(String halogen_light) {
        this.halogen_light = halogen_light;
    }



    public int getSensor_wattage() {
        return sensor_wattage;
    }

    public void setSensor_wattage(int sensor_wattage) {
        this.sensor_wattage = sensor_wattage;
    }

    public float getTotal_points() {
        return total_points;
    }

    public void setTotal_points(float total_points) {
        this.total_points = total_points;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getLux_meter() {
        return lux_meter;
    }

    public void setLux_meter(String lux_meter) {
        this.lux_meter = lux_meter;
    }

    public byte[] getImage_1() {
        return image_1;
    }

    public void setImage_1(byte[] image_1) {
        this.image_1 = image_1;
    }

    public byte[] getImage_2() {
        return image_2;
    }

    public void setImage_2(byte[] image_2) {
        this.image_2 = image_2;
    }

    public byte[] getImage_3() {
        return image_3;
    }

    public void setImage_3(byte[] image_3) {
        this.image_3 = image_3;
    }

    public byte[] getImage_4() {
        return image_4;
    }

    public void setImage_4(byte[] image_4) {
        this.image_4 = image_4;
    }

    public byte[] getImage_5() {
        return image_5;
    }

    public void setImage_5(byte[] image_5) {
        this.image_5 = image_5;
    }

    public byte[] getImage_6() {
        return image_6;
    }

    public void setImage_6(byte[] image_6) {
        this.image_6 = image_6;
    }

    @Override
    public String toString() {
        return "LightingModel{" +
                "led_light='" + led_light + '\'' +
                ", cfl_light='" + cfl_light + '\'' +
                ", halogen_light='" + halogen_light + '\'' +
                ", total_indoor_light=" + total_indoor_light +
                ", total_outdoor_light=" + total_outdoor_light +
                ", total_indoor_wattage=" + total_indoor_wattage +
                ", sensor_wattage=" + sensor_wattage +
                ", total_points=" + total_points +
                ", inspectorlightview='" + inspectorlightview + '\'' +
                ", lux_meter='" + lux_meter + '\'' +
                ", id=" + id +
                ", total_outdoor_wattage=" + total_outdoor_wattage +
                '}';
    }
}
