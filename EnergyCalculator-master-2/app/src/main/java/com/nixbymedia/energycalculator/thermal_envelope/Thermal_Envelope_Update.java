package com.nixbymedia.energycalculator.thermal_envelope;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.EditingSection;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.SchemaHelperClass;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.StepProcessActUpdate;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.Heating_System;
import com.nixbymedia.energycalculator.heatingsystem.Heating_SystemUpdate;
import com.nixbymedia.energycalculator.property.PropertyDetailUpdate;
import com.nixbymedia.energycalculator.property.PropertyModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Thermal_Envelope_Update extends Fragment {

    ImageButton back, forward, comment;
    private android.support.v4.app.FragmentManager fragmentManager;
    Spinner sp_index;
    String[] index = new String[]{"< 1", "< 1.2", "< 1.4", "< 1.55", "< 1.77", "> 1.7"};
    int[] points = new int[]{10, 8, 6, 4, 2, 0};
    private Activity ctx;

    private int userid;
    private String CertifiNo;
    ThermalEnvelope thermalmodel = new ThermalEnvelope();
    DatabaseHelperClass dbhelper;

    public Thermal_Envelope_Update() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.CertifiNo = getArguments().getString("propertyid");
        this.userid = getArguments().getInt("userid");
        // Inflate the layout for this fragment
        this.dbhelper = new DatabaseHelperClass(getContext());
        View view = inflater.inflate(R.layout.fragment_thermal__envelope__update, container, false);

        this.getThermal(this.userid, this.CertifiNo);
        // cmnt = view.findViewById(R.id.thermal_comment_text);
        sp_index = view.findViewById(R.id.sp_index_update);
        comment = view.findViewById(R.id.thermal_comment);
        back = view.findViewById(R.id.thermal_back);
        forward = view.findViewById(R.id.thermal_forward);



        fragmentManager = getActivity().getSupportFragmentManager();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPoints(sp_index.getSelectedItemPosition());
                PropertyDetailUpdate property_update = new PropertyDetailUpdate();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", CertifiNo);
                bundle.putInt("userid", userid);
                property_update.setArguments(bundle);
                ((StepProcessActUpdate) getActivity()).replaceFragment(property_update, Extra.Tag.property);
            }
        });

//        ThermalBackTask thermalBackTask = new ThermalBackTask(getContext());
//        thermalBackTask.execute("remove");
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).openComment(Extra.Tag.thermal);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPoints(sp_index.getSelectedItemPosition());
                EditingSection.Iseditinggroup.isthermaledited = true;
                Heating_SystemUpdate heating_update = new Heating_SystemUpdate();
                Bundle bundle = new Bundle();
                bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
                bundle.putInt("userid", userid);
                heating_update.setArguments(bundle);
                ((StepProcessActUpdate) getActivity()).replaceFragment(heating_update, Extra.Tag.HeatingSystem);
            }
        });
        setSpinner();
        return view;
    }


    private void setPoints(int pos) {
        try {
            if (pos < points.length) {
                ThermalEnvelope.getInstance().setPoints(points[pos]);
                ThermalEnvelope.getInstance().setIndex(sp_index.getSelectedItem().toString());
            }
            ThermalEnvelope.getInstance().setId(thermalmodel.getId());
            ThermalEnvelope.getInstance().setComment(BaseActivity.getComment(Extra.Tag.thermal));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinner() {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
            adapter.addAll(index);

            sp_index.setAdapter(adapter);

            if (!EditingSection.Iseditinggroup.isthermaledited) {
                System.out.println(thermalmodel.getIndex());
                for (int i = 0; i < this.sp_index.getCount(); i++) {

                    if (sp_index.getItemAtPosition(i).toString().equals(thermalmodel.getIndex())) {
                        sp_index.setSelection(i);
                    }
                }



            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getThermal(int userid, String properID) {
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLTHERMALSELECT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(1200, TimeUnit.SECONDS)
                .readTimeout(1200, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("id", Integer.toString(userid))
                .add("prop_id", properID);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                        () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    String jsonData = response.body().string();
                    Log.i("Data", jsonData);
                    try {
                        Log.e("Get JSON", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.has("error")) {
                            if (jsonObject.getBoolean("error")) {
                                String message = jsonObject.getString("message");
                                ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                        (Runnable) () -> Toast.makeText(getContext(), "Error: " + message, Toast.LENGTH_SHORT).show()
                                );

                            }
                        } else {
                            thermalmodel.setId(jsonObject.getInt("thermalid"));
                            thermalmodel.setIndex(jsonObject.getString("thermalindex"));
                            thermalmodel.setPoints(jsonObject.getInt("thermalpoint"));
                            thermalmodel.setComment(jsonObject.getString("asstcomment"));

                            ((StepProcessActUpdate) getActivity()).runOnUiThread(
                                    (Runnable) () -> {
                                        setSpinner();
                                        bindtocomponent();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ((StepProcessActUpdate) getActivity()).runOnUiThread((Runnable)
                            () -> Toast.makeText(getActivity(), "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }


    public void bindtocomponent() {
        if (EditingSection.Iseditinggroup.isthermaledited) {
            for (int i = 0; i < this.sp_index.getCount(); i++) {
                if (sp_index.getItemAtPosition(i).toString().equals(ThermalEnvelope.getInstance().getIndex())) {
                    System.out.println(sp_index.getItemAtPosition(i).toString().equals(ThermalEnvelope.getInstance().getIndex()));
                    sp_index.setSelection(i);
                }
            }


            BaseActivity.saveComment(ThermalEnvelope.getInstance().getComment(), Extra.Tag.thermal);
        }else{
            BaseActivity.saveComment(thermalmodel.getComment(), Extra.Tag.thermal);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}