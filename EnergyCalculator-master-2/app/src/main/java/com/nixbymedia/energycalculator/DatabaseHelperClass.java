package com.nixbymedia.energycalculator;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.extra.EnergyAPI;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DatabaseHelperClass {

    private String result;
    private Activity ctx;
    private StringRequest stringRequest;
    private RequestQueue reqQueue;
    private JsonObjectRequest jsonObjectRequest;
    ArrayList<PropertyModel> propertyList;


    //matching constructor of database helper class to create database
    public DatabaseHelperClass(Context context) {
        this.ctx = (Activity)context;
        this.reqQueue = Volley.newRequestQueue(this.ctx);
    }


//    public Response getAllPropertybyUserID(int userID) throws IOException {
//        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROJECTLIST;
//        //ArrayList<PropertyModel> propertyList = new ArrayList<>();
//        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
//                .writeTimeout(1200, TimeUnit.SECONDS)
//                .readTimeout(1200, TimeUnit.SECONDS)
//                .build();
//        FormBody.Builder formBuilder = new FormBody.Builder()
//                .add("id", Integer.toString(userID));
//        RequestBody formBody = formBuilder.build();
//        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
//                .header("Content-Type", "application/json").build();
//        Response res = okhttp.newCall(request).execute();
//        if(res != null){
//            return res;
//        }
//        return null;
//    }










}
