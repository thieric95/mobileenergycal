package com.nixbymedia.energycalculator.water_user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.nixbymedia.energycalculator.BackgroundOperations.WaterUseBackTask;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.Splash_Screen_Process;
import com.nixbymedia.energycalculator.StepProcessAct;
import com.nixbymedia.energycalculator.extra.Debugger;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.lighting.DayLight_Lightening;

public class Water_Use extends Fragment {

    private ImageButton image1, image2, image3, back, forward, comment;
    private EditText kitchen_tap, worst_basin_tap, worst_shower;
    private RadioButton rb1, rb2;
    private Spinner spinner1;
    private TextView cmnt;
    private WaterUseBackTask waterUseBackTask;
    private String rbst1, rbst2;
    private FragmentManager fragmentManager;
    private View view;

    public Water_Use() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_water_use, container, false);

        image1 = view.findViewById(R.id.img_kitchen_tap);
        image2 = view.findViewById(R.id.img_basin_tap);
        image3 = view.findViewById(R.id.img_worst_shower);

        cmnt = view.findViewById(R.id.water_comment_text);

        kitchen_tap = view.findViewById(R.id.text_kitchen_tap);
        worst_basin_tap = view.findViewById(R.id.text_basin_tap);
        worst_shower = view.findViewById(R.id.text_worst_shower);

        rb1 = view.findViewById(R.id.rb_flow_inspect);
        rb2 = view.findViewById(R.id.rb_doc_inspect);

        spinner1 = view.findViewById(R.id.spinner_heat_to_air_water);

        back = view.findViewById(R.id.water_back);
        forward = view.findViewById(R.id.water_forward);
        comment = view.findViewById(R.id.water_comment);
        rb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbst1 = "YES";
                } else {
                    rbst1 = "NO";
                }
            }
        });

        rb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbst2 = "YES";
                } else {
                    rbst2 = "NO";
                }
            }
        });


        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.kitchen_tap + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.kitchen_tap, Extra.SubPath.Water, getActivity());
                }

            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.basin_tap + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.basin_tap, Extra.SubPath.Water, getActivity());
                }

            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    //((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.worst_shower + "_" + System.currentTimeMillis(), Extra.SubPath.Water, getActivity());
                    ((StepProcessAct) getActivity()).openCamera(Extra.FileConstants.worst_shower, Extra.SubPath.Water, getActivity());
                }

            }
        });
//
//        waterUseBackTask = new WaterUseBackTask(getContext(), img1, img2, img3);
//        waterUseBackTask.execute("remove");

        fragmentManager = getActivity().getSupportFragmentManager();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StepProcessAct) getActivity()).replaceFragment(new DayLight_Lightening(), Extra.Tag.Daylight);
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StepProcessAct) getActivity()).openComment(Extra.Tag.WaterUse);
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    savePoints();
                    Intent intent = new Intent(getActivity(), Splash_Screen_Process.class);
                    intent.putExtra("option", "adding");
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });

        return view;
    }

    private boolean isValidate() {
        try {
            if (kitchen_tap.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Kitchen Tap", view);
                return false;
            } else if (worst_basin_tap.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Worst Basin Tap", view);
                return false;
            } else if (worst_shower.getText().toString().isEmpty()) {
                ((StepProcessAct) getActivity()).showError("Please enter Worst Shower", view);
                return false;
            } else
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private void savePoints() {
        try {
            saveShowerPoint();
            saveKitchenPoint();
            saveBathroomPoint();
            saveToiletPoint();
            this.saveInspector();
            float total_points = WaterUseModel.getInstance().getBasin_tap_point()
                    + WaterUseModel.getInstance().getKitchen_tap_point()
                    + WaterUseModel.getInstance().getShower_point()
                    + WaterUseModel.getInstance().getWorst_Wc();
            WaterUseModel.getInstance().setTotal_points(total_points);
            WaterUseModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.WaterUse));
            Debugger.debugE("water", WaterUseModel.getInstance().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveShowerPoint() {
        try {
            if (Float.parseFloat(worst_shower.getText().toString()) <= 7.5) {
                WaterUseModel.getInstance().setShower_point(4f);
            } else if (Float.parseFloat(worst_shower.getText().toString()) <= 9) {
                WaterUseModel.getInstance().setShower_point(3.5f);
            } else if (Float.parseFloat(worst_shower.getText().toString()) > 9) {
                WaterUseModel.getInstance().setShower_point(0f);
            }
            WaterUseModel.getInstance().setShower(worst_shower.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveKitchenPoint() {
        try {
            if (Float.parseFloat(kitchen_tap.getText().toString()) <= 4.5) {
                WaterUseModel.getInstance().setKitchen_tap_point(1.5f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) <= 6) {
                WaterUseModel.getInstance().setKitchen_tap_point(1f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) <= 7.5) {
                WaterUseModel.getInstance().setKitchen_tap_point(0.5f);
            } else if (Float.parseFloat(kitchen_tap.getText().toString()) > 7.5) {
                WaterUseModel.getInstance().setKitchen_tap_point(0f);
            }
            WaterUseModel.getInstance().setKitchen_tap(kitchen_tap.getText().toString());
            //System.out.println(WaterUseModel.getInstance().getKitchen_tap_point());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveBathroomPoint() {
        try {
            if (Float.parseFloat(worst_basin_tap.getText().toString()) <= 4.5) {
                WaterUseModel.getInstance().setBasin_tap_point(1.5f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) <= 6) {
                WaterUseModel.getInstance().setBasin_tap_point(1f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) <= 7.5) {
                WaterUseModel.getInstance().setBasin_tap_point(0.5f);
            } else if (Float.parseFloat(worst_basin_tap.getText().toString()) > 7.5) {
                WaterUseModel.getInstance().setBasin_tap_point(0f);
            }
            WaterUseModel.getInstance().setBasin_tap(worst_basin_tap.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //start from saveToiletPoint.
    private void saveToiletPoint() {
        try {
            Debugger.debugE("selectedItem", spinner1.getSelectedItem().toString());
            switch (getResources().getStringArray(R.array.water_wc)[spinner1.getSelectedItemPosition()]) {
                case "> 4.5 WELS":
                    WaterUseModel.getInstance().setWorst_Wc(3f);
                    break;
                case "4 star WELS":
                    WaterUseModel.getInstance().setWorst_Wc(2f);
                    break;
                case "3 star WELS":
                    WaterUseModel.getInstance().setWorst_Wc(1f);
                    break;
                case "single flush":
                    WaterUseModel.getInstance().setWorst_Wc(0f);
                    break;
            }
            WaterUseModel.getInstance().setWc_point(spinner1.getSelectedItem().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveInspector(){
        try{
            this.rbst1 = this.rb1.isChecked() ? "YES" : "NO";
            this.rbst2 = this.rb2.isChecked() ? "YES" : "NO";
            WaterUseModel.getInstance().setMeasuredInspect(this.rbst1);
            WaterUseModel.getInstance().setDocInspect(this.rbst2);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
