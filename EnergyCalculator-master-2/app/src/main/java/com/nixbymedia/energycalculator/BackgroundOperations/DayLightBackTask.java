package com.nixbymedia.energycalculator.BackgroundOperations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.Login_Activity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DayLightBackTask{

    Activity ctx;
    byte[] img1, img2, img3,img4, img5, img6;
    private int user_id;
    private String pro_id;
    RequestQueue requestQueue;
    public DayLightBackTask(Context context, byte[] image1, byte[] image2, byte[] image3, byte[] image4,
                            byte[] image5, byte[] image6){
        this.ctx = (Activity) context;
        this.img1 = image1;
        this.img2 = image2;
        this.img3 = image3;
        this.img4 = image4;
        this.img5 = image5;
        this.img6 = image6;

    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public void insert(String... params){
        System.out.println(Arrays.toString(params));
        String lux_meter = params[0];
        String no_led = params[1];
        String no_fluo = params[2];
        String no_halog = params[3];
        String total_in_watt = params[4];
        String total_in_light = params[5];
        String total_out_watt = params[6];
        String watt_sensor = params[7];
        String total_out_light = params[8];
        String light_inspect = params[9];
        String lights_points = params[10];
        String comment = params[11];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLLIGHTINGINSERT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1500, TimeUnit.SECONDS)
                .readTimeout(1500, TimeUnit.SECONDS)
                .build();
        RequestBody body  = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("lux_meter", lux_meter)
                    .addFormDataPart("no_led", no_led)
                    .addFormDataPart("no_led_img", "no_led_img.jpg", RequestBody.create(this.img1, MediaType.parse("image/*")))
                    .addFormDataPart("no_fluo", no_fluo)
                    .addFormDataPart("no_fluo_img","no_fluo_img.jpg", RequestBody.create(this.img2, MediaType.parse("image/*")))
                    .addFormDataPart("no_halog", no_halog)
                    .addFormDataPart("no_halog_img", "no_halog_img.jpg", RequestBody.create(this.img3, MediaType.parse("image/*")))
                    .addFormDataPart("total_in_watt", total_in_watt)
                    .addFormDataPart("total_in_watt_img", "total_in_watt_img.jpg", RequestBody.create(this.img4, MediaType.parse("image/*")))
                    .addFormDataPart("total_in_light", total_in_light)
                    .addFormDataPart("total_out_watt", total_out_watt)
                    .addFormDataPart("total_out_watt_img", "total_out_watt_img.jpg", RequestBody.create(this.img5, MediaType.parse("image/*")))
                    .addFormDataPart("watt_sensor", watt_sensor)
                    .addFormDataPart("watt_sensor_img", "watt_sensor_img.jpg", RequestBody.create(this.img6, MediaType.parse("image/*")))
                    .addFormDataPart("total_out_light", total_out_light)
                    .addFormDataPart("light_inspect", light_inspect)
                    .addFormDataPart("light_points", lights_points)
                    .addFormDataPart("asstcomment", comment)
                    .addFormDataPart("prop_id", this.pro_id)
                    .addFormDataPart("userid", Integer.toString(this.user_id)).build();


        //RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                ctx.runOnUiThread(
                        () -> Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show()
                );

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Response", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            Log.e("Error", message);
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    ctx.runOnUiThread(
                            () -> Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show()
                    );

                }
            }
        });
    }

    public void update(String... params){
        System.out.println(Arrays.toString(params));
        String lux_meter = params[0];
        String no_led = params[1];
        String no_fluo = params[2];
        String no_halog = params[3];
        String total_in_watt = params[4];
        String total_in_light = params[5];
        String total_out_watt = params[6];
        String watt_sensor = params[7];
        String total_out_light = params[8];
        String light_inspect = params[9];
        String lights_points = params[10];
        String comment = params[11];
        String daylightid = params[12];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLLIGHTINGUPDATE;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1500, TimeUnit.SECONDS)
                .readTimeout(1500, TimeUnit.SECONDS)
                .build();
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("lux_meter", lux_meter)
                    .addFormDataPart("no_led", no_led)
                    .addFormDataPart("no_led_img", "no_led_img.jpg", RequestBody.create(this.img1, MediaType.parse("image/*jpg")))
                    .addFormDataPart("no_fluo", no_fluo)
                    .addFormDataPart("no_fluo_img","no_fluo_img.jpg", RequestBody.create(this.img2, MediaType.parse("image/*jpg")))
                    .addFormDataPart("no_halog", no_halog)
                    .addFormDataPart("no_halog_img", "no_halog_img.jpg", RequestBody.create(this.img3, MediaType.parse("image/*jpg")))
                    .addFormDataPart("total_in_watt", total_in_watt)
                    .addFormDataPart("total_in_watt_img", "total_in_watt_img.jpg", RequestBody.create(this.img4, MediaType.parse("image/*jpg")))
                    .addFormDataPart("total_in_light", total_in_light)
                    .addFormDataPart("total_in_light", total_in_light)
                    .addFormDataPart("total_out_watt", total_out_watt)
                    .addFormDataPart("total_out_watt_img", "total_out_watt_img.jpg", RequestBody.create(this.img5, MediaType.parse("image/*jpg")))
                    .addFormDataPart("watt_sensor", watt_sensor)
                    .addFormDataPart("watt_sensor_img", "watt_sensor_img.jpg", RequestBody.create(this.img6, MediaType.parse("image/*jpg")))
                    .addFormDataPart("total_out_light", total_out_light)
                    .addFormDataPart("light_inspect", light_inspect)
                    .addFormDataPart("light_points", lights_points)
                    .addFormDataPart("asstcomment", comment)
                    .addFormDataPart("prop_id", this.pro_id)
                    .addFormDataPart("userid", Integer.toString(this.user_id))
                    .addFormDataPart("daylightid", daylightid).build();
        Request request = new Request.Builder().url(api).post(body).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Response", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
