package com.nixbymedia.energycalculator.heatingsystem;

import android.os.Parcel;
import android.os.Parcelable;

public class HeatModel implements Parcelable {

    String heat_pump_to_air = "", lpg_gas = "", natural_gas = "", Wood_burner = "", eletric_heater = "", open_fire = "";
    String heat_pump_hydrolic = "";
    String heat_pump_to_air_kw = "", lpg_gas_kw = "", natural_gas_kw = "", Wood_burner_kw = "", eletric_heater_kw = "", open_fire_kw = "", heat_pump_hydrolic_kw = "";
    float points = 0f;

    private int id;
    private byte[] image1, image2, image3, image4, image5, image6, image7;
    private String comment;

    private static HeatModel heatModel = new HeatModel();

    public static HeatModel getInstance(){
        return heatModel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeat_pump_hydrolic_kw() {
        return heat_pump_hydrolic_kw;
    }

    public void setHeat_pump_hydrolic_kw(String heat_pump_hydrolic_kw) {
        this.heat_pump_hydrolic_kw = heat_pump_hydrolic_kw;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHeat_pump_to_air_kw() {
        return heat_pump_to_air_kw;
    }

    public void setHeat_pump_to_air_kw(String heat_pump_to_air_kw) {
        this.heat_pump_to_air_kw = heat_pump_to_air_kw;
    }

    public String getLpg_gas_kw() {
        return lpg_gas_kw;
    }

    public void setLpg_gas_kw(String lpg_gas_kw) {
        this.lpg_gas_kw = lpg_gas_kw;
    }

    public String getNatural_gas_kw() {
        return natural_gas_kw;
    }

    public void setNatural_gas_kw(String natural_gas_kw) {
        this.natural_gas_kw = natural_gas_kw;
    }

    public String getWood_burner_kw() {
        return Wood_burner_kw;
    }

    public void setWood_burner_kw(String wood_burner_kw) {
        Wood_burner_kw = wood_burner_kw;
    }

    public String getEletric_heater_kw() {
        return eletric_heater_kw;
    }

    public void setEletric_heater_kw(String eletric_heater_kw) {
        this.eletric_heater_kw = eletric_heater_kw;
    }

    public String getOpen_fire_kw() {
        return open_fire_kw;
    }

    public void setOpen_fire_kw(String open_fire_kw) {
        this.open_fire_kw = open_fire_kw;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public String getHeat_pump_hydrolic() {
        return heat_pump_hydrolic;
    }

    public void setHeat_pump_hydrolic(String heat_pump_hydrolic) {
        this.heat_pump_hydrolic = heat_pump_hydrolic;
    }

    public String getHeat_pump_to_air() {
        return heat_pump_to_air;
    }

    public void setHeat_pump_to_air(String heat_pump_to_air) {
        this.heat_pump_to_air = heat_pump_to_air;
    }

    public String getLpg_gas() {
        return lpg_gas;
    }

    public void setLpg_gas(String lpg_gas) {
        this.lpg_gas = lpg_gas;
    }

    public String getNatural_gas() {
        return natural_gas;
    }

    public void setNatural_gas(String natural_gas) {
        this.natural_gas = natural_gas;
    }

    public String getWood_burner() {
        return Wood_burner;
    }

    public void setWood_burner(String wood_burner) {
        Wood_burner = wood_burner;
    }

    public String getEletric_heater() {
        return eletric_heater;
    }

    public void setEletric_heater(String eletric_heater) {
        this.eletric_heater = eletric_heater;
    }

    public String getOpen_fire() {
        return open_fire;
    }

    public void setOpen_fire(String open_fire) {
        this.open_fire = open_fire;
    }


    public byte[] getImage1() {
        return image1;
    }

    public void setImage1(byte[] image1) {
        this.image1 = image1;
    }

    public byte[] getImage2() {
        return image2;
    }

    public void setImage2(byte[] image2) {
        this.image2 = image2;
    }

    public byte[] getImage3() {
        return image3;
    }

    public void setImage3(byte[] image3) {
        this.image3 = image3;
    }

    public byte[] getImage4() {
        return image4;
    }

    public void setImage4(byte[] image4) {
        this.image4 = image4;
    }

    public byte[] getImage5() {
        return image5;
    }

    public void setImage5(byte[] image5) {
        this.image5 = image5;
    }

    public byte[] getImage6() {
        return image6;
    }

    public void setImage6(byte[] image6) {
        this.image6 = image6;
    }

    public byte[] getImage7() {
        return image7;
    }

    public void setImage7(byte[] image7) {
        this.image7 = image7;
    }


    @Override
    public String toString() {
        return "HeatModel{" +
                "heat_pump_to_air='" + heat_pump_to_air + '\'' +
                ", lpg_gas='" + lpg_gas + '\'' +
                ", natural_gas='" + natural_gas + '\'' +
                ", Wood_burner='" + Wood_burner + '\'' +
                ", eletric_heater='" + eletric_heater + '\'' +
                ", open_fire='" + open_fire + '\'' +
                ", heat_pump_hydrolic='" + heat_pump_hydrolic + '\'' +
                ", heat_pump_to_air_kw='" + heat_pump_to_air_kw + '\'' +
                ", lpg_gas_kw='" + lpg_gas_kw + '\'' +
                ", natural_gas_kw='" + natural_gas_kw + '\'' +
                ", Wood_burner_kw='" + Wood_burner_kw + '\'' +
                ", eletric_heater_kw='" + eletric_heater_kw + '\'' +
                ", open_fire_kw='" + open_fire_kw + '\'' +
                ", heat_pump_hydrolic_kw='" + heat_pump_hydrolic_kw + '\'' +
                ", points=" + points +
                ", comment='" + comment + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.heat_pump_to_air);
        dest.writeString(this.lpg_gas);
        dest.writeString(this.natural_gas);
        dest.writeString(this.Wood_burner);
        dest.writeString(this.eletric_heater);
        dest.writeString(this.open_fire);
        dest.writeString(this.heat_pump_hydrolic);
        dest.writeFloat(this.points);
    }

    public HeatModel() {
    }

    protected HeatModel(Parcel in) {
        this.heat_pump_to_air = in.readString();
        this.lpg_gas = in.readString();
        this.natural_gas = in.readString();
        this.Wood_burner = in.readString();
        this.eletric_heater = in.readString();
        this.open_fire = in.readString();
        this.heat_pump_hydrolic = in.readString();
        this.points = in.readFloat();
    }

    public static final Parcelable.Creator<HeatModel> CREATOR = new Parcelable.Creator<HeatModel>() {
        @Override
        public HeatModel createFromParcel(Parcel source) {
            return new HeatModel(source);
        }

        @Override
        public HeatModel[] newArray(int size) {
            return new HeatModel[size];
        }
    };
}
