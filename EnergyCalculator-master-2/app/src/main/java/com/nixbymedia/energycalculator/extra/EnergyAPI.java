package com.nixbymedia.energycalculator.extra;

public interface EnergyAPI {
    interface URLapi{
        String URL = "http://111.65.227.13/energycalapi/";
        //String URL = "http://192.168.1.6/cmsweb/energycalapi2/";
    }

    interface URLroute{
        String USERLOGIN = "login.php";
        String USERREGISTER = "register.php";
        String URLPROPERTYINSERT = "insertproperty.php";
        String URLTHERMALINSERT = "insertthermal.php";
        String URLHEATINGINSERT = "insertheatingsystem.php";
        String URLLIGHTINGINSERT = "insertdaylightlighting.php";
        String URLWATERUSEINSERT = "insertwateruse.php";
        String URLPROPERTYUPDATE = "updateproperty.php";
        String URLTHERMALUPDATE = "updatethermal.php";
        String URLHEATINGUPDATE = "updateheatingsystem.php";
        String URLLIGHTINGUPDATE = "updatedaylightlighting.php";
        String URLWATERUSEUPDATE = "updatewateruse.php";
        String URLPROPERTYSELECT = "selectproperty.php";
        String URLTHERMALSELECT = "selectthermal.php";
        String URLHEATINGSELECT = "selectheatingsystem.php";
        String URLLIGHTINGSELECT = "selectdaylightlighting.php";
        String URLWATERUSESELECT = "selectwateruse.php";
        String URLPROJECTLIST = "showpropertyproj.php";
    }
}
