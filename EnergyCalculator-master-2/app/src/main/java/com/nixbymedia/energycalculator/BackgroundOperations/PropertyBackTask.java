package com.nixbymedia.energycalculator.BackgroundOperations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.Login_Activity;
import com.nixbymedia.energycalculator.extra.EnergyAPI;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PropertyBackTask{
    Activity ctx;
    private int user_id;
    RequestQueue requestQueue;

    public PropertyBackTask(Context context) {
        this.ctx = (Activity) context;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    public void insert(String... params) {
        String certi_id = params[0];
        String dwell_type = params[1];
        String dwell_size = params[2];
        String room_no = params[3];
        String climate_location = params[4];
        String legal_desc = params[5];
        String title_cert = params[6];
        String address = params[7];
        String comment = params[8];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROPERTYINSERT;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("prop_id", certi_id)
                .add("dwell_type", dwell_type)
                .add("dwell_size", dwell_size)
                .add("room_no", room_no)
                .add("climate_location", climate_location)
                .add("legal_decs", legal_desc)
                .add("cert_title", title_cert)
                .add("address", address)
                .add("asstcomment", comment)
                .add("userid", Integer.toString(this.user_id));
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.i("Reponse", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void update(String... params) {
        String certi_id = params[0];
        String dwell_type = params[1];
        String dwell_size = params[2];
        String room_no = params[3];
        String climate_location = params[4];
        String legal_desc = params[5];
        String title_cert = params[6];
        String address = params[7];
        String comment = params[8];
        this.requestQueue = Volley.newRequestQueue(this.ctx);
        String api = EnergyAPI.URLapi.URL + EnergyAPI.URLroute.URLPROPERTYUPDATE;
        OkHttpClient okhttp = new OkHttpClient.Builder().connectTimeout(1500, TimeUnit.SECONDS)
                .writeTimeout(1000, TimeUnit.SECONDS)
                .readTimeout(1000, TimeUnit.SECONDS)
                .build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("prop_id", certi_id)
                .add("dwell_type", dwell_type)
                .add("dwell_size", dwell_size)
                .add("room_no", room_no)
                .add("climate_location", climate_location)
                .add("legal_desc", legal_desc)
                .add("cert_title", title_cert)
                .add("address", address)
                .add("asstcomment", comment)
                .add("userid", Integer.toString(this.user_id));
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder().url(api).post(formBody).header("Accept", "application/json")
                .header("Content-Type", "application/json").build();
        okhttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.w("failure Response", e.getMessage());
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String jsonData = response.body().string();
                        Log.e("Response", jsonData);
                        JSONObject jsonObject = new JSONObject(jsonData);
                        if (jsonObject.getBoolean("error")) {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(
                                    () -> Toast.makeText(ctx, "Error: " + message, Toast.LENGTH_SHORT).show()
                            );
                        } else {
                            String message = jsonObject.getString("message");
                            ctx.runOnUiThread(

                                    () -> {
                                        Toast.makeText(ctx, "Success: " + message, Toast.LENGTH_SHORT).show();
                                    }
                            );
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ctx, "Error: Something Wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


}
