package com.nixbymedia.energycalculator.water_user;

public class WaterUseModel {
    private float kitchen_tap_point = 0f, basin_tap_point = 0f, shower_point = 0f, worst_Wc_point = 0f;
    private float total_points;

    private String kitchen_tap = "", basin_tap = "", shower = "", wc_point = "";
    private String measuredInspect, docInspect, comment;

    private byte[] image_1, image_2, image_3;

    private int id;


    public byte[] getImage_1() {
        return image_1;
    }

    public void setImage_1(byte[] image_1) {
        this.image_1 = image_1;
    }

    public byte[] getImage_2() {
        return image_2;
    }

    public void setImage_2(byte[] image_2) {
        this.image_2 = image_2;
    }

    public byte[] getImage_3() {
        return image_3;
    }

    public void setImage_3(byte[] image_3) {
        this.image_3 = image_3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeasuredInspect() {
        return measuredInspect;
    }

    public void setMeasuredInspect(String measuredInspect) {
        this.measuredInspect = measuredInspect;
    }

    public float getWorst_Wc_point() {
        return worst_Wc_point;
    }

    public void setWorst_Wc_point(float worst_Wc_point) {
        this.worst_Wc_point = worst_Wc_point;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDocInspect() {
        return docInspect;
    }

    public void setDocInspect(String docInspect) {
        this.docInspect = docInspect;
    }

    public String getKitchen_tap() {
        return kitchen_tap;
    }

    public void setKitchen_tap(String kitchen_tap) {
        this.kitchen_tap = kitchen_tap;
    }

    public String getBasin_tap() {
        return basin_tap;
    }

    public void setBasin_tap(String basin_tap) {
        this.basin_tap = basin_tap;
    }


    public String getShower() {
        return shower;
    }

    public void setShower(String shower) {
        this.shower = shower;
    }

    public String getShowerData() {
        return kitchen_tap;
    }

    public String getWc_point() {
        return wc_point;
    }

    public void setWc_point(String wc_point) {
        this.wc_point = wc_point;
    }

    public String getWcPoints() {
        return wc_point;
    }

    public float getTotal_points() {
        return total_points;
    }

    public void setTotal_points(float total_points) {
        this.total_points = total_points;
    }

    private static WaterUseModel model = new WaterUseModel();

    public static WaterUseModel getInstance() {
        return model;
    }

    public float getKitchen_tap_point() {
        return kitchen_tap_point;
    }

    public void setKitchen_tap_point(float kitchen_tap_point) {
        this.kitchen_tap_point = kitchen_tap_point;
    }


    public float getBasin_tap_point() {
        return basin_tap_point;
    }

    public void setBasin_tap_point(float basin_tap_point) {
        this.basin_tap_point = basin_tap_point;
    }

    public float getShower_point() {
        return shower_point;
    }

    public void setShower_point(float shower_point) {
        this.shower_point = shower_point;
    }

    public float getWorst_Wc() {
        return worst_Wc_point;
    }

    public void setWorst_Wc(float worst_Wc) {
        this.worst_Wc_point = worst_Wc;
    }

    @Override
    public String toString() {
        return "WaterUseModel{" +
                "kitchen_tap=" + kitchen_tap_point +
                ", basin_tap=" + basin_tap_point +
                ", shower=" + shower_point +
                ", worst_Wc=" + worst_Wc_point +
                ", total_points=" + total_points +
                '}';
    }
}
