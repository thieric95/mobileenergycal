package com.nixbymedia.energycalculator.thermal_envelope;

public class ThermalEnvelope {
    private static ThermalEnvelope thermalEnvelope = new ThermalEnvelope();

    public static ThermalEnvelope getInstance() {
        return thermalEnvelope;
    }

    public ThermalEnvelope() {
    }

    int points;
    String index;
    private String comment;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
