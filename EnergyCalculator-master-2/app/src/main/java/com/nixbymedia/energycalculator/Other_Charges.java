package com.nixbymedia.energycalculator;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.nixbymedia.energycalculator.BackgroundOperations.OtherUseBackTask;
import com.nixbymedia.energycalculator.water_user.Water_Use;


public class Other_Charges extends Fragment {


    Spinner spinner1, spinner2;
    ImageButton back, forward, comment;
    TextView cmnt;
    FragmentManager fragmentManager;
    OtherUseBackTask otherUseBackTask;

    public Other_Charges() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_other_charges, container, false);

        spinner1 = view.findViewById(R.id.spinner_swimming_pool);
        spinner2 = view.findViewById(R.id.spinner_spa_pool);

        cmnt = view.findViewById(R.id.other_comment_text);

        back = view.findViewById(R.id.other_back);
        comment = view.findViewById(R.id.other_comment);
        forward = view.findViewById(R.id.other_forward);

        otherUseBackTask = new OtherUseBackTask(getContext());

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(getContext());
                View dialogView = li.inflate(R.layout.comment_dialog, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getContext());
                // set title
                alertDialogBuilder.setTitle("Assessors Comment");
                // set custom_dialog.xml to alertdialog builder
                alertDialogBuilder.setView(dialogView);

                Button btncomment = dialogView.findViewById(R.id.btnComment);
                final EditText text = dialogView.findViewById(R.id.text_comment);

                if(!cmnt.getText().toString().trim().equals(""))
                    text.setText(cmnt.getText().toString().trim());

                final AlertDialog alertDialog = alertDialogBuilder.create();
                btncomment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(text.getText().toString().trim().equals("")) {
                            text.setError("Please Enter Some Comment!");
                        }else{
                            cmnt.setText(text.getText().toString());
                            Toast.makeText(getContext(), "Commented : "+text.getText().toString(), Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }
                    }
                });

                alertDialog.show();
            }


        });
        otherUseBackTask.execute("remove");

        fragmentManager = getActivity().getSupportFragmentManager();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentManager.beginTransaction().replace(R.id.fragment_main, new Water_Use()).commit();
            }
        });
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherUseBackTask = new OtherUseBackTask(getContext());
                otherUseBackTask.execute("add","123",spinner1.getSelectedItem().toString(), spinner2.getSelectedItem().toString(),cmnt.getText().toString());
                startActivity(new Intent(getActivity(), Activity_Result.class));
                getActivity().finish();
            }
        });


        return  view;
    }

}
