package com.nixbymedia.energycalculator.Calculations;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.nixbymedia.energycalculator.DatabaseHelperClass;
import com.nixbymedia.energycalculator.SchemaHelperClass;

public class WaterPointCalc {

    double sh, kit, bas, toi;
    Activity ctx;
    double total;

    public WaterPointCalc(Context context) {
        this.ctx = (Activity) context;
    }

    public void getData() {
//        DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(ctx);
//        SQLiteDatabase db = databaseHelperClass.getReadableDatabase();
//        Cursor cursor = databaseHelperClass.readWater(db);
//        String shower, kitchen_tap, basin_tap, toilet;
//        while (cursor.moveToNext()) {
//            shower = cursor.getString(cursor.getColumnIndex(SchemaHelperClass.WaterUse.SHOWER));
//            kitchen_tap = cursor.getString(cursor.getColumnIndex(SchemaHelperClass.WaterUse.KITCHEN_TAP));
//            basin_tap = cursor.getString(cursor.getColumnIndex(SchemaHelperClass.WaterUse.BASIN_TAP));
//            toilet = cursor.getString(cursor.getColumnIndex(SchemaHelperClass.WaterUse.WC_STARS));
//            sh = shower == null || shower.equals("") ? 0 : Double.parseDouble(shower);
//            kit = Double.parseDouble(kitchen_tap);
//            bas = Double.parseDouble(basin_tap);
//            toi = Double.parseDouble(toilet);
//            total = Showerpoints(sh) + Kitpoints(kit) + Basinpoints(bas) + Toiletpoints(toi);
//            Toast.makeText(ctx, "" + Showerpoints(sh) + "," + Kitpoints(kit) + "," + Basinpoints(bas) + "," + Toiletpoints(toi), Toast.LENGTH_LONG).show();
//        }
    }

    public double Showerpoints(double points) {

        if (points >= 9)
            return 0.0;
        else if (points < 9 && points > 7.5)
            return 3.5;
        else
            return 4.0;
    }

    public double Kitpoints(double points) {
        if (points <= 5)
            return 1.5;
        else if (points <= 6)
            return 1.0;
        else if (points <= 7.5)
            return 0.5;
        else
            return 0;
    }

    public double Basinpoints(double points) {
        if (points <= 5)
            return 1.5;
        else if (points <= 6)
            return 1.0;
        else if (points <= 7.5)
            return 0.5;
        else
            return 0;
    }

    public double Toiletpoints(double points) {

        if (points <= 1)
            return 0;
        else if (points <= 3)
            return 1;
        else if (points <= 4)
            return 2;
        else if (points >= 4.5)
            return 3;
        else
            return 0;
    }

    public double getTotalPoints() {
        Toast.makeText(ctx, "" + total, Toast.LENGTH_LONG).show();
        return total;
    }


}
