package com.nixbymedia.energycalculator;

import java.util.HashMap;

public class FileModel {

    private static FileModel fileModel = new FileModel();
    public HashMap<String, String> fileMap = new HashMap<>();
    public HashMap<String, byte[]> fileArrayByte = new HashMap<>();
    public static FileModel getFileModel() {
        return fileModel;
    }



    public void clear_filearraybyte(){
        this.fileArrayByte.clear();
    }
    public void clear_fileMap(){
        this.fileMap.clear();
    }
}
