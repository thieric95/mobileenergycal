package com.nixbymedia.energycalculator.report;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.nixbymedia.energycalculator.Activity_Result;
import com.nixbymedia.energycalculator.BaseActivity;
import com.nixbymedia.energycalculator.FileModel;
import com.nixbymedia.energycalculator.R;
import com.nixbymedia.energycalculator.ReportAdapter;
import com.nixbymedia.energycalculator.certificate.CertificateActivity;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.heatingsystem.WaterHeating;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.nixbymedia.energycalculator.extra.Utils.getFolderFile;

public class ReportActivity extends BaseActivity {

    private static final int PICK_PDF_FILE = 2;
    protected TextView tvTitle;
    protected TextView tvSubTitle;
    protected TextView tvTitleProperty;
    protected View borderProperty;
    protected Guideline left;
    protected Guideline right;
    protected Guideline propertyGuideline;
    protected TextView tvLegalDescription;
    protected TextView tvTitleCertificate;
    protected LinearLayout linearProperty1;
    protected TextView tvTitleAssessment;
    protected TextView tvAssesscorOrganization;
    protected TextView tvOrganisationWebsite;
    protected TextView tvContactEmail;
    protected LinearLayout linearAssessment;
    protected TextView tvCertificateNumber;
    protected TextView tvCertificateDate;
    protected TextView tvAssessorName;
    protected TextView tvRegistration;
    protected LinearLayout linearAssessment1;
    protected TextView tvClimateZone;
    protected LinearLayout llClimate;
    protected TextView tvThermalEnvelope;
    protected TextView tvPerfomanceBuilding;
    protected TextView bpi;
    protected TextView spIndex;
    protected LinearLayout llThermal;
    protected LinearLayout llThermalEnvelope;
    protected TextView tvHeatingInstruction;
    protected TextView tvSpacing;
    protected Guideline center;
    protected RecyclerView recycleView;
    protected TextView tvHeatToAir;
    protected TextView tvHeatAirKw;
    protected TextView tvHeatToHydrolic;
    protected TextView tvHeatHydrolicKw;
    protected TextView tvLpgGas;
    protected TextView tvLpgGasKw;
    protected TextView tvNaturalGas;
    protected TextView tvNaturalGasKw;
    protected TextView tvWoodPallet;
    protected TextView tvWoodPalletKw;
    protected TextView tvElectricHeater;
    protected TextView tvElectricHeaterKw;
    protected LinearLayout llHeatingSystem;
    protected LinearLayout llDaylightLighting;
    protected TextView tvDaylightInstruction;
    protected RecyclerView recycleviewDaylighting;
    protected TextView tvLedLights;
    protected TextView tvFlurosentLights;
    protected TextView tvOr;
    protected TextView tvIndoorLightingWattage;
    protected TextView tvOutdoorWattage;
    protected TextView tvWattageWithSensorFitted;
    protected LinearLayout llDaylightingData;
    protected LinearLayout llInternalWater;
    protected TextView tvWaterInstruction;
    protected RecyclerView recycleviewInternalWater;
    protected TextView tvKitchenTap;
    protected TextView tvBasinTap;
    protected TextView tvWorstShower;
    //  protected TextView tvHeatToAirWater;
    protected LinearLayout llInternalWaterData;
    protected TextView tvAddress;
    protected TextView tvDwellingType;
    protected TextView tvDwellingSize;
    protected TextView tvBedrooms;
    protected TextView tvHalogenLight;
    protected TextView tvWorstWc;
    protected LinearLayout llTitle;
    protected TextView tvComments;
    protected LinearLayout llHeatingComment;
    protected ConstraintLayout page1;
    protected LinearLayout llWaterHeating;
    protected LinearLayout llLightingComment;
    protected ConstraintLayout page2;
    protected TextView tvCommentLighting;
    protected TextView tvCommentWater;
    protected LinearLayout llWaterComment;
    protected TextView tvThermalComments;
    protected LinearLayout llThermalComment;
    private ReportAdapter heating, day_lighting, internal_water, water_heating;
    private ScrollView root;
    private TextView tv_water_heating;
    private RecyclerView recycleview_water_heating;
    private ImageButton btn_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        initView();
        setData();
        setComments();
        setHeatingImages();
        setLightingImages();
        setInternalWaterImages();
        setWaterHeatingImages();
        Picasso.get().setLoggingEnabled(true);

    }

    //here we are setting the data
    private void setData() {
        try {
            String address = PropertyModel.getInstance().getAddress();

            tvAddress.setText(address);
            tvLegalDescription.setText(PropertyModel.getInstance().getLegal_description());
            tvTitleCertificate.setText(PropertyModel.getInstance().getTitle_certificate());
            tvDwellingType.setText(PropertyModel.getInstance().getDwellingType());
            tvDwellingSize.setText(PropertyModel.getInstance().getDwellingSize() + " m 2");
            tvBedrooms.setText(PropertyModel.getInstance().getNoOfBedRooms());
            tvCertificateDate.setText(getDate());
            tvCertificateNumber.setText(PropertyModel.getInstance().getCertificate_number());
            tvClimateZone.setText(PropertyModel.getInstance().getClimate_location());

            spIndex.setText(ThermalEnvelope.getInstance().getIndex());
            //heating
            tvHeatToAir.setText(HeatModel.getInstance().getHeat_pump_to_air());
            tvHeatToHydrolic.setText(HeatModel.getInstance().getHeat_pump_hydrolic());
            tvLpgGas.setText(HeatModel.getInstance().getLpg_gas());
            tvNaturalGas.setText(HeatModel.getInstance().getNatural_gas());
            tvWoodPallet.setText(HeatModel.getInstance().getWood_burner());
            tvElectricHeater.setText(HeatModel.getInstance().getEletric_heater());
            tvNaturalGas.setText(HeatModel.getInstance().getNatural_gas());

            //Lighting.
            tvLedLights.setText(LightingModel.getInstance().getLed_light());
            tvFlurosentLights.setText(LightingModel.getInstance().getCfl_light());
            tvHalogenLight.setText(LightingModel.getInstance().getHalogen_light());
            tvIndoorLightingWattage.setText(String.valueOf(LightingModel.getInstance().getTotal_indoor_wattage()));
            tvOutdoorWattage.setText(String.valueOf(LightingModel.getInstance().getTotal_outdoor_wattage()));
            tvWattageWithSensorFitted.setText(String.valueOf(LightingModel.getInstance().getSensor_wattage()));

            //Internal Water use
            tvKitchenTap.setText(String.valueOf(WaterUseModel.getInstance().getKitchen_tap()));
            tvBasinTap.setText(String.valueOf(WaterUseModel.getInstance().getBasin_tap()));
            tvWorstShower.setText(String.valueOf(WaterUseModel.getInstance().getShower()));
            tvWorstWc.setText(String.valueOf(WaterUseModel.getInstance().getWc_point()));

            //water heating
            tv_water_heating.setText(WaterHeating.getInstance().getWater_heating());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initView() {
        root = findViewById(R.id.root);
        tv_water_heating = findViewById(R.id.tv_water_heating);
        recycleview_water_heating = findViewById(R.id.recycle_view_water_heating);
        heating = new ReportAdapter(this);
        water_heating = new ReportAdapter(this);
        day_lighting = new ReportAdapter(this);
        internal_water = new ReportAdapter(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvSubTitle = (TextView) findViewById(R.id.tv_sub_title);
        tvTitleProperty = (TextView) findViewById(R.id.tv_title_property);
        borderProperty = (View) findViewById(R.id.border_property);
        left = (Guideline) findViewById(R.id.left);
        right = (Guideline) findViewById(R.id.right);
        //   propertyGuideline = (Guideline) findViewById(R.id.property_guideline);
        tvLegalDescription = (TextView) findViewById(R.id.tv_legal_description);
        tvTitleCertificate = (TextView) findViewById(R.id.tv_title_certificate);
        linearProperty1 = (LinearLayout) findViewById(R.id.linear_property_1);
        tvTitleAssessment = (TextView) findViewById(R.id.tv_title_assessment);
        tvAssesscorOrganization = (TextView) findViewById(R.id.tv_assesscor_organization);
        tvOrganisationWebsite = (TextView) findViewById(R.id.tv_organisation_website);
        tvContactEmail = (TextView) findViewById(R.id.tv_contact_email);
        linearAssessment = (LinearLayout) findViewById(R.id.linear_assessment);
        tvCertificateNumber = (TextView) findViewById(R.id.tv_certificate_number);
        tvCertificateDate = (TextView) findViewById(R.id.tv_certificate_date);
        tvAssessorName = (TextView) findViewById(R.id.tv_assessor_name);
        tvRegistration = (TextView) findViewById(R.id.tv_registration);
        linearAssessment1 = (LinearLayout) findViewById(R.id.linear_assessment_1);
        tvClimateZone = (TextView) findViewById(R.id.tv_climate_zone);
        llClimate = (LinearLayout) findViewById(R.id.ll_climate);
        tvThermalEnvelope = (TextView) findViewById(R.id.tv_thermal_envelope);
        tvPerfomanceBuilding = (TextView) findViewById(R.id.tv_perfomance_building);
        bpi = (TextView) findViewById(R.id.bpi);
        spIndex = (TextView) findViewById(R.id.sp_index);
        llThermal = (LinearLayout) findViewById(R.id.ll_thermal);
        llThermalEnvelope = (LinearLayout) findViewById(R.id.ll_thermal_envelope);
        tvHeatingInstruction = (TextView) findViewById(R.id.tv_heating_instruction);
        tvSpacing = (TextView) findViewById(R.id.tv_spacing);
        center = (Guideline) findViewById(R.id.center);
        recycleView = (RecyclerView) findViewById(R.id.recycle_view);
        tvHeatToAir = (TextView) findViewById(R.id.tv_heat_to_air);
        //  tvHeatAirKw = (TextView) findViewById(R.id.tv_heat_air_kw);
        tvHeatToHydrolic = (TextView) findViewById(R.id.tv_heat_to_hydrolic);
        //  tvHeatHydrolicKw = (TextView) findViewById(R.id.tv_heat_hydrolic_kw);
        tvLpgGas = (TextView) findViewById(R.id.tv_lpg_gas);
        //  tvLpgGasKw = (TextView) findViewById(R.id.tv_lpg_gas_kw);
        tvNaturalGas = (TextView) findViewById(R.id.tv_natural_gas);
        //   tvNaturalGasKw = (TextView) findViewById(R.id.tv_natural_gas_kw);
        tvWoodPallet = (TextView) findViewById(R.id.tv_wood_pallet);
        //  tvWoodPalletKw = (TextView) findViewById(R.id.tv_wood_pallet_kw);
        tvElectricHeater = (TextView) findViewById(R.id.tv_electric_heater);
        //  tvElectricHeaterKw = (TextView) findViewById(R.id.tv_electric_heater_kw);
        llHeatingSystem = (LinearLayout) findViewById(R.id.ll_heating_system);
        llDaylightLighting = (LinearLayout) findViewById(R.id.ll_daylight_lighting);
        tvDaylightInstruction = (TextView) findViewById(R.id.tv_daylight_instruction);
        recycleviewDaylighting = (RecyclerView) findViewById(R.id.recycleview_daylighting);
        tvLedLights = (TextView) findViewById(R.id.tv_led_lights);
        tvFlurosentLights = (TextView) findViewById(R.id.tv_flurosent_lights);
        tvLpgGas = (TextView) findViewById(R.id.tv_lpg_gas);
        tvOr = (TextView) findViewById(R.id.tv_or);
        tvIndoorLightingWattage = (TextView) findViewById(R.id.tv_indoor_lighting_wattage);
        tvOutdoorWattage = (TextView) findViewById(R.id.tv_outdoor_wattage);
        tvWattageWithSensorFitted = (TextView) findViewById(R.id.tv_wattage_with_sensor_fitted);
        llDaylightingData = (LinearLayout) findViewById(R.id.ll_daylighting_data);
        llInternalWater = (LinearLayout) findViewById(R.id.ll_internal_water);
        tvWaterInstruction = (TextView) findViewById(R.id.tv_water_instruction);
        recycleviewInternalWater = (RecyclerView) findViewById(R.id.recycleview_internal_water);
        tvKitchenTap = (TextView) findViewById(R.id.tv_kitchen_tap);
        tvBasinTap = (TextView) findViewById(R.id.tv_basin_tap);
        tvWorstShower = (TextView) findViewById(R.id.tv_worst_shower);
//        tvHeatToAirWater = (TextView) findViewById(R.id.tv_heat_to_air_water);
        llInternalWaterData = (LinearLayout) findViewById(R.id.ll_internal_water_data);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvDwellingType = (TextView) findViewById(R.id.tv_dwelling_type);
        tvDwellingSize = (TextView) findViewById(R.id.tv_dwelling_size);
        tvBedrooms = (TextView) findViewById(R.id.tv_bedrooms);
        tvHalogenLight = (TextView) findViewById(R.id.tv_halogen_light);
        tvWorstWc = (TextView) findViewById(R.id.tv_worst_wc);
        setAdapters();
        llTitle = (LinearLayout) findViewById(R.id.ll_title);
        tvComments = (TextView) findViewById(R.id.tv_comments);
        llHeatingComment = (LinearLayout) findViewById(R.id.ll_heating_comment);
        page1 = (ConstraintLayout) findViewById(R.id.page1);
        llWaterHeating = (LinearLayout) findViewById(R.id.ll_water_heating);
        llLightingComment = (LinearLayout) findViewById(R.id.ll_lighting_comment);
        page2 = (ConstraintLayout) findViewById(R.id.page2);
        tvCommentLighting = (TextView) findViewById(R.id.tv_comment_lighting);
        tvCommentWater = (TextView) findViewById(R.id.tv_comment_water);
        llWaterComment = (LinearLayout) findViewById(R.id.ll_water_comment);
        tvThermalComments = (TextView) findViewById(R.id.tv_thermal_comments);
        llThermalComment = (LinearLayout) findViewById(R.id.ll_thermal_comment);
    }

    private void setWaterHeatingImages() {
        try {
            String solarPath = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Solar);

            if (solarPath != null && !solarPath.isEmpty()) {
                water_heating.getFilePath().add(solarPath);
                water_heating.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLightingImages() {
        try {
            String led_lights = FileModel.getFileModel().fileMap.get(Extra.FileConstants.LED_Lights);
            String Fluorescent_Lights = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Fluorescent_Lights);
            String Halogen_lights = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Halogen_lights);
            String Indoor_Light = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Indoor_Light);
            String Outdoor_Light = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Outdoor_Light);
            String Wattage_Sensor = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Wattage_Sensor);

            if (led_lights != null && !led_lights.isEmpty()){
                day_lighting.getFilePath().add(led_lights);
            }


            if (Fluorescent_Lights != null && !Fluorescent_Lights.isEmpty()){
                day_lighting.getFilePath().add(Fluorescent_Lights);
            }


            if (Halogen_lights != null && !Halogen_lights.isEmpty()){
                heating.getFilePath().add(Halogen_lights);
            }


            if (Indoor_Light != null && !Indoor_Light.isEmpty()){
                day_lighting.getFilePath().add(Indoor_Light);
            }


            if (Outdoor_Light != null && !Outdoor_Light.isEmpty()){
                day_lighting.getFilePath().add(Outdoor_Light);
            }


            if (Wattage_Sensor != null && !Wattage_Sensor.isEmpty()){
                day_lighting.getFilePath().add(Wattage_Sensor);
            }

            day_lighting.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setInternalWaterImages() {
        try {
            String Kitchen_Tap = FileModel.getFileModel().fileMap.get(Extra.FileConstants.kitchen_tap);
            String basin_tap = FileModel.getFileModel().fileMap.get(Extra.FileConstants.basin_tap);
            String Worst_shower = FileModel.getFileModel().fileMap.get(Extra.FileConstants.worst_shower);

            if (Kitchen_Tap != null && !Kitchen_Tap.isEmpty()){
                internal_water.getFilePath().add(Kitchen_Tap);
            }


            if (basin_tap != null && !basin_tap.isEmpty()){
                internal_water.getFilePath().add(basin_tap);
            }


            if (Worst_shower != null && !Worst_shower.isEmpty()){
                internal_water.getFilePath().add(Worst_shower);
            }

            internal_water.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgressDialog("Please wait...");
        new Handler().postDelayed(() -> saveToPdf("Reports"), 6000);
    }

    public void openPDF(final File path) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW);
                    if (Build.VERSION.SDK_INT >= 26) {

                        Uri uri = FileProvider.getUriForFile(ReportActivity.this, ReportActivity.this.getApplicationContext().getPackageName() + ".provider", path);
                        myIntent.setDataAndType(uri, "application/pdf");
                        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }else {
                        Uri uri = Uri.fromFile(path);
                        myIntent.setDataAndType(uri, "application/pdf");
                    }

                    Intent j = Intent.createChooser(myIntent, "Choose an application to open with:");
                    startActivity(j);
                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Intent intent1 = new Intent(ReportActivity.this, Activity_Result.class);
            intent1.putExtra("option", "add");
            startActivity(intent1);
        }
    }

    private int getDeviceDensity() {
        try {
            DisplayMetrics dm = getResources().getDisplayMetrics();
            return dm.densityDpi;
        } catch (Exception e) {
            e.printStackTrace();
            return 150;
        }
    }

    public void saveToPdf(final String subpath) {
        new Thread() {
            public void run() {
                try {
                    final File pdfFile = new File(getFolderFile(ReportActivity.this, subpath) + "/" + "Report_" + PropertyModel.getInstance().getTitle_certificate()+ System.currentTimeMillis() + ".pdf");
                    final PdfDocument document = new PdfDocument();

                    // final Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath());
                    // do a looping here.
                    int margin = 50;
                    int pageWidth = (int) (getDeviceDensity() * A4Horizantal);
                    int pageHeight = (int) (getDeviceDensity() * A4Vertical);
                    // final Bitmap bitmap = BitmapFactory.decodeFile(photo.getAbsolutePath());
                    // do a looping here.
                    Rect rect = new Rect();
                    rect.set(margin, margin, pageWidth - margin, pageHeight - margin);
                    PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder((int) (getDeviceDensity() * A4Horizantal), (int) (getDeviceDensity() * A4Vertical), 1)
                            .setContentRect(rect)
                            .create();

                    final PdfDocument.PageInfo page2Info = new PdfDocument.PageInfo.Builder((int) (getDeviceDensity() * A4Horizantal), (int) (getDeviceDensity() * A4Vertical), 2)
                            .setContentRect(rect)
                            .create();

                    final PdfDocument.Page page = document.startPage(pageInfo);
                    final int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                    final int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight(), View.MeasureSpec.EXACTLY);


                    runOnUiThread(() -> {
                        try {

                            page1.measure(measureWidth, measuredHeight);
                            page1.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                            page1.draw(page.getCanvas());
                            document.finishPage(page);


                            final PdfDocument.Page page_2 = document.startPage(page2Info);


                            page2.measure(measureWidth, measuredHeight);
                            page2.layout(0, 0, page_2.getCanvas().getWidth(), page_2.getCanvas().getHeight());
                            page2.draw(page_2.getCanvas());
                            document.finishPage(page_2);
                            document.writeTo(new FileOutputStream(pdfFile));
                            document.close();
                            hideProgressDialog();
                            if(pdfFile.exists()){
                                openPDF(pdfFile);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });

                    // finish the page

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    private void setComments() {
        try {
            String thermal = getComment(Extra.Tag.thermal);
            String heating = getComment(Extra.Tag.HeatingSystem);
            String lighting = getComment(Extra.Tag.Daylight);
            String waterUse = getComment(Extra.Tag.WaterUse);

            if (thermal!=null && !thermal.isEmpty()) {
                tvThermalComments.setText(thermal);
            }

            if (heating!=null && !heating.isEmpty()) {
                tvComments.setText(heating);
            }

            if (lighting!=null && !lighting.isEmpty()) {
                tvCommentLighting.setText(lighting);
            }

            if (waterUse!=null && !waterUse.isEmpty()) {
                tvCommentWater.setText(waterUse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHeatingImages() {
        try {
            String heat_air = FileModel.getFileModel().fileMap.get(Extra.FileConstants.heatair);
            String heat_hydronic = FileModel.getFileModel().fileMap.get(Extra.FileConstants.heathydronic);
            String gas = FileModel.getFileModel().fileMap.get(Extra.FileConstants.gas);
            String natural = FileModel.getFileModel().fileMap.get(Extra.FileConstants.naturalgas);
            String wood = FileModel.getFileModel().fileMap.get(Extra.FileConstants.Wood);
            String electric_heater = FileModel.getFileModel().fileMap.get(Extra.FileConstants.ElectricHeater);

            if (heat_air != null && !heat_air.isEmpty()){
                heating.getFilePath().add(heat_air);
            }


            if (heat_hydronic != null && !heat_hydronic.isEmpty()){
                heating.getFilePath().add(heat_hydronic);
            }


            if (heat_hydronic != null && !heat_hydronic.isEmpty()){
                heating.getFilePath().add(heat_hydronic);
            }


            if (gas != null && !gas.isEmpty()){
                heating.getFilePath().add(gas);
            }


            if (natural != null && !natural.isEmpty()){
                heating.getFilePath().add(natural);
            }


            if (wood != null && !wood.isEmpty()){
                heating.getFilePath().add(wood);
            }


            if (electric_heater != null && !electric_heater.isEmpty()){
                heating.getFilePath().add(electric_heater);
            }

            heating.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapters() {
        try {
            recycleView.setLayoutManager(new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false));
            recycleView.setAdapter(heating);
            recycleviewDaylighting.setLayoutManager(new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false));
            recycleviewDaylighting.setAdapter(day_lighting);
            recycleviewInternalWater.setLayoutManager(new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false));
            recycleviewInternalWater.setAdapter(internal_water);
            recycleview_water_heating.setLayoutManager(new GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false));
            recycleview_water_heating.setAdapter(water_heating);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
