package com.nixbymedia.energycalculator;

public interface EditingSection {

    class Iseditinggroup{
        public static boolean ispropertyedited = false;
        public static boolean isthermaledited = false;
        public static boolean isheatingedited = false;
        public static boolean isdaylightingedited = false;
        public static boolean iswateruseedited = false;
    }

    class Isaddinggrouo{
        public static boolean ispropertyadded = false;
        public static boolean isthermaladded = false;
        public static boolean isheatingadded = false;
        public static boolean isdaylightingadded = false;
        public static boolean iswateruseadded = false;
    }
}
