package com.nixbymedia.energycalculator.extra;

import android.util.Log;

public class Debugger {
    public static boolean isDebug = true;
    public static void debugE(String title, String text) {
        Log.e(title, text);
    }
}
