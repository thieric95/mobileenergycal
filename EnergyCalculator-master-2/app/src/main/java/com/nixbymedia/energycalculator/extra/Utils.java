package com.nixbymedia.energycalculator.extra;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.nixbymedia.energycalculator.R;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static boolean emailValidator(String mailAddress) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mailAddress);
        return matcher.matches();
    }

    public static boolean iscontactvalid(String contact) {
        Pattern pattern;
        Matcher matcher;
        final String MobilePattern = "[0-9]{10}";
        pattern = Pattern.compile(MobilePattern);
        matcher = pattern.matcher(contact);
        return matcher.matches();
    }

    public static File getFolderFile(Context c, String sub_path) {
        String folderName = "";
        //Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
//        if (Environment.isExternalStorageRemovable()) {
//            if (Build.VERSION.SDK_INT >= 26) {
//                folderName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + "EnergyCalculator" + "/" + sub_path;
//            } else {
//                folderName = Environment.getExternalStorageDirectory() + "/EnergyCalculator/" + sub_path;
//            }
//
//        }
//        if (Environment.isExternalStorageEmulated()) {
//            if (Build.VERSION.SDK_INT >= 26) {
//                folderName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + "EnergyCalculator" + "/" + sub_path;
//            } else {
//                folderName = Environment.getExternalStorageDirectory() + "/EnergyCalculator/" + sub_path;
//            }
//        } else {
//            folderName = c.getCacheDir() + "/" + "EnergyCalculator" + "/" + sub_path;
//        }

        if (Environment.isExternalStorageEmulated()) {
            if (Build.VERSION.SDK_INT >= 29) {
                folderName = c.getExternalCacheDir().getAbsolutePath()+ "/" + "EnergyCalculator" + "/" + sub_path;
            } else {
                folderName = c.getExternalCacheDir().getAbsolutePath() + "/EnergyCalculator/" + sub_path;
            }
        } else {
            folderName = c.getCacheDir() + "/" + "EnergyCalculator" + "/" + sub_path;
        }
        File folder = new File(folderName);
        try {
            if (!folder.exists()) {
                folder.mkdirs();
                //Log.d("isCreated", b + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folder;
    }


    public static boolean isValidPassword(String pass) {
        return pass.length() < 6;
    }

    public static boolean isValidMobileNumber(String pass) {
        return pass != null && pass.length() == 10;
    }

    public static boolean isValidPinNumber(String pass) {
        return pass != null && pass.length() == 6;
    }

    public static boolean isEmpty(EditText editText) {

        return editText.getText().toString().trim().length() == 0;
    }

    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String isTagExists(JSONObject jsonObj, String tagName) {
        String value = "";
        try {
            if (jsonObj.has(tagName)) {
                value = jsonObj.getString(tagName).trim();
                if (value.equals("null")) {
                    value = "";
                }
            } else {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    public static void showMessageOKCancel(Context c, String message, DialogInterface.OnClickListener onClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(c);
            builder.setMessage(message);
            builder.setPositiveButton("OK", onClickListener);
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    dialog.dismiss();
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMessageOK(Context c, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(c);
            builder.setTitle(c.getResources().getString(R.string.app_name));
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showToast(Context context, String message) {
        try {
            if (message != null)
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
