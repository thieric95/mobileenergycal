package com.nixbymedia.energycalculator;

import java.util.Date;
import java.util.UUID;

public class UUIDGenerate {

    public UUIDGenerate() {
    }

    public String randomUUID(){
        UUID uuid = UUID.randomUUID();
        long currenttime = new Date().getTime();
        return uuid +"-"+ currenttime;
    }
}
