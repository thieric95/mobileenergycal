package com.nixbymedia.energycalculator;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.nixbymedia.energycalculator.BackgroundOperations.DayLightBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.HeatingBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.PropertyBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.ThermalBackTask;
import com.nixbymedia.energycalculator.BackgroundOperations.WaterUseBackTask;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.HeatModel;
import com.nixbymedia.energycalculator.heatingsystem.WaterHeating;
import com.nixbymedia.energycalculator.lighting.LightingModel;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.ThermalEnvelope;
import com.nixbymedia.energycalculator.water_user.WaterUseModel;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

import static java.lang.Thread.sleep;

public class Splash_Screen_Process extends AppCompatActivity {
    ProgressBar progressBar;
    SessionManagement session;
    private int userid;
    private String pro_id;
    DatabaseHelperClass dbhelper;
    private String option;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen__process);
        progressBar = findViewById(R.id.progress_splash);

        FadingCircle wave = new FadingCircle();
        progressBar.setIndeterminateDrawable(wave);
        this.dbhelper = new DatabaseHelperClass(this);
        this.session = new SessionManagement(getApplicationContext());
        if (this.session.checkforlogin()) {
            finish();
        }
        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
        Intent intent = getIntent();
        if (intent != null) {
            this.option = intent.getStringExtra("option");
            if (this.option.equals("adding")) {

                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    add_propertydetail();
                }, 1500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    add_thermal_envelope();
                }, 2500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    add_heatingsystem();
                }, 3500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    add_daylightlightening();
                }, 4500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    add_wateruse();
                }, 5500);
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    //Do something here
                    Intent intent1 = new Intent(Splash_Screen_Process.this, Activity_Result.class);
                    intent1.putExtra("option", "add");
                    startActivity(intent1);
                }, 7400);
            }

        }


    }

    private void add_propertydetail() {
        PropertyBackTask propertytask = new PropertyBackTask(Splash_Screen_Process.this);
        propertytask.setUser_id(this.userid);

        PropertyModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.property));
        propertytask.insert(PropertyModel.getInstance().getCertificate_number(), PropertyModel.getInstance().getDwellingType(),
                PropertyModel.getInstance().getDwellingSize(), PropertyModel.getInstance().getNoOfBedRooms(), PropertyModel.getInstance().getClimate_location(),
                PropertyModel.getInstance().getLegal_description(), PropertyModel.getInstance().getTitle_certificate(), PropertyModel.getInstance().getAddress(),
                PropertyModel.getInstance().getComment());
    }
    
    private void add_wateruse() {
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.kitchen_tap)) {
            WaterUseModel.getInstance().setImage_1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.kitchen_tap));
        } else {
            WaterUseModel.getInstance().setImage_1(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.basin_tap)) {
            WaterUseModel.getInstance().setImage_2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.basin_tap));
        } else {
            WaterUseModel.getInstance().setImage_2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.worst_shower)) {
            WaterUseModel.getInstance().setImage_3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.worst_shower));
        } else {
            WaterUseModel.getInstance().setImage_3(this.convertImageToByte());
        }
//        System.out.println("Image 1: " + Integer.toString(WaterUseModel.getInstance().getImage_1().length));
//        System.out.println("Image 2: " + Integer.toString(WaterUseModel.getInstance().getImage_2().length));
//        System.out.println("Image 3: " + Integer.toString(WaterUseModel.getInstance().getImage_3().length));
        WaterUseBackTask waterusetask = new WaterUseBackTask(Splash_Screen_Process.this, WaterUseModel.getInstance().getImage_1(),
                WaterUseModel.getInstance().getImage_2(), WaterUseModel.getInstance().getImage_3());
        waterusetask.setUser_id(this.userid);
        waterusetask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        WaterUseModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.WaterUse));

        waterusetask.insert(WaterUseModel.getInstance().getKitchen_tap(),
                Float.toString(WaterUseModel.getInstance().getKitchen_tap_point()),
                WaterUseModel.getInstance().getBasin_tap(),
                Float.toString(WaterUseModel.getInstance().getBasin_tap_point()),
                WaterUseModel.getInstance().getShower(),
                Float.toString(WaterUseModel.getInstance().getShower_point()),
                WaterUseModel.getInstance().getMeasuredInspect(),
                WaterUseModel.getInstance().getWc_point(),
                Float.toString(WaterUseModel.getInstance().getWorst_Wc_point()),
                WaterUseModel.getInstance().getDocInspect(),
                Float.toString(WaterUseModel.getInstance().getTotal_points())
                , WaterUseModel.getInstance().getComment());
    }


    private void add_heatingsystem() {
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.heatair)) {
            HeatModel.getInstance().setImage1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heatair));
        } else {
            HeatModel.getInstance().setImage1(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.heathydronic)) {
            HeatModel.getInstance().setImage2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.heathydronic));
        } else {
            HeatModel.getInstance().setImage2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.gas)) {
            HeatModel.getInstance().setImage3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.gas));
        } else {
            HeatModel.getInstance().setImage3(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.naturalgas)) {
            HeatModel.getInstance().setImage4(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.naturalgas));
        } else {
            HeatModel.getInstance().setImage4(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Wood)) {
            HeatModel.getInstance().setImage5(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wood));
        } else {
            HeatModel.getInstance().setImage5(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.ElectricHeater)) {
            HeatModel.getInstance().setImage6(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.ElectricHeater));
        } else {
            HeatModel.getInstance().setImage6(this.convertImageToByte());
        }

        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Solar)) {
            HeatModel.getInstance().setImage7(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Solar));
        } else {
            HeatModel.getInstance().setImage7(this.convertImageToByte());
        }

//        System.out.println("Image 1: " + Integer.toString(HeatModel.getInstance().getImage1().length));
//        System.out.println("Image 2: " + Integer.toString(HeatModel.getInstance().getImage2().length));
//        System.out.println("Image 3: " + Integer.toString(HeatModel.getInstance().getImage3().length));
//        System.out.println("Image 4: " + Integer.toString(HeatModel.getInstance().getImage4().length));
//        System.out.println("Image 5: " + Integer.toString(HeatModel.getInstance().getImage5().length));
//        System.out.println("Image 6: " + Integer.toString(HeatModel.getInstance().getImage6().length));
//        System.out.println("Image 7: " + Integer.toString(HeatModel.getInstance().getImage7().length));
        HeatingBackTask heatingtask = new HeatingBackTask(Splash_Screen_Process.this, HeatModel.getInstance().getImage1(), HeatModel.getInstance().getImage2(),
                HeatModel.getInstance().getImage3(), HeatModel.getInstance().getImage4(), HeatModel.getInstance().getImage5(),
                HeatModel.getInstance().getImage6(), HeatModel.getInstance().getImage7());
        heatingtask.setUser_id(this.userid);
        heatingtask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        HeatModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.HeatingSystem));

        heatingtask.insert(HeatModel.getInstance().getHeat_pump_to_air(),
                HeatModel.getInstance().getHeat_pump_to_air_kw()
                , HeatModel.getInstance().getHeat_pump_hydrolic(),
                HeatModel.getInstance().getHeat_pump_hydrolic_kw()
                , HeatModel.getInstance().getLpg_gas(),
                HeatModel.getInstance().getLpg_gas_kw(),
                HeatModel.getInstance().getNatural_gas(),
                HeatModel.getInstance().getNatural_gas_kw(),
                HeatModel.getInstance().getWood_burner(),
                HeatModel.getInstance().getWood_burner_kw(),
                HeatModel.getInstance().getEletric_heater(),
                HeatModel.getInstance().getEletric_heater_kw(),
                WaterHeating.getInstance().getWater_heating(),
                Float.toString(WaterHeating.getInstance().getPoints()),
                Float.toString(HeatModel.getInstance().getPoints()),
                HeatModel.getInstance().getComment());
    }

    private void add_daylightlightening() {
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.LED_Lights)) {
            LightingModel.getInstance().setImage_1(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.LED_Lights));
        } else {
            LightingModel.getInstance().setImage_1(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Fluorescent_Lights)) {
            LightingModel.getInstance().setImage_2(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Fluorescent_Lights));
        } else {
            LightingModel.getInstance().setImage_2(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Halogen_lights)) {
            LightingModel.getInstance().setImage_3(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Halogen_lights));
        } else {
            LightingModel.getInstance().setImage_3(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Indoor_Light)) {
            LightingModel.getInstance().setImage_4(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Indoor_Light));
        } else {
            LightingModel.getInstance().setImage_4(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Outdoor_Light)) {
            LightingModel.getInstance().setImage_5(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Outdoor_Light));
        } else {
            LightingModel.getInstance().setImage_5(this.convertImageToByte());
        }
        if (FileModel.getFileModel().fileArrayByte.containsKey(Extra.FileConstants.Wattage_Sensor)) {
            LightingModel.getInstance().setImage_6(FileModel.getFileModel().fileArrayByte.get(Extra.FileConstants.Wattage_Sensor));
        } else {
            LightingModel.getInstance().setImage_6(this.convertImageToByte());
        }
//        System.out.println("Image 1: " + Integer.toString(LightingModel.getInstance().getImage_1().length));
//        System.out.println("Image 2: " + Integer.toString(LightingModel.getInstance().getImage_2().length));
//        System.out.println("Image 3: " + Integer.toString(LightingModel.getInstance().getImage_3().length));
//        System.out.println("Image 4: " + Integer.toString(LightingModel.getInstance().getImage_4().length));
//        System.out.println("Image 5: " + Integer.toString(LightingModel.getInstance().getImage_5().length));
//        System.out.println("Image 6: " + Integer.toString(LightingModel.getInstance().getImage_6().length));
        DayLightBackTask daylighttask = new DayLightBackTask(Splash_Screen_Process.this, LightingModel.getInstance().getImage_1(),
                LightingModel.getInstance().getImage_2(), LightingModel.getInstance().getImage_3(), LightingModel.getInstance().getImage_4(),
                LightingModel.getInstance().getImage_5(), LightingModel.getInstance().getImage_6());
            daylighttask.setUser_id(this.userid);
            daylighttask.setPro_id(PropertyModel.getInstance().getCertificate_number());

            LightingModel.getInstance().setComment(BaseActivity.getComment(Extra.Tag.Daylight));
            daylighttask.insert(LightingModel.getInstance().getLux_meter(),
                    LightingModel.getInstance().getLed_light(),
                    LightingModel.getInstance().getCfl_light()
                    , LightingModel.getInstance().getHalogen_light(),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_indoor_light()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_wattage()),
                    Integer.toString(LightingModel.getInstance().getSensor_wattage()),
                    Float.toString(LightingModel.getInstance().getTotal_outdoor_light()),
                    LightingModel.getInstance().getInspectorlightview(),
                    Float.toString(LightingModel.getInstance().getTotal_points()),
                    LightingModel.getInstance().getComment());


    }

    private void add_thermal_envelope() {
        ThermalBackTask thermaltask = new ThermalBackTask(Splash_Screen_Process.this);
        thermaltask.setUser_id(this.userid);
        thermaltask.setPro_id(PropertyModel.getInstance().getCertificate_number());

        ThermalEnvelope.getInstance().setComment(BaseActivity.getComment(Extra.Tag.thermal));

        thermaltask.insert(ThermalEnvelope.getInstance().getIndex(), Integer.toString(ThermalEnvelope.getInstance().getPoints())
                , ThermalEnvelope.getInstance().getComment());
    }

    public byte[] convertImageToByte() {
        byte[] data = null;
        try {
            Drawable drawable = this.getResources().getDrawable(R.drawable.unavailable_image);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


}