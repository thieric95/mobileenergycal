package com.nixbymedia.energycalculator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.nixbymedia.energycalculator.property.PropertyModel;

import java.util.ArrayList;


public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

    private Context ctx;
    private ArrayList<PropertyModel> arrayList;


    class Holder extends RecyclerView.ViewHolder{

        TextView propertyIDtxt, addresstxt;
        Button viewdetail, editbtn;
        public Holder(@NonNull View itemView){
            super(itemView);
            propertyIDtxt = itemView.findViewById(R.id.propertyIDtxt);
            addresstxt = itemView.findViewById(R.id.addresstxt);
            viewdetail = itemView.findViewById(R.id.viewdetail);
            editbtn = itemView.findViewById(R.id.editbtn);

        }
    }

    public Adapter(Context ctx, ArrayList<PropertyModel> arrayList) {
        this.ctx = ctx;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.ctx).inflate(R.layout.row, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
            PropertyModel propertyitem = this.arrayList.get(i);

            final String cert_id = propertyitem.getCertificate_number();
            final String dwel_type = propertyitem.getDwellingType();
            final String dwel_size = propertyitem.getDwellingSize();
            final String room_no = propertyitem.getNoOfBedRooms();
            final String climate = propertyitem.getClimate_location();
            final String legal_desc = propertyitem.getLegal_description();
            final String cert_title = propertyitem.getTitle_certificate();
            final String address = propertyitem.getAddress();
            final String comment = propertyitem.getComment();

            holder.propertyIDtxt.setText(cert_id);
            holder.addresstxt.setText(address);
            holder.viewdetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoResult(cert_id);
                }
            });
            holder.editbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoEdit(cert_id, dwel_type, dwel_size, room_no, climate, legal_desc,cert_title, address, comment);
                }
            });
    }

    @Override
    public int getItemCount() {
        return this.arrayList.size();
    }

    private void gotoResult(String cert_id){
        Intent int1 = new Intent(ctx, Activity_Result.class);
        int1.putExtra("option", "view");
        int1.putExtra("propertyID", cert_id);
        ctx.startActivity(int1);
    }

    private void gotoEdit(String cert_id, String dwel_type, String dwel_size, String room_no,
                          String climate, String legal_desc, String cert_title, String address, String comment){
        System.out.println(cert_id);
        Intent int1 = new Intent(ctx, StepProcessActUpdate.class);
        int1.putExtra("propertyID", cert_id);
        ctx.startActivity(int1);

    }
}
