package com.nixbymedia.energycalculator;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;


import com.nixbymedia.energycalculator.extra.Debugger;
import com.nixbymedia.energycalculator.extra.Extra;
import com.nixbymedia.energycalculator.heatingsystem.Heating_SystemUpdate;
import com.nixbymedia.energycalculator.lighting.Daylight_LighteningUpdate;
import com.nixbymedia.energycalculator.property.PropertyDetailUpdate;
import com.nixbymedia.energycalculator.property.PropertyModel;
import com.nixbymedia.energycalculator.thermal_envelope.Thermal_Envelope_Update;
import com.nixbymedia.energycalculator.water_user.Water_Use_Update;

import java.util.HashMap;

public class StepProcessActUpdate extends BaseActivity {
    public static final int REQUEST_CODE_ASK_SINGLE_PERMISSION = 100;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private FragmentManager fragmentManager;
    int count = 0;
    public PropertyDetailUpdate propertyDetailupdate;
    SessionManagement session;
    private int userid;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_back:
                    manageBackStack();
                    return false;
                case R.id.navigation_comment:

                    checkEnabled();
                    return true;
                case R.id.navigation_forward:
                    //fragmentManager.beginTransaction().replace(R.id.fragment_main, new Thermal_Envelope()).commit();
                    manageForwardStack();
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_process_act_update);
        this.getuserID();
        Intent intent = getIntent();
        if (intent != null) {
            PropertyModel.getInstance().setCertificate_number(intent.getStringExtra("propertyID"));
        }

        propertyDetailupdate = new PropertyDetailUpdate();
        Bundle bundle = new Bundle();
        bundle.putString("propertyid", PropertyModel.getInstance().getCertificate_number());
        bundle.putInt("userid", this.userid);
        propertyDetailupdate.setArguments(bundle);
        permissionForMarshmallow();
        BottomNavigationView navigation = findViewById(R.id.navigation_update);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        replaceFragment(propertyDetailupdate, "property");


        checkEnabled();
    }

    private void manageBackStack() {
        DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(StepProcessActUpdate.this);
        switch (count) {
            case 1:
                count = 0;
                replaceFragment(new PropertyDetailUpdate(), Extra.Tag.property);
                break;
            case 2:
                count = count - 1;
                replaceFragment(new Thermal_Envelope_Update(), Extra.Tag.thermal);
                break;
            case 3:
                count = count - 1;
                replaceFragment(new Heating_SystemUpdate(), Extra.Tag.HeatingSystem);
                break;
            case 4:
                count = count - 1;
                replaceFragment(new Daylight_LighteningUpdate(), Extra.Tag.Daylight);
                break;
            case 5:
                count = count - 1;
                replaceFragment(new Water_Use_Update(), Extra.Tag.WaterUse);
                break;
            default:
                break;
        }
    }

    public void replaceFragment(final Fragment fragment, final String title) {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (!getSupportFragmentManager().popBackStackImmediate(title, 0)) { // here we check that fragment alredy exist or not.
                        Debugger.debugE("FragmentNotFound", "true");
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.in_left, R.anim.fade_out).replace(R.id.fragment_main_update, fragment).addToBackStack(title).commit();
                    }
                }
            }, 250);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("RestrictedApi")
    private void checkEnabled() {
        if (count == 0) {
            BottomNavigationItemView back = findViewById(R.id.navigation_back);
            back.setEnabled(false);
        }
        if (count == 6) {
            BottomNavigationItemView forward = findViewById(R.id.navigation_forward);
            forward.setEnabled(false);
        }
    }

    private void manageForwardStack() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        //DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass()
        switch (count) {
            case 0:
                replaceFragment(new Thermal_Envelope_Update(), Extra.Tag.thermal);
                count = count + 1;
//                Toast.makeText(this, " "+count, Toast.LENGTH_SHORT).show();
                break;
            case 1:
                replaceFragment(new Heating_SystemUpdate(), Extra.Tag.HeatingSystem);
                count = count + 1;
//                Toast.makeText(this, " "+count, Toast.LENGTH_SHORT).show();
                break;
            case 2:
                replaceFragment(new Daylight_LighteningUpdate(), Extra.Tag.Daylight);
                count = count + 1;
                break;
            case 3:
                replaceFragment(new Water_Use_Update(), Extra.Tag.WaterUse);
                count = count + 1;
                break;
            case 4:
                startActivity(new Intent(StepProcessActUpdate.this, Activity_Result.class));
                finish();
                break;
            default:
                break;
        }
    }

    private void permissionForMarshmallow() {


        if (ContextCompat.checkSelfPermission(StepProcessActUpdate.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(StepProcessActUpdate.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_ASK_SINGLE_PERMISSION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void getuserID(){
        this.session = new SessionManagement(getApplicationContext());
        if (this.session.checkforlogin()) {
            finish();
        }
        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}