package com.nixbymedia.energycalculator;

import java.util.HashMap;

public class CommentModel {
    private HashMap<String, String> comment = new HashMap<>();
    private static CommentModel commentModel = new CommentModel();

    public HashMap<String, String> getComment() {
        return comment;
    }

    public boolean checkforkeyexist(String key){
        return comment.containsKey(key);
    }

    public static CommentModel getInstance() {
        return commentModel;
    }

    public void clear_comment(){
        this.comment.clear();
    }

}
