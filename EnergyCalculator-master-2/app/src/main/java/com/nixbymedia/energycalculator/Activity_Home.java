package com.nixbymedia.energycalculator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.HashMap;


public class Activity_Home extends AppCompatActivity implements View.OnClickListener{


    private Button btnCreateProject, btnViewProject;
    private int userid;
    private String mobile, password, username;
    private TextView mobilenumtxt;
    private ImageButton btnlogout;
    SessionManagement session;
    private Activity ctx;
    private static final String PREF_NAME = "Energy Calculator";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //sharedpreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.getuserID();
        this.mobilenumtxt = findViewById(R.id.mobilenumtxt);
        this.btnlogout = findViewById(R.id.btnlogout);
        this.mobilenumtxt.setText("Welcome "+this.username);
        this.btnCreateProject = findViewById(R.id.btnCreateProject);
        this.btnViewProject = findViewById(R.id.btnViewProject);
        btnCreateProject.setOnClickListener(this);
        btnViewProject.setOnClickListener(this);
        this.btnlogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCreateProject:
                Intent intent = new Intent(this, StepProcessAct.class);
                startActivity(intent);
                break;
            case R.id.btnViewProject:
                Intent intent1 = new Intent(this, ProjectList.class);
                startActivity(intent1);
                break;
            case R.id.btnlogout:
                this.session.logoutUser();
                break;
            default:
                break;
        }
    }

    private void getuserID(){
        this.session = new SessionManagement(getApplicationContext());
        if(this.session.checkforlogin()){
            finish();
        }

        HashMap<String, String> user = this.session.getUserDetails();
        this.userid = Integer.parseInt(user.get(SessionManagement.KEY_ID));
        this.mobile = user.get(SessionManagement.KEY_MOBILE);
        this.username = user.get(SessionManagement.KEY_USERNAME);
    }

//    private void manageBackStack() {
//        DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass(Activity_Home.this);
//        SQLiteDatabase db = databaseHelperClass.getWritableDatabase();
//        switch (count) {
//            case 1:
//                count = 0;
//                replaceFragment(new PropertyDetail(), Extra.Tag.property);
//                break;
//            case 2:
//                count = count - 1;
//                replaceFragment(new Thermal_Envelope(), Extra.Tag.thermal);
//                break;
//            case 3:
//                count = count - 1;
//                replaceFragment(new Heating_System(), Extra.Tag.HeatingSystem);
//                break;
//            case 4:
//                count = count - 1;
//                replaceFragment(new DayLight_Lightening(), Extra.Tag.Daylight);
//                break;
//            case 5:
//                count = count - 1;
//                replaceFragment(new Water_Use(), Extra.Tag.WaterUse);
//                break;
//            default:
//                break;
//        }
//    }

//    public void replaceFragment(final Fragment fragment, final String title) {
//        try {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    if (!getSupportFragmentManager().popBackStackImmediate(title, 0)) { // here we check that fragment alredy exist or not.
//                        Debugger.debugE("FragmentNotFound", "true");
//                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.in_left, R.anim.fade_out).replace(R.id.fragment_main, fragment).addToBackStack(title).commit();
//                    }
//                }
//            }, 250);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            switch (requestCode) {
//                case Extra.ActivityRequestCode.CAMERA_REQUEST_CODE:
//                    if (resultCode == RESULT_OK && photo != null) {
//                        String fileName = photo.getName();
//                        Utils.showToast(this, "Image Saved on " + photo.getAbsolutePath());
//                        FileModel.getFileModel().fileMap.put(fileName.split("_")[0], photo.getAbsolutePath());
//                    }
//                    break;
//                case 1:
//                    propertyDetail.onActivityResult(requestCode, resultCode, data);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


//    public void savePdf() {
//        try {
//            new CountDownTimer(1000, 5000) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                }
//
//                @Override
//                public void onFinish() {
//                    saveToPdf(Extra.SubPath.HeatingSystem);
//                }
//            }.start();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    @SuppressLint("RestrictedApi")
//    private void checkEnabled() {
//        if (count == 0) {
//            BottomNavigationItemView back = findViewById(R.id.navigation_back);
//            back.setEnabled(false);
//        }
//        if (count == 6) {
//            BottomNavigationItemView forward = findViewById(R.id.navigation_forward);
//            forward.setEnabled(false);
//        }
//    }
//
//
//    private void manageForwardStack() {
//        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//        //DatabaseHelperClass databaseHelperClass = new DatabaseHelperClass()
//        switch (count) {
//            case 0:
//                replaceFragment(new Thermal_Envelope(), Extra.Tag.thermal);
//                count = count + 1;
////                Toast.makeText(this, " "+count, Toast.LENGTH_SHORT).show();
//                break;
//            case 1:
//                replaceFragment(new Heating_System(), Extra.Tag.HeatingSystem);
//                count = count + 1;
////                Toast.makeText(this, " "+count, Toast.LENGTH_SHORT).show();
//                break;
//            case 2:
//                replaceFragment(new DayLight_Lightening(), Extra.Tag.Daylight);
//                count = count + 1;
//                break;
//            case 3:
//                replaceFragment(new Water_Use(), Extra.Tag.WaterUse);
//                count = count + 1;
//                break;
//            case 4:
//                replaceFragment(new Other_Charges(), Extra.Tag.Other_Charges);
//                count = count + 1;
//
//                break;
//            case 5:
//                startActivity(new Intent(Activity_Home.this, Activity_Result.class));
//                finish();
//                break;
//            default:
//
//        }
//    }
//
//    private void permissionForMarshmallow() {
//
//        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
//
//        if (ContextCompat.checkSelfPermission(Activity_Home.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(Activity_Home.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_ASK_SINGLE_PERMISSION);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
//
//            } else {
//                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_LONG).show();
//            }
//
//        }
//    }


}
